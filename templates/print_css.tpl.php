<?php

/**
 * Generate the css code print.css
 */
function generate_printcss() {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';
  if ($themebuilder_print_mission == 1) {
    $output_mission = '';
  }
  else {
    $output_mission = '  visibility: hidden;
  display: none;';
  }
$output = '/* $Id$ */
<!--
/* <![CDATA[ */

/**
 * generator:      '. $themebuilder_meta_generator .'
 * author:         '. $themebuilder_meta_author .'
 * email:          '. $themebuilder_theme_email .'
 * website:        '. $themebuilder_theme_url .'
 * last modified:  '. date("Y-m-d") .'T'. date("H:i:s") .'-05:00'.'
 * license:        '. $themebuilder_meta_copyright .'
 */
 
/**
 * -----------------------------------------------------------------------------
 * Generic elements
 * -----------------------------------------------------------------------------
 */
* {
  color: #000;
  font-family: '. $themebuilder_font_family_print .';
}
html body {
  background: transparent;
  width: auto;
}

/* white background, black text, arial font, decent font size */
body, node.content {
  background-color: #fff;
  color: #000;
  font-family: '. $themebuilder_font_family_print .';
  font-size: 14pt;
  width: 100%;
}

/* underline all links */
a:link, a:visited {
  color: #520;
  background: transparent;
  text-decoration: underline !important;
}

/**
 * -----------------------------------------------------------------------------
 * Page
 * -----------------------------------------------------------------------------
 */
#page {
  margin: 0;
  width: 90%;
}

/* do not underline header */
#header a:link, #header a:visited {
  text-decoration: none !important;
}

/* CSS2 selector to add visible href after links */
#main a:link:after, #main a:visited:after,
.content a:link:after, .content a:visited:after {
  content: " (" attr(href) ") ";
  font-size: 80%;
  font-weight: normal;
}

/* do not float floating things */
#main, #container .sidebar,
#content,
body.no-sidebars #content,
body.one-sidebar #content,
body.two-sidebars #content,
body.sidebar-left #content,
body.sidebar-right #content {
  margin: 0;
  float: none;
  width: 100% !important;
}
.submitted {
  color: #000;
  font-family: '. $themebuilder_font_family_print .';
  font-size: 10pt;
}

/* Hide sidebars and nav elements */
hr, div#search, div#search-header h2, div#search label, #skip-nav, #primary, #secondary, #sidebar-left, #sidebar-right, #search, #tabs, #menu, #footer-wrapper,
.book-navigation, .tabs, .links, .breadcrumb, .taxonomy, .pager, .feed-icon, .forum-topic-navigation {
  visibility: hidden;
  display: none;
}

#logo-title {
  margin: 20pt 20pt 0 20pt;
}

#page, #header {
 border: none !important;
}
#header {
 height: 100px !important;
}

h1#site-name {
  color: '. $themebuilder_color_text_sitename_print .';
  font-family: '. $themebuilder_font_family_sitename .';
}
#site-slogan {
  color: '. $themebuilder_color_text_siteslogan_print .';
  font-family: '. $themebuilder_font_family_slogan .';
}
#mission {
'. $output_mission .'
}
#logo {
 /* margin: 20pt 20pt 0 20pt;
  padding: 20pt 20pt 0 20pt;
  position: relative !important;
  float: left;
  clear: both;
*/
}
img#logo-image {
  visibility: visible;
}
body, #header, #sidebar-left, #main, #sidebar-right, #footer {
  background: none !important;
  background-image: none !important;
}
';
  return $output;
}