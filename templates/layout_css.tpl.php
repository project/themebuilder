<?php

/**
 * @file
 * Generate the css code layout.css
 */
function generate_layoutcss() {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';
$page_width = themebuilder_page_width("static");
$page_width_left = themebuilder_page_width("left");
$page_width_right = themebuilder_page_width("right");
$page_width_two = themebuilder_page_width("two");
$page_width_screen = themebuilder_page_width("screen");
$page_width_final = themebuilder_page_width("final");

  switch ($themebuilder_page_textalign) {
  case 'left':
  $page_margin = "0 auto 0 0";
  break;
  case 'center':
  $page_margin = "auto";
  break;
  case 'right':
  $page_margin = "0 0 0 auto";
  break;
  default:
  $page_margin = "0 auto";
  break;
  }
  if ($themebuilder_block_order == 1) {
    $output_add = '/* Content first */
#container .sidebar  {
  display: block;
  float: left;
  margin: 0;
  /* left: 0;
  top: 0;
  */
}

/* So we move the #main container over the sidebars to compensate */
body.sidebar-left #main,
body.two-sidebars #main {
  margin-left: 0;
  float: right;
}
body.sidebar-right #main,
body.two-sidebars #main {
  margin-right: 0;
    float: left;
}';
  }
  else {
    $output_add = '';
  }
$themebuilder_element_errors_layout = '
/**
 * -----------------------------------------------------------------------------
 * Display not allowed elements
 * -----------------------------------------------------------------------------
 */
'. $notallowed_elements .' {
  border: 1px solid red;
  display: block;
  padding: 1em;
  color: red !important;
  background-color: #FFCCCC !important;
  border: 1px solid #DD7777 !important;
}';

$output = '/* $Id$ */
<!--
/* <![CDATA[ */

/**
 * generator:      '. $themebuilder_meta_generator .'
 * author:         '. $themebuilder_meta_author .'
 * email:          '. $themebuilder_theme_email .'
 * website:        '. $themebuilder_theme_url .'
 * last modified:  '. date("Y-m-d") .'T'. date("H:i:s") . $themebuilder_theme_timezone .'
 * license:        '. $themebuilder_meta_copyright .'
 *
 * base color 1 '. $color .'
 * base color 2 '. $themebuilder_basecolor2 .'
 * base color 3 '. $themebuilder_basecolor3 .'
 * base color 4 '. $themebuilder_basecolor4 .'
 */

/**
 * Generic elements
 */
* {
  padding: 0;
  margin: 0;
}

/**
 * --------------------
 * Body
 * --------------------
 */
body {
  /* border */
  border-width: '. $themebuilder_body_borderwidth .';
  border-style: '. $themebuilder_body_borderstyle .';
  
  /* typo */
  font-family: '. $themebuilder_div_content_fontfamily .';
  line-height: '. $themebuilder_div_content_lineheight .';
  font-size: '. $themebuilder_div_content_fontsize .';
  text-align: '. $themebuilder_page_textalign .'; /* Center of layout for IE 5 */
  
  /* color */
  color: '. $themebuilder_body_color .';
  background-color: '. $themebuilder_body_backgroundcolor .';
  border-color: '. $themebuilder_body_bordercolor .';
  
  /* background image */
  background-image: url("images/body.jpg");
  background-repeat: '. $themebuilder_body_backgroundrepeat .'; /* "repeat" for NN 4*/
  background-position: '. $themebuilder_body_backgroundposition .'; /* Not for NN 4 */
  background-attachment: '. $themebuilder_body_backgroundattachment .';

 /*  print $base_path . path_to_theme() // Note: Use absolut url and do not use tranparent GIF for NN 4 */
}

h1, h2, h3, h4, h5, h6, ol, ul, li, dl, dt, dd, p, input, textarea, select, a {
/*  color: #333; */
/*  font-family: Arial, Helvetica, sans-serif; */
  font-family: '. $themebuilder_div_content_fontfamily .';

  text-align: left; /* body is text align left */
}
h1 {
  margin: '. $themebuilder_div_h1_lineheight .'em 0em '. $themebuilder_h1_lineheight .'em 0em;
  font-family: '. $themebuilder_h1_fontfamily .';
  line-height: '. $themebuilder_h1_lineheight .'em;
  font-size: '. $themebuilder_h1_fontsize .'em;
  color: '. $themebuilder_h1_color .';
}
h2 {
  margin: '. $themebuilder_h2_lineheight .'em 0em '. $themebuilder_h2_lineheight .'em 0em;
  font-family: '. $themebuilder_h2_fontfamily .';
  line-height: '. $themebuilder_h2_lineheight .'em;
  font-size: '. $themebuilder_h2_fontsize .'em;
  color: '. $themebuilder_h2_color .';
}
h3 {
  margin: '. $themebuilder_h3_lineheight .'em 0em '. $themebuilder_h3_lineheight .'em 0em;
  font-family: '. $themebuilder_h3_fontfamily .';
  line-height: '. $themebuilder_h3_lineheight .'em;
  font-size: '. $themebuilder_h3_fontsize .'em;
  color: '. $themebuilder_h3_color .';
}
h3 {
  margin: '. $themebuilder_h4_lineheight .'em 0em '. $themebuilder_h4_lineheight .'em 0em;
  font-family: '. $themebuilder_h4_fontfamily .';
  line-height: '. $themebuilder_h4_lineheight .'em;
  font-size: '. $themebuilder_h4_fontsize .'em;
  color: '. $themebuilder_h4_color .';
}
h5 {
  margin: '. $themebuilder_h5_lineheight .'em 0em '. $themebuilder_h5_lineheight .'em 0em;
  font-family: '. $themebuilder_h5_fontfamily .';
  line-height: '. $themebuilder_h5_lineheight .'em;
  font-size: '. $themebuilder_h5_fontsize .'em;
  color: '. $themebuilder_h5_color .';
}
h6 {
  margin: '. $themebuilder_h6_lineheight .'em 0em '. $themebuilder_h6_lineheight .'em 0em;
  font-family: '. $themebuilder_h6_fontfamily .';
  line-height: '. $themebuilder_h6_lineheight .'em;
  font-size: '. $themebuilder_h6_fontsize .'em;
  color: '. $themebuilder_h6_color .';
}

div.node h2,
div.node h3,
div.node h4,
div.node h5,
div.node h6 {
  background-color: '. $themebuilder_div_content_backgroundcolor .'; /* for IE 5 Mac */
}
div.block h2,
div.block h3,
div.block h4,
div.block h5,
div.block h6 {
  margin: 1.1em 1em 1.1em 1em;
}

/* Links (Note: Do not use "a {...}" for NN 4) */
a:link {
  /* border */
  border-width: '. $themebuilder_a_link_borderwidth .';
  border-style: '. $themebuilder_a_link_borderstyle .';
  /* typo */
  text-decoration: '. $themebuilder_a_link_textdecoration .';
  /* color */
  color: '. $themebuilder_a_link_color .';
  background-color: '. $themebuilder_a_link_backgroundcolor .';
}
a:visited {
  /* border */
  border-width: '. $themebuilder_a_visited_borderwidth .';
  border-style: '. $themebuilder_a_visited_borderstyle .';
  /* typo */
  text-decoration: '. $themebuilder_a_visited_textdecoration .';
  /* color */
  color: '. $themebuilder_a_visited_color .';
  background-color: '. $themebuilder_a_visited_backgroundcolor .';
}
a:focus {
  /* border */
  border-width: '. $themebuilder_a_focus_borderwidth .';
  border-style: '. $themebuilder_a_focus_borderstyle .';
  /* typo */
  text-decoration: '. $themebuilder_a_focus_textdecoration .';
  /* color */
  color: '. $themebuilder_a_focus_color .';
  background-color: '. $themebuilder_a_focus_backgroundcolor .';
}
a:hover {
  /* border */
  border-width: '. $themebuilder_a_hover_borderwidth .';
  border-style: '. $themebuilder_a_hover_borderstyle .';
  /* typo */
  text-decoration: '. $themebuilder_a_hover_textdecoration .';
  /* color */
  color: '. $themebuilder_a_hover_color .';
  background-color: '. $themebuilder_a_hover_backgroundcolor .';
}
a:active {
  /* border */
  border-width: '. $themebuilder_a_active_borderwidth .';
  border-style: '. $themebuilder_a_active_borderstyle .';
  /* typo */
  text-decoration: '. $themebuilder_a_active_textdecoration .';
  /* color */
  color: '. $themebuilder_a_active_color .';
  background-color: '. $themebuilder_a_active_backgroundcolor .';
}

.node .content a:visited:after {
  color: '. $themebuilder_a_visited_color .';
  content: "\00A0\221A";
}
.node .content a:hover:after {
  color: '. $themebuilder_a_hover_color .';
  content: "\00A0\221A";
}

a:hover img {
  background: transparent;
}
a:link.skip, a:visited.skip, a:hover.skip,
a:active.skip, a:focus.skip {
  position: absolute;
  left: -1000px;
  top: -1000px;
  width: 0px;
  height: 0px;
  overflow: hidden;
  display: inline;
}
a:link img {
  border: 0;
}
img, a img {
  border: 0;
}
p {
  margin: '. $themebuilder_div_content_lineheight .'em 0em '. $themebuilder_div_content_lineheight .'em 0em;
  font-family: '. $themebuilder_div_content_fontfamily .';
  line-height: '. $themebuilder_div_content_lineheight .'em;
  font-size: '. $themebuilder_div_content_fontsize .'em;
  color: '. $themebuilder_div_content_color .';
}
ul, ol {
  padding: '. $themebuilder_div_content_lineheight .'em 0em '. $themebuilder_div_content_lineheight .'em 2em;
  margin: 0;
  line-height: '. $themebuilder_div_content_lineheight .'em;

}
ul li {
/*  padding: 0;
  margin: 0;
  list-style-type: square;
  position: relative;
*/}
dt {
  font-style: italic;
  padding: 0.2em 0em 0.2em 0em;
}
dd {
  padding: 0em 0em 0em 1em;
}
blockquote,
cite,
q {
  font-style: italic;
  border-bottom: 1px dashed gray;
  border-left: 1px dashed gray;
  background: url("images/comment.png") no-repeat top right;
}
/* Abbr, Acronym */
abbr {
  speak: spell-out;
}
acronym {
  speak: normal;
}
acronym, abbr, span.caps {
  cursor: help;
}
acronym, abbr {
  border-bottom: 1px dashed #999;
}
acronym, abbr, acronym:link, abbr:link {
  cursor: help;
  border-bottom: 1px dashed #999;
}
/* Tabellen */

table {
  table-layout: auto;
  margin: 0;
  width: '. $themebuilder_content_width . $themebuilder_theme_layout .';
}
caption {
  width: auto;
  padding: 0.5em 1em;
  margin: 0;
}

thead th, thead {
}
tr,th,td {
  margin: 0;
  padding: .4em;
}

/**
 * --------------------
 * KBD (small logo style)
 * --------------------
 */
kbd,
kbd:link {
  border-width: 0.2em;
  border-style: outset;
  /* typo */
  font: bold 0.7em sans-serif;
  text-decoration: none;
  /* color */
  color: '. $themebuilder_kbd_color .';
  background-color: '. $themebuilder_kbd_backgroundcolor .';
  border-color: '. $themebuilder_kbd_backgroundcolor .';
}

kbd:hover {
  cursor: help;
  text-decoration: none;
/*
  color: #fff;
  background: #666;
  border-color: #666;
*/
}
kbd:focus {
  cursor: crosshair;
  text-decoration: none;
/*
  color: #666;
  background: #fff;
  border-color: #fff;
*/
  border-style: inset;
}

/**
 * --------------------
 * Generic classes
 * --------------------
 */
.content-margin {
  padding: 1em 1em 1em 1em;
}
.clear {
  clear: both;
}
.none {
  display: none;
}
.hidden {
  float: left;
  display: inline;
  width: auto;
  position: absolute;
  top: -100em;
  left: -100em;
}
.leftalign {
  float: left;
}
.hr {
  float: left;
  display: inline;
  width: auto;
  position: absolute;
  top: -100em;
  left: -100em;
}
#content .leftalign {
  float: left;
  margin: 5px 15px 5px 5px;
}

.rightalign {
 float: right;
}

#content .rightalign {
  float: left;
  margin: 5px 5px 5px 15px;
}

/**
 * ------------------------------
 * Page
 * ------------------------------
 */
div#page {
  /* structure */
  display: block;
  float: none;
  position: static;
  
  /* layout */
  min-width: '. themebuilder_calculate($page_width, 'min') . $theme_min_suffix .';
  max-width: '. themebuilder_calculate($page_width, 'max') . $theme_max_suffix .';
  padding: 0px 0px 0px 0px;
  margin: auto;  /* Not: "margin: 0 auto;" for Opera 4 */
  
  /* border */
  border-width: '. $themebuilder_page_borderwidth .';
  border-style: '. $themebuilder_page_borderstyle .';
  
  /* color */
  color: '. $themebuilder_page_color .';
  background-color: '. $themebuilder_page_backgroundcolor .';
  border-color: '. $themebuilder_page_bordercolor .';
}

/**
 * ----------
 * No sidebar
 * ----------
 */
body.no-sidebars #page,
body.no-sidebars #header,
body.no-sidebars #skip-nav,
body.no-sidebars #main,
body.no-sidebars #footer,
body.no-sidebars #header-content,
body.no-sidebars #primary,
body.no-sidebars #secondary {
  width: '. $themebuilder_content_width . $themebuilder_theme_layout .';
/*  min-width: '. themebuilder_calculate($page_width, 'min') . $theme_min_suffix .'; */
/*  max-width: '. themebuilder_calculate($page_width, 'max') . $theme_max_suffix .'; */
}
body.no-sidebars #content {
  width: '. $themebuilder_content_width . $themebuilder_theme_layout .';
/*  min-width: '. themebuilder_calculate($page_width, 'min') . $theme_min_suffix .'; */
/*  max-width: '. themebuilder_calculate($page_width, 'max') . $theme_max_suffix .'; */
}

body.no-sidebars #name-slogan {
  width: '. ($themebuilder_content_width - $themebuilder_image_height_logo_view_width) . $themebuilder_theme_layout .';
}

/**
 * ----------
 * One sidebar
 * ----------
 */
/**
 * One sidebar left
 */
body.sidebar-left #page,
body.sidebar-left #header,
body.sidebar-left #skip-nav,
body.sidebar-left #main,
body.sidebar-left #footer,
body.sidebar-left #header-content,
body.sidebar-left #primary,
body.sidebar-left #secondary {
  width: '. $page_width_left . $themebuilder_theme_layout .';
/*  min-width: '. themebuilder_calculate($page_width, 'min') . $theme_min_suffix .'; */
/*  max-width: '. themebuilder_calculate($page_width, 'max') . $theme_max_suffix .'; */
}

/**
 * One sidebar right
 */
body.sidebar-right #page,
body.sidebar-right #header,
body.sidebar-right #skip-nav,
body.sidebar-right #main,
body.sidebar-right #footer,
body.sidebar-right #header-content,
body.sidebar-right #primary,
body.sidebar-right #secondary {
  width: '. $page_width_right . $themebuilder_theme_layout .';
/*  min-width: '. themebuilder_calculate($page_width, 'min') . $theme_min_suffix .'; */
/*  max-width: '. themebuilder_calculate($page_width, 'max') . $theme_max_suffix .'; */
}
body.one-sidebar #content {
  width: '. $themebuilder_content_width . $themebuilder_theme_layout .';
/*  min-width: '. themebuilder_calculate($themebuilder_content_width, 'min') . $theme_min_suffix .'; */
/*  max-width: '. themebuilder_calculate($themebuilder_content_width, 'max') . $theme_max_suffix .'; */
}
body.one-sidebar #sidebar-right {
  width: '. $themebuilder_right_width . $themebuilder_theme_layout .';
/*  min-width: '. themebuilder_calculate($themebuilder_right_width, 'min') . $theme_min_suffix .'; */
/*  max-width: '. themebuilder_calculate($themebuilder_right_width, 'max') . $theme_max_suffix .'; */
}
body.one-sidebar #name-slogan {
  width: '. ($page_width - $themebuilder_image_height_logo_view_width ) . $themebuilder_theme_layout .';
}

/**
 * ----------
 * Two sidebar
 * ----------
 */
body.two-sidebars #page,
body.two-sidebars #header,
body.two-sidebars #skip-nav,
body.two-sidebars #main,
body.two-sidebars #footer,
body.two-sidebars #header-content,
body.two-sidebars #primary,
body.two-sidebars #secondary {
  width: '. $page_width_two . $themebuilder_theme_layout .';
/*  min-width: '. themebuilder_calculate($page_width, 'min') . $theme_min_suffix .'; */
/*  max-width: '. themebuilder_calculate($page_width, 'max') . $theme_max_suffix .'; */
}
body.two-sidebars #name-slogan {
/*  width: 52.5em; */
  width: '. ($page_width_two - $themebuilder_image_height_logo_view_width ) . $themebuilder_theme_layout .';
}
body.two-sidebars #content {
  width: '. $themebuilder_content_width . $themebuilder_theme_layout .';
/*  min-width: '. themebuilder_calculate($themebuilder_content_width, 'min') . $theme_min_suffix .'; */
/*  max-width: '. themebuilder_calculate($themebuilder_content_width, 'max') . $theme_max_suffix .'; */
}
body.two-sidebars #main.content-first #content {
  margin: 0 0 0 '. $themebuilder_left_width . $themebuilder_theme_layout .';
}
body.two-sidebars #main.content-first #sidebar-left {
  /* margin: 0 0 0 -46.7em;  35+12-0.5+0.2=46.7em (0.2 for 2px border of #content) */
  margin: 0 0 0 '. ($page_width_two - $themebuilder_rounding_errors + 0.2 ) . $themebuilder_theme_layout .';
  background-color: transparent;
}
body.two-sidebars #main.content-first #sidebar-right {
}

/**
 * ----------
 * Header
 * ----------
 */
#header {
  /* structure */
  display: block;
  float: none;
  clear: none;

  /* layout */
  position: static;
  /*  width: '. $page_width . $themebuilder_theme_layout .'; */
  /*  min-width: '. themebuilder_calculate($page_width, 'min') . $theme_min_suffix .'; */
  /*  max-width: '. themebuilder_calculate($page_width, 'max') . $theme_max_suffix .'; */
  height: auto;
  padding: 0px 0px 0px 0px;
  margin: 0px 0px 0px 0px;

  /* border */
  border-width: '. $themebuilder_div_header_borderwidth .';
  border-style: '. $themebuilder_div_header_borderstyle .'; /* 1px for NN 6 */ 

  /* typo */
  font-family: '. $themebuilder_div_content_fontfamily .';
  line-height: '. $themebuilder_div_content_lineheight .';
  font-size: '. $themebuilder_div_content_fontsize .';
  text-align: '. $themebuilder_page_textalign .'; /* Center of layout for IE 5 */

  /* color */
  color: '. $themebuilder_div_header_color .';
  background-color: '. $themebuilder_div_header_backgroundcolor .';
  border-color: '. $themebuilder_div_header_bordercolor .';

  /* background image */
  background-image: url("images/div_header.jpg");
  background-repeat: '. $themebuilder_div_header_backgroundrepeat .'; /* "repeat" for NN 4*/
  background-position: '. $themebuilder_div_header_backgroundposition .'; /* Not for NN 4 */
  background-attachment: '. $themebuilder_div_header_backgroundattachment .';
}

/**
 * Skip-nav link to main content
 * "a" for Opera 5
 * Note: Opera browser do not support (Opera 6 ":active" and Opera 7 ":focus) in the skip nav with "tabindex".
 */
#skip-nav {
  float: none; /* Important for #name-slogan with margin -6.5em */
  display: block;
  position: static;
  z-index: 10;
/*  width: '. $page_width . $themebuilder_theme_layout .'; *//* 46x2=92em */
  height: 10px;
  padding: 0px 0px 0px 0px;
  border: none;
  margin: 0px 0px 0px 0px;
}
#skip-nav a,
#skip-nav a:link,
#skip-nav a:visited {
  display: block;
/*  width: 100%;  46x2=92em */
  height: 10px;
  padding: 0.2em;
  border: none;
  margin: 0px 0px 0px 0px;
 /* font-size: 5px; */
  text-indent: -1000px;
  background-color: transparent;
}
#skip-nav a:focus {
  z-index: 10;
  height: auto;
  white-space: nowrap;
/*  font-size: 1em; */
  text-align: center;
  text-indent: 0px;
  color: '. $themebuilder_a_link_color .';
  background-color: '. $themebuilder_a_link_backgroundcolor .';
/*  background-color: #027ac6; */
  border: 1px dashed white;
}

#skip-nav a:hover {
  z-index: 15;
  height: auto;
/*  font-size: 1em; */
  text-align: center;
  text-indent: 0px;
  color: '. $themebuilder_a_hover_color .';
  background: '. $themebuilder_a_hover_backgroundcolor .';
  border: 1px solid white;
}
#skip-nav a:active {
  z-index: 10;
  height: auto;
/*  font-size: 1em; */
  text-align: center;
  text-indent: 0px;
  white-space: nowrap;
  color: '. $themebuilder_a_active_color .';
  background-color: '. $themebuilder_a_active_backgroundcolor .';
/*  background-color: #027ac6; */
  border: 1px dashed white;
}

#skip-nav a:after {
  content:" [Alt] + [Shift] + [" attr(accesskey) "] + [Enter]";
}
#skip-nav a:before {
  content:"" attr(tabindex) " x [Tab]: ";
}
#skip-nav a.no-acc:after {
  content:" ";
}
#skip-nav a.no-tab:before {
  content:"";
}

#name-slogan {
  float: none;
  display: block;
  position: static;
  z-index: 8;
  height: 6.5em;
/* width: 39.4em; *//* for rounding error -0.1em */
  height: 6.5em; /* for IE and relative logo margin-top -6.25em (~100px) */
  padding: 0em 0em 0em 6.5em; /* 93px */
  border: 1px solid white;
  border: none;
  margin: 0px;
  background-color: transparent;
}
#name-slogan.no-logo {
  padding: 0em 0em 0em 1em;
}
#header-content {
  float: left;
  display: inline;
  position: relative;
  /* NOT clear: both; Crash in Opera 4 */
  z-index: 9;
  height: 6.5em;
/*  width: 47em; */
/*  min-width: 108px; */
/*  max-width: 988px; */
  padding: 0px;
  border: 1px solid white;
  border: none;
  margin: -6.5em 0px 0px 0px;
  background-color: transparent;
}
h1#site-name {
  clear: both;
  margin: 0em 0em 0.2em 0em; /* set NO padding value (for margin collapsing) in non floated elements! */
  font-size: 2em;
/*  line-height: 1.5em; */
  text-align: left;
/*  text-shadow: #1659ac 0.2em 0.1em 0.3em; */
  font-weight: bold;
  font-family: '. $themebuilder_h1_sitename_fontfamily .';
  color: '. $themebuilder_h1_sitename_color .';
  border: none;

}
p#site-slogan {
  font-family: '. $themebuilder_p_siteslogan_fontfamily .';
  color: '. $themebuilder_p_siteslogan_color .';
  margin: 0em;
/*  text-shadow: #1659ac 0.2em 0.1em 0.3em; */
}

/**
 * Logo
 */
#logo-title {
  position: relative;
}
#logo {
  float: left;
  display: inline; /*  Doubled float-margin bug Win IE, http://www.positioniseverything.net/explorer/doubled-margin.html */
  z-index: 3;
  width: '. $themebuilder_image_width_logo .'px;
  height: '. $themebuilder_image_height_logo .'px !important;
  padding: 0px 0px 0px 0px;
  border: none;
  margin: 0em 0em 0em 1em;
}
#logo a:link,
#logo a:visited,
#logo a:focus,
#logo a:hover,
#logo a:active {
  float: left;
  display: block;
  width: '. $themebuilder_image_width_logo .'px;
  height: '. $themebuilder_image_height_logo .'px;
  background-color: transparent;
}
/* Replace the print logo.png for screen transparent logo_screen.png */
img#logo-image {
  visibility: hidden;
}
div#logo {
  background: transparent url("images/logo_screen.png") no-repeat;
}

/**
 * ----------
 * Search
 * ----------
 */
/* hidde label and block title */
div#search-header h2,
div#search label {
  float: left;
  display: inline;
  width: auto;
  position: absolute;
  top: -100em;
  left: -100em;
}
div#search-header {
  float: right;
  display: inline;
  position: relative;
  width: auto;
  border: none;
  margin: 0em 1em 0em 0em;
}
div#search input.form-text {
  width: 7em;
  min-height: 22px;
  padding: 0.2em 0.2em 0.2em 24px;
  border: 1px solid gray;
  margin: 0px;
/*  color: #696969; */
/*  background: #58B3EC url("images/search.png") no-repeat scroll 0% 50%;  */
  background: white url("images/div_search_input_formtext.jpg") no-repeat scroll 0% 0%;
/*  background-color: #DCDCDC;
  background-image: url("images/div_search_input_formtext.jpg");
  background-repeat: no-repeat;
  background-attachment: scroll;
  background-position: 0% 50%;
    box-shadow: 0.2em 0.1em 0.3em #1659ac;
*/
}
div#search input.form-text:focus {
  width: 14em;
/*  color: black; */
  border: 1px solid green;
  background: #ffffd9 url("images/div_search_input_formtext_active.jpg") no-repeat scroll 0% 0%;
}
div#search input.form-text:hover {
/*  color: #027ac6; */
  border: 1px solid black; /* #027AC6 */
/*  background: white url("images/div_search_input_formtext_hover.jpg") no-repeat scroll 0% 0%; */
}
div#search input.form-text:active {
  width: 14em;
/*  color: black; */
  border: 1px solid green;
  background: #ffffd9 url("images/div_search_input_formtext.jpg") no-repeat scroll 0% 0%;
}
div#search input.form-submit {
  text-align: center;
}

/* submit */
div#search input.form-submit,
div#search input.form-text:visited {
  text-decoration: none;
  font-size: 0.9em;
  min-height: 22px;
/*  padding: 0.2em 0.5em 0.2em 0.5em;
  padding: 0.1em 0.2em; */
  padding: 0.2em 0.2em 0.2em 0.2em;
  border: 1px solid gray;
  margin: 0px;
  color: #000000;
/*   background-color: #58B3EC; */
}
div#search input.form-submit:focus {
  text-decoration: underline;
/*  color: white; */
  border: 1px dashed black;
/*  background-color: green; */
}
div#search input.form-submit:hover {
  cursor: pointer;
  text-decoration: underline;
/*  color: white; */
  border: 1px solid white;
/*  background-color: #027ac6; */
}
div#search input.form-submit:active {
  text-decoration: underline;
/*  color: white; */
  border: 1px dashed black;
/*  background-color: green; */
}

/**
 * ----------
 * Header blocks
 * ----------
 */
div#header_blocks {
  float: right;
  display: inline;
  position: relative;
  width: 20em;
   height: auto;
 /*  width: 10em;  for Opera 6 */
 /* height: 2em;  for Opera 6 */
  padding: 0px;
  border: none;
  margin: 0.2em 1em 0em 0em;
}
div#header_blocks div h2 {
  float: left;
  width: auto;
  display: inline;
  position: absolute;
  top: -100em;
  left: -100em;
}
div#header_blocks div div.content ul li {
  padding: 0px;
  margin: 0px;
  float: right;
  display: inline;
}
div#header_blocks a:link,
div#header_blocks a:visited {
  display: block;
  padding: 0.2em;
  margin: 0.2em;
  color: white;
}

/**
 * ------------------------------
 * Navigation
 * ------------------------------
 */
#navigation {
  float: none;
  display: block;
  position: static;
 /* clear: both; DO NOT USE! Crash in Opera 4 */
  padding: 0px 0px 0px 0px;
  border: none;
  margin: 0px 0px 0px 0px;
}
ul.links li {
  display:inline;
  list-style-type:none;
  padding: 0em 1em 0em 0em;
}
div.item-list ul.pager li {
margin: 0em 0em 0.2em 1em;
padding: 0em;
}

/**
 * ------------------------------
 * Primary navigation
 * ------------------------------
 */
#primary {
  /* structure */
  display: inline;
  float: left;
  /* clear: both; */

  /* layout */
  /* position: relative; */
  z-index: 5;
  /* NO WIDTH: Opera 4 crash!*/
  padding: 0px 0px 0px 0px;
  margin: 0px 0px 0px 0px;

  /* border */
  border: none;

  /* typo */
  font-weight: bold;

  /* color */
  /*  background-color: #58b3ec; */
  background-color: transparent;
}

#primary ul {
  position: relative;
 /*  width: 100%;  width: 760px; */
  padding: 0px 0px 0px 1em;
  margin: 0px 0px 0px 0px;
  border: none;
  background: transparent;
}
#primary li {
  float: left; /* For Mz 1.0, Opera 7 */
  /*  display: inline; */
  /* width: auto;  8.8em in hacks.css only for Opera 5 */
  /* background image in hacks.css only for Opera 5-6 */
  padding: 0em 0.2em 0em 0em;
  border: none;
  /* margin: 0px 0px 0px 0px;
  list-style-image: none;
  list-style-position: outside;
  list-style-type: none;
  -moz-border-radius-topleft: 0.8em;
  -moz-border-radius-topright: 0.8em;
  */
}

/* "#primary ul.links li a" for Opera 4 */
#primary ul.links li a,
#primary ul.links li a:link,
#primary ul.links li a:visited,
#primary ul.links li a:focus,
#primary ul.links li a:hover,
#primary ul.links li a:active {
  /* structure */
  display: block;

  /* layout */
  width: auto; /* For Opera 5-6 */
  padding: 8px 10px 8px 10px;

  /* border */
  /* border-top-left-radius: 0.8em;
  border-top-right-radius: 0.8em;  */
  /* Mozilla proprietary code
  -moz-border-radius-topleft: 0.8em;
  -moz-border-radius-topright: 0.8em;
   */

  /* background image */
  background-image: url("images/div_primary_a.jpg");
}

#primary ul.links li a,
#primary ul.links li a:link,
#primary ul.links li a:visited {
  /* border */
  border-width: 1px;
  border-style: solid;

  /* typo */
  text-decoration: none;

  /* color */
  color: '. $themebuilder_div_primary_a_link_color .';
  background-color: '. $themebuilder_div_primary_a_link_backgroundcolor .';
  border-color: '. $themebuilder_div_primary_a_link_color .';

  /* background image */
  background-image: url("images/div_primary_a.jpg");
  background-repeat: no-repeat;
  background-position: 0px 0px;
}

#primary ul.links li a:focus {
  /* border */
  border-width: 1px;
  border-style: solid;

  /* typo */
  text-decoration: underline;

  /* color */
  color: '. $themebuilder_div_primary_a_link_backgroundcolor .';
  background-color: '. $themebuilder_div_primary_a_link_color .';
  border-color: '. $themebuilder_div_primary_a_link_backgroundcolor .';

  /* background image */
  background-image: url("images/div_primary_a.jpg");
  background-repeat: no-repeat;
  background-position: 0px -200px;
}
#primary ul.links li a:hover {
  /* border */
  border-width: 1px;
  border-style: dashed;

  /* typo */
  text-decoration: underline;

  /* color */
  color: '. $themebuilder_div_primary_a_link_color .';
  // background-color: '. $themebuilder_div_primary_a_link_backgroundcolor .';
  border-color: '. $themebuilder_div_primary_a_link_color .';

  /* background image */
  background-image: url("images/div_primary_a.jpg");
  background-repeat: no-repeat;
  background-position: 0px -100px;
}

/* ID (#active) not class (.active) for IE 4-6 */
#primary ul.links li a.active,
#primary ul.links li a#active {
  /* border */
  border-width: 1px;
  border-style: solid;

  /* typo */
  text-decoration: none;

  /* color */
  color: '. $themebuilder_div_primary_a_link_backgroundcolor .';
  background-color: '. $themebuilder_div_primary_a_link_color .';
  border-color: '. $themebuilder_div_primary_a_link_backgroundcolor .';

  /* background image */
  background-image: url("images/div_primary_a.jpg");
  background-repeat: no-repeat;
  background-position: 0px -100px;
}
#primary ul.links li a:hover.active {
  cursor: crosshair;
}

#primary ul.links li a:active {
  /* border */
  border-width: 1px;
  border-style: solid;

  /* typo */
  text-decoration: underline;

  /* color */
  color: '. $themebuilder_div_primary_a_link_backgroundcolor .';
  background-color: '. $themebuilder_div_primary_a_link_color .';
  border-color: '. $themebuilder_div_primary_a_link_backgroundcolor .';

  /* background image */
  background-image: url("images/div_primary_a.jpg");
  background-repeat: no-repeat;
  background-position: 0px -200px;
}

/**
 * ------------------------------
 * Secondary navigation
 * ------------------------------
 */
#secondary {
  float: left;
  display: inline;
  position: relative;
  z-index: 6;
/*  width: 47em; */
/*  min-width: 108px;
/*  max-width: 988px; */
  padding: 0px 0px 0px 0px;
  margin: 0px 0px 0px 0px;
  font-weight: normal;
/*  font-size: 0.9em;
  border-top: 1px solid #0c4995; #6191C5
  background-color: #2763A5;
  background-color: #0c4995;  */
  background-color: '. $themebuilder_div_secondary_a_link_backgroundcolor .';
}
#secondary ul {
  position: relative;
  width: 100%; /* width: 760px; */
  padding: 0px 0px 0px 0px;
  border: none;
  margin: 0px 0px 0px 0px;
  background: transparent;
}
#secondary li {
  float: left; /* For Mz 1.0, Opera 7 */
  display: inline;
  width: auto; /* 9.3em in hacks.css only for Opera 5 */
  /* background image in hacks.css only for Opera 5-6 */
  padding: 0px 0px 0px 0px;
  border: none;
  margin: 0px 0px 0px 0px;
  list-style-image: none;
  list-style-position: outside;
  list-style-type: none;
}
/* "#primary ul.links li a" for Opera 4 */
#secondary ul.links li a,
#secondary ul.links li a:link,
#secondary ul.links li a:visited,
#secondary ul.links li a:focus,
#secondary ul.links li a:hover,
#secondary ul.links li a:active,
#secondary li a,
#secondary li a:link,
#secondary li a:visited,
#secondary li a:focus,
#secondary li a:hover,
#secondary li a:active {
  display: block;
  width: auto; /* For Opera 5-6 */
  padding: 0px 10px 0px 10px;
  border-top: 1px solid #0c4995; /* For NN4 */
  border-right: 1px dashed white;
  border-bottom: 1px solid #0c4995;
  border-left: 1px solid #0c4995;
  background-image: none;
}
#secondary ul.links li a,
#secondary ul.links li a:link,
#secondary ul.links li a:visited,
#secondary li a:link,
#secondary li a:visited {
  /* border */
  border-width: 1px;
  border-style: solid;

  /* typo */
  text-decoration: none;

  /* color */
  color: '. $themebuilder_div_primary_a_link_color .';
  background-color: '. $themebuilder_div_secondary_a_link_backgroundcolor .';
  border-color: '. $themebuilder_div_secondary_a_link_color .';

  /* background image */
  background-image: url("images/div_primary_a.jpg");
  background-repeat: no-repeat;
  background-position: 0px 0px;
}
#secondary li a:focus,
#secondary ul.links li a:focus {
  /* border */
  border-width: 1px;
  border-style: solid;

  /* typo */
  text-decoration: none;

  /* color */
  color: '. $themebuilder_div_secondary_a_link_backgroundcolor .';
  background-color: '. $themebuilder_div_secondary_a_link_color .';
  border-color: '. $themebuilder_div_secondary_a_link_backgroundcolor .';

  /* background image */
  background-image: url("images/div_primary_a.jpg");
  background-repeat: no-repeat;
  background-position: 0px -200px;
}
#secondary li a:hover,
#secondary ul.links li a:hover {
  /* border */
  border-width: 1px;
  border-style: dashed;

  /* typo */
  text-decoration: none;

  /* color */
  color: '. $themebuilder_div_secondary_a_link_backgroundcolor .';
  background-color: '. $themebuilder_div_secondary_a_link_color .';
  border-color: '. $themebuilder_div_secondary_a_link_backgroundcolor .';

  /* background image */
  background-image: url("images/div_primary_a.jpg");
  background-repeat: no-repeat;
  background-position: 0px -100px;
}

/* ID (#active) not class (.active) for IE 4-6 */
#secondary ul.links li a.active,
#secondary li a.active,
#secondary li a#active {
  /* border */
  border-width: 1px;
  border-style: solid;

  /* typo */
  text-decoration: none;

  /* color */
  color: '. $themebuilder_div_secondary_a_link_backgroundcolor .';
  background-color: '. $themebuilder_div_secondary_a_link_color .';
  border-color: '. $themebuilder_div_secondary_a_link_backgroundcolor .';

  /* background image */
  background-image: url("images/div_primary_a.jpg");
  background-repeat: no-repeat;
  background-position: 0px -100px;

  cursor: crosshair;
}

#secondary li a:active,
#secondary ul.links li a:active {
  /* border */
  border-width: 1px;
  border-style: solid;

  /* typo */
  text-decoration: none;

  /* color */
  color: '. $themebuilder_div_secondary_a_link_backgroundcolor .';
  background-color: '. $themebuilder_div_secondary_a_link_color .';
  border-color: '. $themebuilder_div_secondary_a_link_backgroundcolor .';

  /* background image */
  background-image: url("images/div_primary_a.jpg");
  background-repeat: no-repeat;
  background-position: 0px -200px;
}

/**
 * --------------------
 * Main
 * --------------------
 */
#main {
  /* structure */
  display: block;
  float: none;

  /* layout */
  position: static;
  padding: 0px 0px 0px 0px;
  margin: 0px 0px 0px 0px;

  /* border */
  border-width: 1px;
  border-style: solid; /* 1px for NN 6 */ 

  /* typo */
  text-align: left;

  /* color */
  background-color: '. $themebuilder_div_sidebar_backgroundcolor .';
  border-color: '. $themebuilder_div_sidebar_bordercolor .';

/*  width: 47em; */
/*  height: auto; */
/*  min-width: 108px; */
/*  max-width: 988px; */
}

/**
 * ----------
 * Sidebars
 * ----------
 */
div#sidebar-left h2,
div#sidebar-right h2 {
  /* color */
  color: '. $themebuilder_div_sidebar_color .';
}
div#sidebar-left a:link,
div#sidebar-left a:visited,
div#sidebar-right a:link,
div#sidebar-right a:visited {
  /* color */
  color: '. $themebuilder_div_sidebar_color .';
  background-color: '. $themebuilder_div_sidebar_backgroundcolor .';
}

/**
 * ----------
 * Sidebar left
 * ----------
 */
div#sidebar-left {
  /* structure */
  display: inline;
  float: left;
  clear: none;

  /* layout */
  position: relative;
  width: '. $themebuilder_left_width . $themebuilder_theme_layout .';
  height: auto;
  /*  min-width: '. themebuilder_calculate($themebuilder_left_width, 'min') . $theme_min_suffix .'; */
  /*  max-width: '. themebuilder_calculate($themebuilder_left_width, 'max') . $theme_max_suffix .'; */

  /* border */
  border-width: '. $themebuilder_div_sidebar_borderwidth .';
  border-style: '. $themebuilder_div_sidebar_borderstyle .'; /* 1px for NN 6 */ 

  /* typo */
  font-family: '. $themebuilder_div_sidebar_fontfamily .';
  line-height: '. $themebuilder_div_sidebar_lineheight .';
  font-size: '. $themebuilder_div_sidebar_fontsize .';
  text-align: '. $themebuilder_div_sidebar_textalign .'; /* Center of layout for IE 5 */

  /* color */
  color: '. $themebuilder_div_sidebar_color .';
  background-color: '. $themebuilder_div_sidebar_backgroundcolor .';
  border-color: '. $themebuilder_div_sidebar_bordercolor .';

  /* background image */
  background-image: url("images/div_sidebar.jpg");
  background-repeat: '. $themebuilder_div_sidebar_backgroundrepeat .'; /* "repeat" for NN 4*/
  background-position: '. $themebuilder_div_sidebar_backgroundposition .'; /* Not for NN 4 */
  background-attachment: '. $themebuilder_div_sidebar_backgroundattachment .';
}


/* With 3 columns, require a minimum width of 1000px to ensure there is enough horizontal space. */
body.two-sidebars {
/*  min-width: 980px; */
}
/* With 2 columsn, require a minimum width of 800px. */
body.sidebar-left, body.sidebar-right {
/* min-width: 780px; */
}
/* We ensure the sidebars are still clickable using z-index */

form#user-login-form {
/*  text-align: left; */
}

/**
 * ----------
 * Content
 * ----------
 */

#content p {
  background-color: white; /* for IE 5 Mac*/
}
#content img {
  border: none;
}
/* We must define 100% width to avoid the body being too narrow for near-empty pages */

div#content {
  /* structure */
  display: inline;
  float: left;
  clear: none;

  /* layout */
  position: static;
  top: 0px;
  left: 0px;
  width: '. $themebuilder_content_width . $themebuilder_theme_layout .'; /* for IE 5.0: 35-1-1=33em -0.5 for rounding errors = 32.5em */
  /*  min-width: '. themebuilder_calculate($themebuilder_content_width, 'min') . $theme_min_suffix .'; */
  /*  max-width: '. themebuilder_calculate($themebuilder_content_width, 'max') . $theme_max_suffix .'; */
  height: auto;
  padding: 0;
  margin: 0px;

  /* border */
  border-width: '. $themebuilder_div_content_borderwidth .';
  border-style: '. $themebuilder_div_content_borderstyle .'; /* 1px for NN 6 */ /* DO NOT USE "border: none;" for NN 6, Beonex 0.6 */

  /* typo */
  font-family: '. $themebuilder_div_content_fontfamily .';
  line-height: '. $themebuilder_div_content_lineheight .';
  font-size: '. $themebuilder_div_content_fontsize .';
  text-align: '. $themebuilder_div_content_textalign .'; /* Center of layout for IE 5 */

  /* color */
  color: '. $themebuilder_div_content_color .';
  background-color: '. $themebuilder_div_content_backgroundcolor .';
  border-color: '. $themebuilder_div_content_bordercolor .';

  /* background image */
  background-image: url("images/div_content.jpg");
  background-repeat: '. $themebuilder_div_content_backgroundrepeat .'; /* "repeat" for NN 4*/
  background-position: '. $themebuilder_div_content_backgroundposition .'; /* Not for NN 4 */
  background-attachment: '. $themebuilder_div_content_backgroundattachment .';
}

/* So we move the #main container over the sidebars to compensate */
body.sidebar-left #content {
/*  width: 36em; */
  float: right;
  display: inline;
  position: relative;
/*   width: auto;
  margin-left: 12em; */
 border-right: 1px solid transparent; /* DO NOT USE "border: none;" for NN 6, Beonex 0.6 */
}
/* So we move the #main container over the sidebars to compensate */
body.sidebar-right #content {
/*  width: 36em; 46-12=37em */
  float: left;
  display: inline;
  position: relative;
/*   width: auto;
  margin-right: 12em; */
  border-left: 1px solid transparent; /* DO NOT USE "border: none;" for NN 6, Beonex 0.6 */
}

/**
 * -----
 * Content, Breadcrumb, mission
 * -----
 */
.breadcrumb {
  margin: 0;
  padding:0;
}
#mission {
  padding: 5px;
  background-color: #EEE;
  border: 1px solid #CCC;
}
div.messages {
  min-height: 32px;                 /* icon height */
  padding: 1em 1em 1em 40px; /* zen icons 52px; */
/*  border: 2px solid #ddd;
  background: #eee;
*/  margin: 10px 0;
}
.messages ul {
/*  padding: 0 0 0 20px;
  margin: 0;
*/}
div#content div.help {
  padding: 1em;
  margin: 10px 0pt;
}
div#content div.help {
  background-image: none;
  background-color: white;
  border: 1px dashed #CCC;
  color: black;
}

/**
 * ----------
 * Sidebar right
 * ----------
 */
div#sidebar-right {
  /* structure */
  display: inline;
  float: right;
  clear: none;

  /* layout */
  position: relative;
  width: '. $themebuilder_right_width . $themebuilder_theme_layout .';
  height: auto;
  /*  min-width: '. themebuilder_calculate($themebuilder_right_width, 'min') . $theme_min_suffix .'; */
  /*  max-width: '. themebuilder_calculate($themebuilder_right_width, 'max') . $theme_max_suffix .'; */

  /* border */
  border-width: '. $themebuilder_div_sidebar_borderwidth .';
  border-style: '. $themebuilder_div_sidebar_borderstyle .'; /* 1px for NN 6 */ 

  /* typo */
  font-family: '. $themebuilder_div_sidebar_fontfamily .';
  line-height: '. $themebuilder_div_sidebar_lineheight .';
  font-size: '. $themebuilder_div_sidebar_fontsize .';
  text-align: '. $themebuilder_div_sidebar_textalign .'; /* Center of layout for IE 5 */

  /* color */
  color: '. $themebuilder_div_sidebar_color .';
  background-color: '. $themebuilder_div_sidebar_backgroundcolor .';
  border-color: '. $themebuilder_div_sidebar_bordercolor .';

  /* background image */
  background-image: url("images/div_sidebar.jpg");
  background-repeat: '. $themebuilder_div_sidebar_backgroundrepeat .'; /* "repeat" for NN 4*/
  background-position: '. $themebuilder_div_sidebar_backgroundposition .'; /* Not for NN 4 */
  background-attachment: '. $themebuilder_div_sidebar_backgroundattachment .';
}

.sidebar {
/*  margin: 0;
  float: left;
  display: inline;
  z-index: 2;
  position: relative;
*/}

/* If the content is set to print bevore the sidebar left */
/* $themebuilder_color_sidebar */

.sidebar .block {
  border-top: '. $themebuilder_div_sidebar_borderwidth . $themebuilder_div_sidebar_borderstyle . $themebuilder_div_sidebar_bordercolor .';
  border-right: '. $themebuilder_div_sidebar_borderwidth . $themebuilder_div_sidebar_borderstyle . $themebuilder_div_sidebar_bordercolor .';
  border-bottom: '. $themebuilder_div_sidebar_borderwidth . $themebuilder_a_link_color. $themebuilder_div_sidebar_bordercolor .';
  border-left: '. $themebuilder_div_sidebar_borderwidth . $themebuilder_div_sidebar_borderstyle . $themebuilder_div_sidebar_bordercolor .';
}

#sidebar-left .block {
//   padding: 10px;
}

#sidebar-right .block {
//   padding: 10px;
}

.block .content {
/* margin: 0.5em 0; */
}
.block ul {
//   margin: 0em;
//   padding: 0.2em 0em 0.2em 0.7em;
}

/**
 * --------------------
 * Footer
 * --------------------
 */
#footer {
  /* structure */
  display: block;
  float: none;
  clear: none;

  /* layout */
  position: static;
  /* width: '. $themebuilder_content_width . $themebuilder_theme_layout .'; */
  /*  min-width: '. themebuilder_calculate($themebuilder_content_width, 'min') . $theme_min_suffix .'; */
  /*  max-width: '. themebuilder_calculate($themebuilder_content_width, 'max') . $theme_max_suffix .'; */
  height: auto;
  padding:  0px 0px 0px 0px;
  margin: 0px 0px 0px 0px;

  /* border */
  border-width: '. $themebuilder_div_sidebar_borderwidth .';
  border-style: '. $themebuilder_div_footer_borderstyle .'; /* 1px for NN 6 */

  /* typo */
  font-family: '. $themebuilder_div_footer_fontfamily .';
  line-height: '. $themebuilder_div_footer_lineheight .';
  font-size: '. $themebuilder_div_footer_fontsize .';
  text-align: '. $themebuilder_div_footer_textalign .'; /* Center of layout for IE 5 */

  /* color */
  color: '. $themebuilder_div_footer_color .';
  background-color: '. $themebuilder_div_footer_backgroundcolor .';
  border-color: '. $themebuilder_div_footer_bordercolor .';

  /* background image */
  background-image: url("images/div_footer.jpg");
  background-repeat: '. $themebuilder_div_footer_backgroundrepeat .'; /* "repeat" for NN 4*/
  background-position: '. $themebuilder_div_footer_backgroundposition .'; /* Not for NN 4 */
  background-attachment: '. $themebuilder_div_footer_backgroundattachment .';

}
#footer p {
  text-align: right;
  font-size: 0.8em;
  padding: 0 1em;
}
#footer div h2 {
  float: left;
  width: auto;
  display: inline;
  position: absolute;
  top: -100em;
  left: -100em;
}
#footer ul li,
#footer ol li {
  float: right;
  display: inline;
  list-style-type: none;
  padding: 0 10px;
  font-size: 0.8em;
}
.valid,
ul li.valid {
  list-style-type: none;
  padding: 3px 4px 3px 13px;
  margin: 0px;
  background: url("images/emblem-default8.png") no-repeat 0 50%;
}

/**
 * --------------------
 * Form
 * --------------------
 */
div#content form {
  /*line-height: 1.4em; */
  border: 0 solid #fff;
}
div#content fieldset {
  font-family: '. $themebuilder_div_content_fontfamily .';
  /*color: #006699;
  font-weight: bold;*/
  border-top: 1px solid #e2e2e2;
  border-right: 0 solid #fff;
  border-bottom: 0 solid #fff;
  border-left: 0 solid #fff;
  text-align: left;
}
/* legend */
fieldset legend {
  border-top: 1px solid #e2e2e2;
  border-left: 1px solid #e2e2e2;
  border-bottom: 1px solid transparent;
  border-right: 1px solid #e2e2e2;
  border-style: solid;
  font-weight: bold;
  color: black;
  font-size: 0.88em;
  font-weight: bold;
  color: #666;
  background-color: transparent;
}

/**
 * Form links
 */
/* Links (Note: Do not use "a {...}" for NN 4) */
div#content fieldset legend a:link {
  /* border */
  border-width: '. $themebuilder_a_link_borderwidth .';
  border-style: '. $themebuilder_a_link_borderstyle .';
  /* typo */
  text-decoration: '. $themebuilder_a_link_textdecoration .';
  /* color */
  color: '. $themebuilder_a_link_color .';
  background-color: '. $themebuilder_a_link_backgroundcolor .';
}
div#content fieldset legend a:visited {
  /* border */
  border-width: '. $themebuilder_a_visited_borderwidth .';
  border-style: '. $themebuilder_a_visited_borderstyle .';
  /* typo */
  text-decoration: '. $themebuilder_a_visited_textdecoration .';
  /* color */
  color: '. $themebuilder_a_visited_color .';
  background-color: '. $themebuilder_a_visited_backgroundcolor .';
}
div#content fieldset legend a:focus {
  /* border */
  border-width: '. $themebuilder_a_focus_borderwidth .';
  border-style: '. $themebuilder_a_focus_borderstyle .';
  /* typo */
  text-decoration: '. $themebuilder_a_focus_textdecoration .';
  /* color */
  color: '. $themebuilder_a_focus_color .';
  background-color: '. $themebuilder_a_focus_backgroundcolor .';
}
div#content fieldset legend a:hover {
  /* border */
  border-width: '. $themebuilder_a_hover_borderwidth .';
  border-style: '. $themebuilder_a_hover_borderstyle .';
  /* typo */
  text-decoration: '. $themebuilder_a_hover_textdecoration .';
  /* color */
  color: '. $themebuilder_a_hover_color .';
  background-color: '. $themebuilder_a_hover_backgroundcolor .';
}
div#content fieldset legend a:active {
  /* border */
  border-width: '. $themebuilder_a_active_borderwidth .';
  border-style: '. $themebuilder_a_active_borderstyle .';
  /* typo */
  text-decoration: '. $themebuilder_a_active_textdecoration .';
  /* color */
  color: '. $themebuilder_a_active_color .';
  background-color: '. $themebuilder_a_active_backgroundcolor .';
}





fieldset legend[title]:hover {
  cursor: help;
}

/**
 * Label
 */
div#content label {
  text-align: left;
  font-weight: normal;
  color: #000;
}
div#content label:hover {
  cursor: pointer;
}
div#content label:active,
div#content label:focus {
  cursor: crosshair;
}

/**
 * Input, Textarea, Select
 */
input,
textarea,
select {
/*	font-family: Times, "Times New Roman"; */
  font-weight: normal;
  border: 0.1em solid #e2e2e2;
  background: #fff url("images/input.jpg") top repeat-x;
}
input#edit-color {
  background: none;
}
textarea,
input {
 padding: 0.2em
}
input:hover,
textarea:hover,
select:hover {
  cursor: pointer;
  color: #000;
  background: #fff url("images/input.jpg") top repeat-x;
  border: 0.1em solid #027AC6;
}
input:active,
textarea:active,
input:focus,
textarea:focus,
select:hover {
  cursor: text;
  color: #000;
  border: 0.1em solid green ;
  background: #ffffd9 url("images/input_active.jpg") top repeat-x;
}

/**
 * Form buttons
 */
.form-submit,
#edit-submit,
#edit-preview,
#edit-reset,
#edit-delete {
  font-weight: bold;
  font-size: 0.9em;
  font-family: '. $themebuilder_div_content_fontfamily .';
  text-align: center;
  padding: 0.2em 0.5em;
  background: #e2e2e2 url("images/input_form_submit.jpg") top repeat-x;
  border-width: 0.2em;
  border-style: outset;
  cursor: pointer;
}
.form-submit,
#edit-submit {
  color: green;
}
#edit-preview {
  color: #027AC6;
}
#edit-reset,
#edit-delete {
  color: red;
}

.form-submit:link,
.form-submit:visited,
div#content #edit-submit:link,
div#content #edit-submit:visited,
div#content #edit-preview:link,
div#content #edit-preview:visited {
  text-decoration: underline;
  cursor: pointer;
  color: #077101;
  background: #e2e2e2 url("images/input_editsubmit.jpg") top repeat-x;
  border-color: #e2e2e2;
  border-width: 0.2em;
  border-style: outset;
}
div#content #edit-reset:link,
div#content #edit-reset:visited,
div#content #edit-delete:link,
div#content #edit-delete:visited {
  text-decoration: none;
  cursor: pointer;
  color: red;
  background: red url("images/input_editreset.jpg") top repeat-x;
  border-color: #e2e2e2;
  border-width: 0.2em;
  border-style: outset;
}
.form-submit:hover,
div#content #edit-submit:hover {
  text-decoration: underline;
  color: #fff;
  background: green url("images/input_editsubmit_hover.jpg") bottom repeat-x;
  border-color: green;
  border-width: 0.2em;
  border-style: outset;
  cursor: pointer;
}
div#content #edit-preview:hover {
  text-decoration: underline;
  color: #fff;
  background: #027AC6 url("images/input_editsubmit_hover.jpg") bottom repeat-x;
  border-color: #027AC6;
  border-width: 0.2em;
  border-style: outset;
  cursor: pointer;
}
div#content #edit-reset:hover,
div#content #edit-delete:hover {
  text-decoration: underline;
  color: #fff;
  background: red url("images/input_editreset_hover.jpg") top repeat-x;
  border-color: #CD000A;
  border-width: 0.2em;
  border-style: outset;
  cursor: pointer;
}

.form-submit:active,
.form-submit:focus,
div#content #edit-submit:active,
div#content #edit-submit:focus {
  text-decoration: none;
  color: #fff;
  background: #027AC6  url("images/input_editsubmit_active.jpg") top repeat-x;
  border-color: #027AC6 ;
  border-width: 0.2em;
  border-style: inset;
}

div#content #edit-preview:active,
div#content #edit-preview:focus {
  text-decoration: none;
  color: #fff;
  background: green url("images/input_editsubmit_active.jpg") top repeat-x;
  border-color: green;
  border-width: 0.2em;
  border-style: inset;
}
div#content #edit-reset:active,
div#content #edit-reset:focus,
div#content #edit-delete:active,
div#content #edit-delete:focus {
  text-decoration: none;
  color: red;
  background: #fff532 url("images/input_editreset_active.jpg") top repeat-x;
  border-color: #000;
  border-width: 0.2em;
  border-style: inset;
}

/**
 * Form classes
 */
div#content .required {
/*	color: #CD000A;
  border-color: #CD000A;
*/}
span.required:hover,
span.form-required:hover,
div.required:hover,
div.form-required:hover{
  cursor: help;
}

/**
 * --------------------
 * Status and Error
 * --------------------
 */
.status {
  color: #3a3;
  border: 2px solid #3a3;
  background-color: #c7f2c8;
  color: #3a3;
  background-image: url("../../../../misc/watchdog-ok.png");
  background-repeat: no-repeat;
  background-position: .5em 50%;
}

.warning,
div.warning {
display: inline;
  background-image: url("../../../../misc/watchdog-warning.png");
  background-repeat: no-repeat;
  background-position: 0% 50%;
}
.error,
div.error {
  background-image: url("../../../../misc/watchdog-error.png");
  background-repeat: no-repeat;
  background-position: 0% 50%;
}

/**
 * --------------------
 * For developers
 * --------------------
 */
.float {
  float: left;
  display: inline;
  position: relative;
  width: 5em;
  background-color: yellow;
  border: 1px solid red;
}
#float_none {
  float: none;
  display: block;
  position: static;
  clear: both;
  width: 30em;
  height: auto;
  padding: 0px 0px 0px 0px;
  border: none;
  margin: 0px 0px 0px 0px;
}


/**
 * --------------------
 * Table
 * --------------------
 */
table {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  table-layout: auto;
  border: 1px solid #ccc;
  background-color: #ffffff;
  empty-cells: show;
}
caption {
  caption-side: top;
  text-align: center;
  color: '. $themebuilder_caption_color .';
  font-weight: bold;
  background: '. $themebuilder_caption_backgroundcolor .' url("images/div_head.jpg") repeat-x scroll left top;
  border-top: 1px solid #ccc;
  border-right: 1px solid #ccc;
  border-bottom: none;
  border-left: 1px solid #ccc;
}

thead th {
  text-transform: uppercase;
  text-align: center;
  font-family: "Bitstream Vera Sans", Verdana, Helvetica, Arial, sans-serif;
  color: '. $themebuilder_thead_color .';
  background-color: '. $themebuilder_thead_backgroundcolor .';
/*  background: #003095  url("images/thead.jpg") top repeat-x;*/
  border-bottom: none;
}
tfoot td {
  text-align: center;
  color: '. $themebuilder_tfoot_color .';
  background-color: '. $themebuilder_tfoot_backgroundcolor .';
/*  background: #ECECEC url("images/tfoot.jpg") top repeat-x;*/
}
tbody td {
  border-bottom: 1px solid #e2e2e2;
  vertical-align: center;
  text-align: left;
  border-left: 1px solid #dadada;
}
tbody th {
  border-bottom: 1px solid #e2e2e2;
  vertical-align: top;
  text-align: left;
  color: '. $themebuilder_div_content_color .';
  background-color: #ECECEC;
/*  background: #f1f1f1 url("images/table_horizontal.jpg") top left repeat-y;*/
}
tr.center td {
  text-align: center;
}
tr.center td.text_left {
  text-align: left;
}
tr.center td.text_right {
  text-align: right;
}
tr.odd {
  background-color: #f1f1f1;
}
tr.even {
  background-color: #fff;
}
table thead tr:hover,
table thead tr th:hover  {
  color: '. $themebuilder_a_hover_color .';
  background-color: '. $themebuilder_a_hover_backgroundcolor .';
/*  background: #027ac6 url("images/thead_hover.jpg") top repeat-x;*/
}

table tbody tr:hover,
table tbody tr:hover h2 {
  color: '. $themebuilder_a_hover_color .';
}
table tbody tr:hover,
table tbody th:hover {
  color: '. $themebuilder_a_hover_color .';
  background-color: '. $themebuilder_a_hover_backgroundcolor .';
/*  background: #027ac6 url("images/table_hover.jpg") top repeat-x;*/
}

table tfoot tr:hover,
table tfoot td:hover {
  color: '. $themebuilder_a_hover_color .';
  background-color: '. $themebuilder_a_hover_backgroundcolor .';
/*  background: #027ac6 url("images/table_hover.jpg") top repeat-x;*/
}
table tfoot td:hover,
table tfoot th:hover {
  color: '. $themebuilder_a_hover_color .';
}
table thead th:hover,
table thead th:hover {
  font-weight: bold;
  color: '. $themebuilder_a_hover_color .';
  background-color: '. $themebuilder_a_hover_backgroundcolor .';
}
table tfoot td:hover,
table tfoot td:hover {
  font-weight: normal;
  color: '. $themebuilder_a_hover_color .';
  background-color: '. $themebuilder_a_hover_backgroundcolor .';
}
table tbody td:hover,
table tbody td:hover {
//   font-weight: bold;
//   font-size: 120%;
  color: '. $themebuilder_a_hover_color .';
  background-color: '. $themebuilder_a_hover_backgroundcolor .';
}

table thead th:focus,
table thead th:active, 
table tfoot td:focus,
table tfoot td:active,
table tbody td:focus,
table tbody td:active {
//   font-weight: bold;
//   font-size: 120%;
  color: '. $themebuilder_a_focus_color .';
  background-color: '. $themebuilder_a_focus_backgroundcolor .';
/*  background: #027ac6 url("images/table_hover.jpg") top repeat-x;*/
}

.yes_image {
  display: block;
  background: transparent url("images/yes.gif") center no-repeat;
  text-indent:  -999em;
  cursor: help;
}
.yes_image:hover {
  background: transparent url("images/yes_hover.gif") center no-repeat;
  cursor: help;
}
.no_image {
  display: block;
  background: transparent url("images/no.gif") center no-repeat;
  text-indent:  -999em;
  cursor: help;
}
.no_image:hover {
  background: transparent url("images/no_hover.gif") center no-repeat;
  cursor: help;
}
.unknown_image {
  display: block;
  background: transparent url("images/unknown.gif") center no-repeat;
  text-indent:  -999em;
  cursor: help;
}

.price:before  {
  content: "\0043\0048\0046\ ";
}
.price:after  {
  content: ".—"; 
}
.price1:before {
  content: "\0043\0048\0046\ ";
}
.price2:after {
  content: ".—"; 
}

/* Links relation and reverse */
a[rel] {
  text-decoration: underline;
}
a[rel]:visited {
  color: #003095;
text-decoration: underline;
}
a[rel]:hover {
  color: #fff;
  text-decoration: underline;
}
a[rel] {
  vertical-align: middle;
  text-decoration: underline;
}

a[rel="alternate"]:after,
a[rev="alternate"]:after {
  content: url("images/rel_alternate.png");
}
a[rel="author"]:after,
a[rev="author"]:after {
  content: url("images/rel_author.png");
}
a[rel="appendix"]:after,
a[rev="appendix"]:after {
  content: url("images/rel_appendix.png");
}
a[rel="bookmark"]:after,
a[rev="bookmark"]:after {
  content: url("images/rel_bookmark.png");
}
a[rel="chapter"]:after,
a[rev="chapter"]:after {
  content: url("images/rel_chapter.png");
}
a[rel="contents"]:after,
a[rev="contents"]:after {
  content: url("images/rel_contents.png");
}
a[rel="copyright"]:after,
a[rev="copyright"]:after {
  content: url("images/rel_copyright.png");
}
a[rel="glossary"]:after,
a[rev="glossary"]:after {
  content: url("images/rel_glossary.png");
}
a[rel="help"]:after,
a[rev="help"]:after {
  content: url("images/rel_help.png");
}
a[rel="index"]:after,
a[rev="index"]:after {
  content: url("images/rel_index.png");
}
a[rel="next"]:after,
a[rev="next"]:after {
  content: url("images/rel_next.png");
}
a[rel="prev"]:after,
a[rev="pref"]:after {
  content: url("images/rel_prev.png");
}
a[rel="section"]:after,
a[rev="section"]:after {
  content: url("images/rel_section.png");
}
a[rel="start"]:after,
a[rev="start"]:after {
  content: url("images/rel_start.png");
}
a[rel="subsection"]:after,
a[rev="subsection"]:after {
  content: url("images/rel_subsection.png");
}

/* Links internal and external */
a.internal:after {
  content: url("images/internal.png");
  vertical-align: middle;
}
a.internal.icon8:after {
  content: url("images/internal8.png");
  vertical-align: middle;
}
a.external:after {
  content: url("images/external.png");
  vertical-align: middle;
}
a.external.icon8:after {
  content: url("images/external8.png");
  vertical-align: middle;
}
a.skip_up {
  padding: 3px 4px 3px 23px;
  background-image: url("images/skip_up.png");
  background-repeat: no-repeat;
  background-position: 0px 50%;
}
a.skip_first {
  padding: 3px 4px 3px 23px;
  background-image: url("images/skip_first.png");
  background-repeat: no-repeat;
  background-position: 0 50%;
  
}
a.skip_last {
  padding: 3px 4px 3px 23px;
  background-image: url("images/skip_last.png");
  background-repeat: no-repeat;
  background-position: 0px 50%;
}

/* Mimetypes */
.text, .css, .javascript {
  padding: 3px 4px 3px 23px;
  background-image: url("images/x-text-generic.png");
  background-repeat: no-repeat;
  background-position: 0px 50%;
}
.html, .xhtml, .php {
  padding: 3px 4px 3px 23px;
  background-image: url("images/x-text-html.png");
  background-repeat: no-repeat;
  background-position: 0px 50%;
}
.image {
  padding: 3px 4px 3px 23px;
  background-image: url("images/x-image.png");
  background-repeat: no-repeat;
  background-position: 0px 50%;
}
.document, .word, .pdf, .oasis_word, .oasis_text {
  padding: 3px 4px 3px 23px;
  background-image: url("images/x-office-document.png");
  background-repeat: no-repeat;
  background-position: 0px 50%;
}
.ods, .spread, .spreadsheet, .oasis_spreadsheet {
  padding: 3px 4px 3px 23px;
  background-image: url("images/x-office-spreadsheet.png");
  background-repeat: no-repeat;
  background-position: 0px 50%;
}
.odp, .presentation, .oasis_presentation {
  padding: 3px 4px 3px 23px;
  background-image: url("images/x-office-presentation.gif");
  background-repeat: no-repeat;
  background-position: 0px 50%;
}
.odg, .drawing, .oasis_drawing {
  padding: 3px 4px 3px 23px;
  background-image: url("images/x-office-drawing.png");
  background-repeat: no-repeat;
  background-position: 0px 50%;
}


';

if ($themebuilder_style_shadow_sitename == 1) {
$output .= '/**
 * ----------
 * Not valid in W3C CSS 1, 2.1 validator
 * ----------
 */
h1#site-name {
  text-shadow: #1659ac 0.2em 0.1em 0.3em;
}
';
}
if ($themebuilder_style_shadow_siteslogan == 1) {
$output .= 'p#site-slogan {
  text-shadow: #1659ac 0.2em 0.1em 0.3em;
}
';
}

$output .= themebuilder_code_errors('theme');
$output .= '/* ]]> */
-->
';



  return $output;
}