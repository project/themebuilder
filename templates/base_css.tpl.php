<?php

/**
 * Generate the css code base.css
 */
function generate_basecss() {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';

$output = '/* $Id$ */
<!--
/* <![CDATA[ */

/**
 * generator:      '. $themebuilder_meta_generator .'
 * author:         '. $themebuilder_meta_author .'
 * email:          '. $themebuilder_theme_email .'
 * website:        '. $themebuilder_theme_url .'
 * last modified:  '. date("Y-m-d") .'T'. date("H:i:s") . $themebuilder_theme_timezone .'
 * license:        '. $themebuilder_meta_copyright .'
 *
 * base color 1 '. $color .'
 * base color 2 '. $themebuilder_basecolor2 .'
 * base color 3 '. $themebuilder_basecolor3 .'
 * base color 4 '. $themebuilder_basecolor4 .'
 */
 
/**
 * Generic elements
 */
body {
  color: '. $themebuilder_body_color .';
  background-color: '. $themebuilder_body_backgroundcolor .';
  font-family: '. $themebuilder_typo_content_fontfamily .';
}
img {
  border: none;
}
blockquote,
cite,
q {
  font-style: italic;
}

/* Links (Note: Do not use "a {...}" for NN 4) */
a:link {
  text-decoration: underline;
  color: '. $themebuilder_color_link .';
  background-color: '. $themebuilder_color_link_bg .';
}
a:visited {
  text-decoration: underline;
  color: '. $themebuilder_color_link_visited .';
  background-color: '. $themebuilder_color_link_visited_bg .';
}
a:focus {
  color: '. $themebuilder_color_link_focus .';
  background-color: '. $themebuilder_color_link_focus_bg .';
}
a:hover {
  text-decoration: none;
  color: '. $themebuilder_color_link_hover .';
  background-color: '. $themebuilder_color_link_hover_bg .';
}
a:active {
  text-decoration: underline;
  color: '. $themebuilder_color_link_active .';
  background-color: '. $themebuilder_color_link_active_bg .';
}

/**
 * IDs
 */
h1#site-name {
  font-weight: bold;
  font-family: '. $themebuilder_typo_sitename_fontfamily .';
  color: '. $themebuilder_color_sitename .';
  border: none;
}
p#site-slogan {
  font-family: '. $themebuilder_typo_siteslogan_fontfamily .';
  color: '. $themebuilder_color_siteslogan .';
}

/* ]]> */
-->
';
  return $output;
}