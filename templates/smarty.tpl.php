<?php

/**
 * Generate the code for smarty.tpl.php
 */
function generate_smarty() {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';
    $output .= '<?php // $Id$ ?>'."\n";
    $output .= $prolog_html;
    $output .= '<!DOCTYPE html PUBLIC "-//'. $doctype_oranisation .'//DTD '. $themebuilder_doctype .'//EN" "'. $doctype_link .'">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{'. $themebuilder_lang .'}" xml:lang="{'. $themebuilder_lang .'}">
  <head>
    <title>{$head_title}</title>'."\n";
    if ($themebuilder_shortcuticon_display == 1) {
      if ($themebuilder_shortcuticon_display_default == 1) {
        $shortcut_link = $path_theme .'favicon.ico';
      }
      else {
        $shortcut_link = $themebuilder_link_shortcut_icon;
      }
      $output .= '<link rel="shortcut icon" href="'. $shortcut_link .'" type="image/x-icon" />'."\n";
    }
    $output .= '<meta http-equiv="Content-Type" content="text/html; charset='. $themebuilder_charset .'" />
    <meta http-equiv="content-language" content="{'. $themebuilder_lang .'}" />
    <meta name="generator" content="'. $themebuilder_meta_generator .'" />
    <meta name="author" content="'. $themebuilder_meta_author .'" />
    <meta name="organisation" content="'. $themebuilder_meta_organization .'" />
    <meta name="copyrigh" content="'. $themebuilder_meta_copyrigh .'" />
    <meta name="date" content="'. $themebuilder_meta_date .'" />
    <meta name="description" lang="'. $themebuilder_meta_language .'" content="'. $themebuilder_meta_description .'" />
    <meta name="keywords" lang="'. $themebuilder_meta_language .'" content="'. $themebuilder_meta_keywords .'" />
    <meta name="revisit-after" content="'. $themebuilder_meta_revisit_after .'" />
    <meta name="robots" content="'. $themebuilder_meta_robots .'" />
    <meta name="cache" http-equiv="Cache-Control" content="max-age='. $themebuilder_meta_cache .'" />
    <meta name="audience" content="'. $themebuilder_meta_audience .'" />
    <meta name="rating" content="'. $themebuilder_meta_rating .'" />
    {$head}
    <!-- Note: media="screen" important for NN 4 and iCab 2.9 -->
    <link type="text/css" rel="stylesheet" media="screen" href="{$base_path . path_to_theme()}/base.css" />
    {$styles}
    {$scripts}
    <script type="text/javascript" src="{$base_path . path_to_theme()}/'. $themebuilder_theme_dir .'.js"></script>
    <!-- Site Navigation Bar -->
    <!-- Top -->
    <link rel="start" href="index.php" title="'. $themebuilder_logo_link_title .'" />
    <!-- Up -->
    <link rel="up" href="#page" title="'. t('Page') .'" />
    <!-- First -->
    <!--<link rel="first" href="#" title="'. t('First') .'" />-->
    <!-- Last -->
    <!--<link rel="last" href="#" title="'. t('Last') .'" />-->
    <!-- Previous -->
    <!--<link rel="prev" href="#" title="'. t('Preview') .'" />-->
    <!-- Next -->
    <!--<link rel="next" href="#" title="'. t('Next') .'" />-->

    <!-- Document -->'."\n";
    if (!empty($themebuilder_navbar_contents)) {
    $output .= '    <link rel="contents" href="'. $themebuilder_navbar_contents .'" title="'. t('Contents') .'" />'."\n";
    }
    $output .= '    <!--<link rel="chapter" href="#" title="'. t('Chapter') .' 1" />-->
    <!--<link rel="section" href="#" title="'. t('Section') .' 1.1" />-->
    <!--<link rel="subsection" href="#" title="'. t('Subsection') .' 1.1.1" />-->
    <!--<link rel="appendix" href="#" title="'. t('Footnote') .'" />-->'."\n";
    if (!empty($themebuilder_navbar_glossary)) {
    $output .= '    <link rel="glossary" href="'. $themebuilder_navbar_glossary .'" title="'. t('Glossary') .'" />'."\n";
    }
    if (!empty($themebuilder_navbar_index)) {
    $output .= '    <link rel="index" href="'. $themebuilder_navbar_index .'" title="'. t('Index') .'" />'."\n";
    }
    $output .= '    <!-- More -->
    <link rel="help" href="'. $themebuilder_navbar_help .'" title="'. t('Help') .'" />'."\n";
    if (!empty($themebuilder_navbar_search)) {
    $output .= '    <link rel="search" href="'. $themebuilder_navbar_search .'" title="'. t('Search') .'" />'."\n";
    }
    $output .= '    <link rel="author" href="'. $themebuilder_navbar_author .'" title="'. t('Author') .'" />
    <link rel="copyright" href="'. $themebuilder_navbar_copyright .'" title="'. t('Copyright') .'" />
    <link rel="bookmark" href="'. $themebuilder_theme_url .'" title="<?php print $site_name; ?>" />
    <!-- Other versions -->'."\n";
    $output .= '    <!-- End Site Navigation Bar -->

  </head>
  <body class="{$body_classes}">'."\n";
    $output .= '    <div id="page">'."\n";
    $output .= '      <div id="header">'."\n";

  if (!empty($themebuilder_skiplink_content_tab)) {
  $output_tab = 'tabindex="'. $themebuilder_skiplink_content_tab .'" ';
  }
  else {
  $output_tab = 'class="no-tab" ';
  $output_tab_class = 'no-tab ';
  }
  if (!empty($themebuilder_skiplink_content_accesskey)) {
  $output_acc = 'accesskey="'. $themebuilder_skiplink_content_accesskey .'" ';
  }
  else {
  $output_acc = '';
  $output_acc_class = 'no-acc';
  }
  if ($themebuilder_skiplink_settings == 0) {
    $output .= '      <div id="skip-nav">
      {if ($title)}
      <a href="#content" '. $output_tab . $output_acc .' class="'. $output_tab_class . $output_acc_class .'" title="'. t("Skip to Main Content: ") .' {$title} ">{$head_title}</a>
      {/if}
      {if (!$title)}
      <a href="#content" '. $output_tab . $output_acc .' class="'. $output_tab_class . $output_acc_class .'"  title="'. t("Skip to Main Content: ") . $themebuilder_logo_link_title .'">'. $themebuilder_logo_link_title .'</a>
      {/if}
    </div>'."\n";
  }
  else if ($themebuilder_skiplink_settings == 1) {
    $output .= '      <div id="skip-nav">
      {if ($title)}
      <a href="#content" '. $output_tab . $output_acc .' class="'. $output_tab_class . $output_acc_class .'" title="'. t("Skip to Main Content: ") . $title .'">'. t("Skip to Main Content") .'</a>
      {/if}
      {if (!$title)}
      <a href="#content" '. $output_tab . $output_acc .' class="'. $output_tab_class . $output_acc_class .'" title="'. t("Skip to Main Content: ") . $themebuilder_logo_link_title .'">'. t("Skip to Main Content") .'</a>
      {/if}
    </div>'."\n";
  }
  else {
    $output .= '      <div id="skip-nav">
      {if ($title)}
      <a href="#content" '. $output_tab . $output_acc .' class="'. $output_tab_class . $output_acc_class .'" title="'. t("Skip to Main Content: ") . $title .'">'. t("Skip to Main Content: ") . $head_title .'</a>
      {/if}
      {if (!$title)}
      <a href="#content" '. $output_tab . $output_acc .' class="'. $output_tab_class . $output_acc_class .'" title="'. t("Skip to Main Content: ") . $themebuilder_logo_link_title .'">'. t("Skip to Main Content: ") . $themebuilder_logo_link_title .'</a>
      {/if}
    </div>'."\n";
  }

/**
 * Logo
 */
/* Logo order */
if ($themebuilder_logo_order == 0) {
  $logo_first_class = '';
}
else {
  $logo_first_class = 'class="logo-first"';
}

/* Logo display */
if ($themebuilder_logo_display == 1) {
  $name_slogan_class = '{if (!$logo)} class="no-logo" {/if}';
}
else {
  $name_slogan_class = '{if (!$logo)} class="no-logo" {/if}';
}

/* Logo link */
if ($themebuilder_link_logo == 0) {
// Logo not in a link.
    $output_logo = '      {if ($logo)}
      <div id="logo" '. $logo_first_class .'>
        <img src="'. $base_path . $theme_path .'/images/logo.gif" alt="'. t(" $themebuilder_logo_image_alt") .'" id="logo-image" title="'. t("$themebuilder_logo_link_title") .'"/>
      </div>
      {/if}'."\n";
}
else if ($themebuilder_link_logo == 1) {
// Logo in a link ("a").
    $output_logo = '      {if ($logo)}
      <div id="logo" '. $logo_first_class .'>
      {if ($is_front)}
        <img src="'. $base_path . $theme_path .'/images/logo.gif" alt="'. t("$themebuilder_logo_image_alt") .'" title="'. t("$themebuilder_logo_image_alt") .'" id="logo-image" />
      {/if}
      {if (!$is_front)}
        <a href="{$base_path}" title="'. t("$themebuilder_logo_link_title") .'" rel="home">
        <img src="'. $base_path . $theme_path .'/images/logo.gif" alt="'. t("$themebuilder_logo_image_alt") .'" id="logo-image" />
        </a>
      {/if}
      </div>
      {/if}
      '."\n";
}

/**
 * Site name and site slogan
 */
if ($themebuilder_sitename_display == 1) {
  $site_name_class = '';
}
else {
  $site_name_class = 'class="hidden"';
}
if ($themebuilder_slogan_display == 1) {
$output_site_slogan = '     {if ($site_slogan)}
     <p id="site-slogan">{$site_slogan}</p>
     {/if}'."\n";
}
if ($themebuilder_site_name_link == 0) {
// Site name not in a link.
    $output_name_slogan .= '    <div id="name-slogan" '. $name_slogan_class .'>'."\n";
    $output_name_slogan .= '      {if ($site_name)}
      <h1 id="site-name" '. $site_name_class .'>{$site_name}</h1>
      {/if}'."\n";
    $output_name_slogan .= $output_site_slogan;
    $output_name_slogan .= '    </div> <!-- /#name-slogan -->'."\n";

}
else if ($themebuilder_site_name_link == 1) {
// Site name in a link ("a").
    $output_name_slogan .= '    <div id="name-slogan" '. $name_slogan_class .'>'."\n";
    $output_name_slogan .= '     {if ($site_name)}'."\n";
    $output_name_slogan .= '     {if ($is_front)}'."\n";
    $output_name_slogan .= '     <h1 id="site-name" '. $site_name_class .'>{$site_name}</h1>'."\n";
    $output_name_slogan .= '     {/if}'."\n";
    $output_name_slogan .= '     {if (!$is_front)}'."\n";
    $output_name_slogan .= '     <a href="{$base_path}" '. $site_name_class .' title="'. t("$themebuilder_logo_link_title") .'" rel="home"><h1 id="site-name">{$site_name}</h1></a>'."\n";
    $output_name_slogan .= '     {/if}'."\n";
    $output_name_slogan .= '     {/if}'."\n";
    $output_name_slogan .= $output_site_slogan;
    $output_name_slogan .= '    </div> <!-- /#name-slogan -->'."\n";

}

    $output .= $output_name_slogan;
    $output .= '    <div id="header-content">'."\n";
    $output .= $output_logo;
    if ($themebuilder_search_display == 1) {
    $output .= '      <div id="search-header">'."\n";
    $output .= '        {if ($search_box)}'."\n";
    $output .= '        <h2 class="hidden">Search</h2>'."\n";
    $output .= '        {$search_box}'."\n";
    $output .= '        {/if}'."\n";
    $output .= '      </div> <!-- /#search-header -->'."\n";
    }
    $output .= '      <div id="header_blocks" >'."\n";
    $output .= '        {$header}'."\n";
    $output .= '      </div> <!-- /header_blocks -->'."\n";
    $output .= '    </div> <!-- /#header-content -->'."\n";
    $output .= '    <div class="clear" title="for opera 4"></div>'."\n";
    $output .= '    <div id="navigation" >'."\n";
    if ($themebuilder_primary_display == 1) {
    $output .= '      {if ($primary_links)}'."\n";
    $output .= '      <div id="primary">'."\n";
    $output .= '        <h2 class="hidden">'. t('Servicenavigation') .'</h2>'."\n";
    $output .= '        {theme("links", $primary_links)}'."\n";
    $output .= '      </div> <!-- /#primary -->'."\n";
    $output .= '      {/if}'."\n";
    }
    if ($themebuilder_secondary_display == 1) {
    $output .= '      {if ($secondary_links)}'."\n";
    $output .= '      <div id="secondary">'."\n";
    $output .= '        <h2 class="hidden">'. t('Navigation') .'</h2>'."\n";
    $output .= '        {theme("links", $secondary_links)}'."\n";
    $output .= '      </div> <!-- /#secondary -->'."\n";
    $output .= '      {/if}'."\n";
    }
    $output .= '      <div class="clear"></div>'."\n";
    $output .= '    </div> <!-- /navigation -->'."\n";
    $output .= '    </div> <!-- /header -->'."\n";
    $output .= '    <hr class="hidden" />'."\n";


/**
 * Sidbar code
 */
    $output_sidebar_left = '      {if ($left)}'."\n";
    $output_sidebar_left .= '      <div id="sidebar-left" class="column sidebar">'."\n";
    $output_sidebar_left .= '        {$left}'."\n";
    $output_sidebar_left .= '      </div> <!-- /sidebar-left -->'."\n";
    $output_sidebar_left .= '      {/if}'."\n";
    $output_sidebar_left .= '      <hr class="hidden" />'."\n";

    $output_sidebar_right = '      {if ($right)}'."\n";
    $output_sidebar_right .= '     <div id="sidebar-right" class="column sidebar">'."\n";
    $output_sidebar_right .= '       {$right}'."\n";
    $output_sidebar_right .= '     </div> <!-- /sidebar-right -->'."\n";
    $output_sidebar_right .= '     {/if}'."\n";
    $output_sidebar_right .= '     <hr class="hidden" />'."\n";
/**
 * Main code
 */
    $output_main = '      <div id="content">
        <div class="content-margin">
          {$breadcrumb}'."\n";
          if ($themebuilder_mission_display == 1) {
          $output_main .= '          {if ($mission)}'."\n";
          $output_main .= '          <div id="mission">{$mission}</div>'."\n";
          $output_main .= '          {/if}'."\n";
          }
    $output_main .= '      {if ($content_top)}
          <div id="content-top">{$content_top}</div>
          {/if}
          {if ($title)}
          <h2 class="title">{$title}</h2>
          {/if}
          {if ($tabs)}
          <div class="tabs">{$tabs}</div>
          {/if}
          {$help}
          {$messages}
          {$content}
          {if ($feed_icons)}
          <div class="feed-icons">{$feed_icons}</div>
          {/if}
          {if ($content_bottom)}
          <div id="content-bottom">{$content_bottom}</div>
          {/if}
          <div class="clear"></div>
        </div> <!-- /content-margin -->
      </div> <!-- /content -->'."\n";

/**
 * Sidebar first
 */
  if ($themebuilder_block_order == 'navigation-first') {
    $output .= '    <div id="main" class="'. $themebuilder_block_order .'">'."\n";
    $output .= $output_sidebar_left;
    $output .= '      {if (!$left)}'."\n";
    $output .= $output_sidebar_right;
    $output .= '      {/if}'."\n";
    $output .= $output_main;
    $output .= '      {if ($left)}'."\n";
    $output .= $output_sidebar_right;
    $output .= '      {/if}'."\n";
  }
/**
 * Content first
 */
  else if ($themebuilder_block_order == 'content-first') {
    $output .= '    <div id="main" class="'. $themebuilder_block_order .'">'."\n";
    $output .= $output_main;
    $output .= $output_sidebar_left;
    $output .= $output_sidebar_right;
  }
    $output .= '      <div class="clear"></div>'."\n";
    $output .= '      </div> <!-- /main -->'."\n";
    $output .= '      <div id="footer">'."\n";
    $output .= '        <p>{$footer_message}</p>'."\n";
    if ($themebuilder_valid_html_display == 1 || $themebuilder_valid_css_display == 1 || $themebuilder_footer_copyright_display == 1 || $themebuilder_footer_home_display == 1) {
    $output .= '        <ul>'."\n";
    }
    if ($themebuilder_valid_html_display == 1) {
    $output .= '          <li><small><a class="icon8 valid" href="http://validator.w3.org/check?uri=referer" rel="external" xml:lang="en" title="'. $valid_title .'" lang="en"><span lang="en">Valid</span> '. $valid_abbr .'</a></small></li>'."\n";
    }
    if ($themebuilder_valid_css_display == 1) {
    $output .= '          <li><small><a class="icon8 valid" href="http://jigsaw.w3.org/css-validator/validator?uri='. $themebuilder_theme_url . $theme_path .'/layout.css" rel="external" title="This page validates as Cascading Style Sheet Transitional" lang="en"><span xml:lang="en" lang="en">Valid</span> <acronym xml:lang="en" title="Cascading Style Sheet" lang="en">CSS</acronym>!</a></small></li>'."\n";
    }
    if ($themebuilder_footer_copyright_display == 1) {
    $output .= '          <li><small><a class="icon8 copyright" href="'. $theme_path .'LICENSE.txt" rel="copyright" title="Copyright">&copy; <span xml:lang="en" lang="en">Copyright</span> '. date("Y") .'</a></small></li>'."\n";
    }
    if ($themebuilder_footer_home_display == 1) {
    $output .= '          <li><small><a class="icon8 home" href="'. $base_path .'" rel="home" title="'. $themebuilder_logo_link_title .'">'. $themebuilder_logo_link_title .'</a></small></li>'."\n";
    }
    if ($themebuilder_valid_html_display == 1 || $themebuilder_valid_css_display == 1 || $themebuilder_footer_copyright_display == 1 || $themebuilder_footer_home_display == 1) {
    $output .= '        </ul>'."\n";
    }
    $output .= '        {$footer}'."\n";
    $output .= '        {$closure}'."\n";
    $output .= '        <div class="clear"></div>'."\n";
    $output .= '      </div> <!-- /footer -->'."\n";
    $output .= '    </div> <!-- /page -->
  </body>
</html>
';

  return $output;
}