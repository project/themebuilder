<?php

/**
 * Generate the css code style.css
 * Note: Alternate import with a hack: @import "null?\"\{";
 */
function generate_stylecss() {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';
$output = '/* $Id$ */
<!--
/* <![CDATA[ */

/**
 * generator:      '. $themebuilder_meta_generator .'
 * author:         '. $themebuilder_meta_author .'
 * email:          '. $themebuilder_theme_email .'
 * website:        '. $themebuilder_theme_url .'
 * last modified:  '. date("Y-m-d") .'T'. date("H:i:s") .'-05:00'.'
 * license:        '. $themebuilder_meta_copyright .'
 */

@import "layout.css";
@import "hacks.css";

/* ]]>*/
-->
';
  return $output;
}