<?php

/**
 * Generate the css code hacks.css
 */
function generate_hackscss() {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';
/**
 * Primary link width
 */
$themebuilder_primary_link_width_no = round((($themebuilder_content_width - ($themebuilder_rounding_errors + $re) - $themebuilder_primary_padding) / $themebuilder_primary_links), 2);
$themebuilder_primary_link_width_one = round(((themebuilder_page_width("static") - ($themebuilder_rounding_errors + $re) - $themebuilder_primary_padding) / $themebuilder_primary_links), 2);
$themebuilder_primary_link_width_two = round(((themebuilder_page_width("static") - ($themebuilder_rounding_errors + $re) - $themebuilder_primary_padding) / $themebuilder_primary_links), 2);

/**
 *  Secondary link width
 */
$themebuilder_secondary_link_width_no = round((($themebuilder_content_width - ($themebuilder_rounding_errors + $re)) / $themebuilder_secondary_links), 2);
$themebuilder_secondary_link_width_one = round(((themebuilder_page_width("static") - ($themebuilder_rounding_errors + $re)) / $themebuilder_secondary_links), 2);
$themebuilder_secondary_link_width_two = round(((themebuilder_page_width("static") - ($themebuilder_rounding_errors + $re)) / $themebuilder_secondary_links), 2);

$output = '/* $Id$ */
<!--
/* <![CDATA[ */

/**
 * generator:      '. $themebuilder_meta_generator .'
 * author:         '. $themebuilder_meta_author .'
 * email:          '. $themebuilder_theme_email .'
 * website:        '. $themebuilder_theme_url .'
 * last modified:  '. date("Y-m-d") .'T'. date("H:i:s") . $themebuilder_theme_timezone .'
 * license:        '. $themebuilder_meta_copyright .'
 */

/**
 * -----------------------------------------------------------------------------
 * Descripton of the hacks
 * -----------------------------------------------------------------------------
 */

/**
 * NN 4 / Opera 5 HACK
 * W3C valid hack: "/*..."
 * Only for: NN 4, Opera 5
 * Not for: All other browser
 */
/**
 * NN/IE 4 HACK
 * W3C valid hack: "@media all"
 * Only for: NN 6-7, Mz 1, FF, IE 5-7 Win, Opera, Safari 2, Kq 3.
 * Not for: NN 4, IE 4 Win, IE 5 Mac, iCal 2, Omniweb 4
 */
/**
 * IE 5 HACK
 * W3C valid hack: "voice-family:"\"}\""; voice-family:inherit;"
 * Only for: NN 6-7, Mz 1, FF, IE 6-7, Opera 5+, Omniweb 4, Konqueror 3, Safari 1,
 * Not for: IE 5.5 and Opera 6
 */
/**
 * IE 6 HACK
 * W3C valid hack: "* html div"
 * Only for: IE 4-6 (Win,Mac)
 * Not for: All other browser
 */
/**
 * IE 7 HACK
 * W3C valid hack: "*+html"
 * Only for: IE 7
 * Not for: All other browser
 */
/**
 * OPERA/PNG HACK
 * W3C valid hack: "head:first-child+body div"
 * Only for: NN 6+, Mz 1+, IE 7+, Opera 7+, Sf 2, Kq 3.
 * Not for: NN 4, IE 4-6, Opera 5+6, Omniweb 4
 */

/**
 * SAFARI HACK
 * W3C valid hack: "html:not([dummy]) Selektor { Deklaration; } "
 * Only for: FF 1+, Safari 2+
 * Not for: Safari 1
 */

/*
html:not([dummy]) Selektor { Deklaration; }
*/

/**
 * -----------------------------------------------------------------------------
 * The hacks
 * -----------------------------------------------------------------------------
 */
.hidden,
.hr {
  display: none;
}

head:first-child+body .hidden,
head:first-child+body .hr {
  float: left;
  display: inline;
  width: auto;
  position: absolute;
  top: -100em;
  left: -100em;
}
/* ----- Hacks only for IE Mac ----- */
h1#site-name {
  /*\*//*/
  font-size: 2em !important; /* only "!important" */
  /**/
}
div#header_blocks div h2 {
/*  display: none; */
}

/**
 * ----------
 * Search
 * ----------
 */
/* ----- Hacks only for IE Mac ----- */
div#search-header {
  /*\*//*/
  width: 20em;
  /**/
}
/* IE 1-6 do only support hover on a elements */
* html div#search div.form-item input.form-text {
  width: 14em;
}
*+html div#search div.form-item input.form-text {
  width: 14em;
}
/* Hide from IE5-mac. Only IE-win see this. \*/
* html div div#search input.form-submit {
  height: 22px;
  font-size: 12px;
}
/* End hide from IE5/mac */

/* ----- Hacks only for IE Mac ----- */
div#search input.form-submit {
  /*\*//*/
  font-size: 0.9em !important;
  /**/
}

div#search input.form-text {
/*  background: white url(images/search.gif) no-repeat scroll 0% 50%; */
}
head:first-child+body div#search input.form-text {
/*   background: white url(images/search.png) no-repeat scroll 0% 50%; */
}

/**
 * ----------
 * IE Width hacks
 * ----------
 */
/**
 * IE 5 HACK
 * W3C valid hack: "voice-family:"\"}\""; voice-family:inherit;"
 * Only for: NN 6-7, Mz 1, FF, IE 6-7, Opera 5+, Omniweb 4, Konqueror 3, Safari 1,
 * Not for: IE 5.5 and Opera 6
 */
body.no-sidebars #name-slogan {
  /* width: 35em; */
  width: '. $themebuilder_content_width . $themebuilder_theme_layout .';
}
body.no-sidebars #name-slogan {
  voice-family:"\"}\"";
  voice-family:inherit;
  width: 28.5em;
  width: '. ($themebuilder_content_width - $themebuilder_image_height_logo_view_width) . $themebuilder_theme_layout .';
}
body.one-sidebars #name-slogan {
  /* width: 47em; */
  width: '. $themebuilder_content_width . $themebuilder_theme_layout .';
}
body.one-sidebars #name-slogan {
  voice-family:"\"}\"";
  voice-family:inherit;
  /* width: 40.5em; */
  width: '. ($themebuilder_content_width - $themebuilder_image_height_logo_view_width) . $themebuilder_theme_layout .';
}
body.two-sidebars #name-slogan {
  /* width: 59em; */
  width: '. $themebuilder_content_width . $themebuilder_theme_layout .';
}
body.two-sidebars #name-slogan {
  voice-family:"\"}\"";
  voice-family:inherit;
  width: 52.5em;
  width: '. ($themebuilder_content_width - $themebuilder_image_height_logo_view_width) . $themebuilder_theme_layout .';
}
/* Replace the print logo.png for screen transparent logo_screen.png */
head:first-child+body img#logo-image {
/*  visibility: hidden; */
}
div#logo {
  background: transparent url("images/logo_screen.gif") no-repeat;
}
head:first-child+body div#logo {
  background: transparent url("images/logo_screen.png") no-repeat;
}
#logo {
  /* width: 69px; *//* margin-left: 1em, 1em=16px, 53+16=69px */
  width: '. ($themebuilder_image_width_logo + 16) .'px; /* margin-left: 1em, 1em=16px, 53+16=69px */
}
#logo {
  voice-family:"\"}\"";
  voice-family:inherit;
  /* width: 53px; */
  width: '. $themebuilder_image_width_logo .'px;
}

/* IE 1-6 do not support transparent border */
* html div#content {
  border-top: none;
}
* html body.sidebar-left #content {
  border-right: none;
}
* html body.sidebar-right #content {
  border-left: none;
}

/**
 * NN 4 / Opera 5 HACK
 * W3C valid hack: "/*..."
 * Only for: NN 4, Opera 5
 * Not for: All other browser
 */
/* Note: Not valid in Amaya 10 */
/* primary */
body.no-sidebars #primary li {
  /*/*//*/
  width: '. $themebuilder_primary_link_width_no . $themebuilder_theme_layout .';
  /* */
}
body.one-sidebar #primary li {
  /*/*//*/
  width: '. $themebuilder_primary_link_width_one . $themebuilder_theme_layout .';
  /* */
}
body.two-sidebars #primary li {
  /*/*//*/
  width: '. $themebuilder_primary_link_width_two . $themebuilder_theme_layout .';
  /* */
}

/* secondary */
body.no-sidebars #secondary li {
  /*/*//*/
  width: '. $themebuilder_secondary_link_width_no . $themebuilder_theme_layout .';
  /* */
}
body.one-sidebar #secondary li {
  /*/*//*/
  width: '. $themebuilder_secondary_link_width_one . $themebuilder_theme_layout .';
  /* */
}
body.two-sidebars #secondary li {
  /*/*//*/
  width: '. $themebuilder_secondary_link_width_two . $themebuilder_theme_layout .';
  /* */
}
#primary li a,
#secondary li a {
  /*/*//*/
  text-align: center;
  /* */
}

/**
 * IE 6 HACK
 * W3C valid hack: "* html div"
 * Only for: IE 4-6 (Win,Mac)
 * Not for: All other browser
 */

/* ----- Hacks only for IE Mac ----- */
/* primary */
* html div body.no-sidebars #primary ul li {
  /*\*//*/
  width: '. $themebuilder_primary_link_width_no . $themebuilder_theme_layout .';
  /**/
}
* html div body.one-sidebar #primary ul li {
  /*\*//*/
  width: '. $themebuilder_primary_link_width_one . $themebuilder_theme_layout .';
  /**/
}
* html div body.two-sidebars #primary ul li {
  /*\*//*/
  width: '. $themebuilder_primary_link_width_two . $themebuilder_theme_layout .';
  /**/
}

/* secondary */
* html div body.no-sidebars #secondary ul li {
  /*\*//*/
  width: '. $themebuilder_secondary_link_width_no . $themebuilder_theme_layout .';
  /**/
}
* html div body.one-sidebar #secondary ul li {
  /*\*//*/
  width: '. $themebuilder_secondary_link_width_one . $themebuilder_theme_layout .';
  /**/
}
* html div body.two-sidebars #secondary ul li {
  /*\*//*/
  width: '. $themebuilder_secondary_link_width_two . $themebuilder_theme_layout .';
  /**/
}

* html div #primary li a,
* html div #secondary li a {
  /* ----- Hacks only for IE Mac ----- */
  /*\*//*/
  text-align: center;
  /**/
}

/* Hide from IE5-mac. Only IE-win sees this. \*/
* html p {
/*  height: 1%; */
  margin-left: 0;
  }
* html div#header {
/*  margin-right: -3px; */
  border: none; /* "1px solid transparent" for NN 6 */
  }
* html div#main {
  border: none; /* "1px solid transparent" for NN 6 */
  }
* html div#footer {
  border: none; /* "1px solid transparent" for NN 6 */
  }
/* End hide from IE5/mac */

/* Links relation and reverse */
a[rel="alternate"]:after,
a[rev="alternate"]:after {
  content: url("images/rel_alternate.gif");
}
head:first-child+body a[rel="alternate"]:after,
head:first-child+body a[rev="alternate"]:after {
  content: url("images/rel_alternate.png");
}
a[rel="author"]:after,
a[rev="author"]:after {
  content: url("images/rel_author.gif");
}
head:first-child+body a[rel="author"]:after,
head:first-child+body a[rev="author"]:after {
  content: url("images/rel_author.png");
}
a[rel="appendix"]:after,
a[rev="appendix"]:after {
  content: url("images/rel_appendix.gif");
}
head:first-child+body a[rel="appendix"]:after,
head:first-child+body a[rev="appendix"]:after {
  content: url("images/rel_appendix.png");
}
a[rel="bookmark"]:after,
a[rev="bookmark"]:after {
  content: url("images/rel_bookmark.gif");
}
head:first-child+body a[rel="bookmark"]:after,
head:first-child+body a[rev="bookmark"]:after {
  content: url("images/rel_bookmark.png");
}
a[rel="chapter"]:after,
a[rev="chapter"]:after {
  content: url("images/rel_chapter.gif");
}
head:first-child+body a[rel="chapter"]:after,
head:first-child+body a[rev="chapter"]:after {
  content: url("images/rel_chapter.png");
}

a[rel="contents"]:after,
a[rev="contents"]:after {
  content: url("images/rel_contents.gif");
}
head:first-child+body a[rel="contents"]:after,
head:first-child+body a[rev="contents"]:after {
  content: url("images/rel_contents.png");
}
a[rel="copyright"]:after,
a[rev="copyright"]:after {
  content: url("images/rel_copyright.gif");
}
head:first-child+body a[rel="copyright"]:after,
head:first-child+body a[rev="copyright"]:after {
  content: url("images/rel_copyright.png");
}
a[rel="glossary"]:after,
a[rev="glossary"]:after {
  content: url("images/rel_glossary.gif");
}
head:first-child+body a[rel="glossary"]:after,
head:first-child+body a[rev="glossary"]:after {
  content: url("images/rel_glossary.png");
}
a[rel="help"]:after,
a[rev="help"]:after {
  content: url("images/rel_help.gif");
}
head:first-child+body a[rel="help"]:after,
head:first-child+body a[rev="help"]:after {
  content: url("images/rel_help.png");
}
a[rel="index"]:after,
a[rev="index"]:after {
  content: url("images/rel_index.gif");
}
head:first-child+body a[rel="index"]:after,
head:first-child+body a[rev="index"]:after {
  content: url("images/rel_index.png");
}
a[rel="next"]:after,
a[rev="next"]:after {
  content: url("images/rel_next.gif");
}
head:first-child+body a[rel="next"]:after,
head:first-child+body a[rev="next"]:after {
  content: url("images/rel_next.png");
}
a[rel="prev"]:after,
a[rev="pref"]:after {
  content: url("images/rel_prev.gif");
}
head:first-child+body a[rel="prev"]:after,
head:first-child+body a[rev="pref"]:after {
  content: url("images/rel_prev.png");
}
a[rel="section"]:after,
a[rev="section"]:after {
  content: url("images/rel_section.gif");
}
head:first-child+body a[rel="section"]:after,
head:first-child+body a[rev="section"]:after {
  content: url("images/rel_section.png");
}
a[rel="start"]:after,
a[rev="start"]:after {
  content: url("images/rel_start.gif");
}
head:first-child+body a[rel="start"]:after,
head:first-child+body a[rev="start"]:after {
  content: url("images/rel_start.png");
}
a[rel="subsection"]:after,
a[rev="subsection"]:after {
  content: url("images/rel_subsection.gif");
}
head:first-child+body a[rel="subsection"]:after,
head:first-child+body a[rev="subsection"]:after {
  content: url("images/rel_subsection.png");
}

/* Links internal and external */
a.skip_up {
  background-image: url("images/skip_up.gif");
}
head:first-child+body a.skip_up {
  background-image: url("images/skip_up.png");
}
a.skip_first {
  background-image: url("images/skip_first.gif");
}
head:first-child+body a.skip_first {
  background-image: url("images/skip_first.png");
}
a.skip_last {
  background-image: url("images/skip_last.gif");
}
head:first-child+body a.skip_last {
  background-image: url("images/skip_last.png");
}

/* Mimetypes */
.text, .css, .javascript {
  background-image: url("images/x-text-generic.gif");
}
head:first-child+body .text, head:first-child+body .css, head:first-child+body .javascript {
  background-image: url("images/x-text-generic.png");
}
.html, .xhtml, .php {
  background-image: url("images/x-text-html.gif");
}
head:first-child+body .html, head:first-child+body .xhtml, head:first-child+body .php {
  background-image: url("images/x-text-html.png");
}
.image {
  background-image: url("images/x-image.gif");
}
head:first-child+body .image {
  background-image: url("images/x-image.png");
}
.document, .word, .pdf, .oasis_word, .oasis_text {
  background-image: url("images/x-office-document.gif");
}
head:first-child+body .document, head:first-child+body .word, head:first-child+body .pdf, head:first-child+body .oasis_word, head:first-child+body .oasis_text {
  background-image: url("images/x-office-document.png");
}
.ods, .spread, .spreadsheet, .oasis_spreadsheet {
  background-image: url("images/x-office-spreadsheet.gif");
}
head:first-child+body head:first-child+body .ods, head:first-child+body .spread, head:first-child+body .spreadsheet, head:first-child+body .oasis_spreadsheet {
  background-image: url("images/x-office-spreadsheet.png");
}
.odp, .presentation, .oasis_presentation {
  background-image: url("images/x-office-presentation.gif");
}
head:first-child+body .odp, head:first-child+body .presentation, head:first-child+body .oasis_presentation {
  background-image: url("images/x-office-presentation.png");
}
.odg, .drawing, .oasis_drawing {
  background-image: url("images/x-office-drawing.gif");
}
head:first-child+body .odg, head:first-child+body .drawing, head:first-child+body .oasis_drawing {
  background-image: url("images/x-office-drawing.png");
}

';

if ($themebuilder_page_minmax_ie == 1) {
$output .= '/* min-width and max-width only for IE5 and 6 */
* html #page {
  width: '. themebuilder_calculate($page_width, 'max') . $theme_max_suffix .';

  width: expression((document.documentElement && document.documentElement.clientHeight) ?
    (document.documentElement.clientWidth < '. themebuilder_calculate($page_width, 'min') .') ? "'. themebuilder_calculate($page_width, 'min') . $theme_min_suffix .'" : (( document.documentElement.clientWidth > ('. themebuilder_calculate($page_width, 'max') .' * parseInt(document.documentElement.currentStyle.fontSize))) ? "80em" : "auto") :

    (document.body.clientWidth < '. themebuilder_calculate($page_width, 'min') .') ? "'. themebuilder_calculate($page_width, 'min') . $theme_min_suffix .'" : (( document.body.clientWidth > ('. themebuilder_calculate($page_width, 'max') .' * parseInt(document.body.currentStyle.fontSize))) ? "'. themebuilder_calculate($page_width, 'max') . $theme_max_suffix .'" : "auto")
 );
}
';
}
$output .= '/* ]]> */
-->';

  return $output;
}