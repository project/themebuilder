<?php

/**
 * Generate the text code for info file
 */
function generate_info() {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';
  $output .= '; $Id$
name = '. $themebuilder_theme_name .'
description = '. $themebuilder_theme_description .' '. t('Block order') .': '. $themebuilder_block_order_text .'. '. t('Theme layout') .': '. $themebuilder_theme_layout_text .'.
version = VERSION
core = '. $themebuilder_theme_version .'.x
engine = phptemplate
stylesheets[all][] = style.css
stylesheets[print][] = print.css
';
  return $output;
}
