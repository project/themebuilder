/* $Id: */

Theme Builder
-------------

Description
-----------
Generate templates for Drupal 5 and 6, Smarty themes for Drupal 5.x, XTemplates for Drupal 4 or static XHTML templates.

Features
- XHTML and Drupal 5 and 6 templates.
- Accessibility and Web Standards.
- No browser proprietary code.
- Slim and easy.
- Extensive browser support.
- Valid XHTML, CSS and CSS Hacks.
- License GPL.

Installation
------------
   1. Copy the modul to: "/drupal6/sites/all/modules/".
   2. Create a file folder "themes" in: "/drupal6/sites/all/".
   3. Set the premission of the directory: "/drupal6/sites/all/themes" to a writable folder.
   4. Enable the module on: "Modules".
   5. Enable the block "Theme Builder" on: "Blocks".

Generate Templates
------------------
   1. Go to: "Theme builder".
   2. Save the configuration of your theme in each page. In the end, generate the theme on: Generate

More information and tutorials
------------------------------
Read the help on "admin/help/themebuilder".

Bugs/Contributions
------------------
You can help!
    * Create or send feature requests. Tell us, what you miss in the theme builder.
    * Create or send bug reports. Test the standard theme local (http://localhost/) or on: http://www.zwahlendesign.ch/drupal6/ in not tested browsers and tell us the details about the errors.
    * Create or send support requests if you need more help.
    * Send a message to the developers:
          o Christian Zwahlen, E-Mail: info@zwahlendesign.ch
    * Make a donation to the developers:
          o Christian Zwahlen, Information on: http://www.zwahlendesign.ch
    * Help to coding. Edit the "style_css.php" or the "hacks_css.php" and tell us this.
You will be named under "11. Thanks" if you will that.

Thanks a lot!

Roadmap
- Drupal color modul integration.
- More color functions (color picker).
- More element design.

Author
------
Christian Zwahlen
info@zwahlendesign.ch
