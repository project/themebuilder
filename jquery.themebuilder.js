
if (Drupal.jsEnabled) {
  $(document).ready(function(){
    $("div.details").hide();
    $("div.colorpalette").hide();
    $("div.installation").hide();
    $("div.tutorial").hide();
    $("div.documentation").hide();
    $("div.features").hide();
    $("div.hardware_support").hide();
    $("div.os_support").hide();
    $("div.browser_support").hide();
    $("div.bugs").hide();
    $("div.devel").hide();
    $("div.source").hide();
    $("div.license").hide();
    $("div.thanks").hide();
    $("div.legend").hide();
    $("div.recources").hide();
    $("a.all").click( function(){ 
      $("div.details").toggle("slow");
      $("div.installation").toggle("slow");
      $("div.tutorial").toggle("slow");
      $("div.documentation").toggle("slow");
      $("div.features").toggle("slow");
      $("div.hardware_support").toggle("slow");
      $("div.os_support").toggle("slow");
      $("div.browser_support").toggle("slow");
      $("div.bugs").toggle("slow");
      $("div.devel").toggle("slow");
      $("div.source").toggle("slow");
      $("div.license").toggle("slow");
      $("div.thanks").toggle("slow");
      $("div.legend").toggle("slow");
      $("div.recources").toggle("slow");
    });
    $("a.details").click( function(){ $("div.details").toggle("slow"); } );
    $("a.colorpalette").click( function(){ $("div.colorpalette").toggle("slow"); } );
    $("a.installation").click( function(){ $("div.installation").toggle("slow"); });
    $("a.tutorial").click( function(){ $("div.tutorial").toggle("slow"); });
    $("a.documentation").click( function(){ $("div.documentation").toggle("slow"); });
    $("a.features").click( function(){ $("div.features").toggle("slow"); });
    $("a.hardware_support").click( function(){ $("div.hardware_support").toggle("slow"); });
    $("a.os_support").click( function(){ $("div.os_support").toggle("slow"); });
    $("a.browser_support").click( function(){ $("div.browser_support").toggle("slow"); });
    $("a.bugs").click( function(){ $("div.bugs").toggle("slow"); });
    $("a.devel").click( function(){ $("div.devel").toggle("slow"); });
    $("a.source").click( function(){ $("div.source").toggle("slow"); });
    $("a.license").click( function(){ $("div.license").toggle("slow"); });
    $("a.thanks").click( function(){ $("div.thanks").toggle("slow"); });
    $("a.legend").click( function(){ $("div.legend").toggle("slow"); });
    $("a.recources").click( function(){ $("div.recources").show("slow"); });
    $("a.tb_preview").click( function(){ $("div.tb_preview_block").toggle("slow"); });
    $("a.tb_noimages").click( function(){ $("div.preview-body").removeClass("images"); $("div.preview-body").addClass('noimages'); });
    $("a.tb_images").click( function(){ $("div.preview-body").removeClass("noimages"); $("div.preview-body").addClass('images'); });
  });
}
