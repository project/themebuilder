<?php

/**
 * @file
 * Theme Builder View
 */
function themebuilder_view() {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';
// $page_width_calc = themebuilder_page_width("static");
// themebuilder_page_width("screen") = themebuilder_page_width("screen");
// themebuilder_page_width("final")  = themebuilder_page_width("final");
/**
 * Primary and secondary link width
 */
$themebuilder_primary_link_width = round(((themebuilder_page_width("static") - ($themebuilder_rounding_errors + $re) - $themebuilder_primary_padding) / $themebuilder_primary_links), 2);

$themebuilder_secondary_link_width = round(((themebuilder_page_width("static") - ($themebuilder_rounding_errors + $re)) / $themebuilder_secondary_links), 2);

$primary_calc_rerrors = (themebuilder_page_width("static") - $themebuilder_primary_padding - ($themebuilder_primary_links * $themebuilder_primary_link_width));
$secondary_calc_rerrors = (themebuilder_page_width("static") - $themebuilder_secondary_padding - ($themebuilder_secondary_links * $themebuilder_secondary_link_width));


if ((themebuilder_page_width("screen")) > 1280) {
  drupal_set_message( t('The current page width: '. themebuilder_page_width("static") . $themebuilder_theme_layout .' ('. themebuilder_page_width("screen") .'px) is not for screen resolution: 1280 x 1024px. Edit the width values in: ') .'<em>'. l('CSS layout', 'themebuilder/edit/layout') .'</em>', 'error');
}
else if ((themebuilder_page_width("screen")) > 1024) {
  drupal_set_message( t('The current page width: '. themebuilder_page_width("static") . $themebuilder_theme_layout .' ('. themebuilder_page_width("screen") .'px) is not for screen resolution: 1024 x 768px. Edit the width values in: ') .'<em>'. l('CSS layout', 'themebuilder/edit/layout') .'</em>', 'warning');
}
else if ((themebuilder_page_width("screen")) > 800) {
  drupal_set_message( t('The current page width: '. themebuilder_page_width("static") . $themebuilder_theme_layout .' ('. themebuilder_page_width("screen") .'px) is not for screen resolution: 800 x 600px. Edit the width values in: ') .'<em>'. l('CSS layout', 'themebuilder/edit/layout') .'</em>', 'warning');
}

if ((themebuilder_page_width("screen")) <= 128) {
  drupal_set_message( t('The current page width: '. themebuilder_page_width("static") . $themebuilder_theme_layout .' ('. themebuilder_page_width("screen") .'px) is ready for screen resolution: 128 x 128px. (Handy)'), 'ok');
}
else if ((themebuilder_page_width("screen")) <= 240) {
  drupal_set_message( t('The current page width: '. themebuilder_page_width("static") . $themebuilder_theme_layout .' ('. themebuilder_page_width("screen") .'px) is ready for screen resolution: 240 x 320px. (PDA)'), 'ok');
}
else if ((themebuilder_page_width("screen")) <= 800) {
  drupal_set_message( t('The current page width: '. themebuilder_page_width("static") . $themebuilder_theme_layout .' ('. themebuilder_page_width("screen") .'px) final: '. themebuilder_page_width("final")  .'px is ready for screen resolution: 800 x 600px. (Personal computer)'), 'ok');
}
else if ((themebuilder_page_width("screen")) <= 1024) {
  drupal_set_message( t('The current page width: '. themebuilder_page_width("static") . $themebuilder_theme_layout .' ('. themebuilder_page_width("screen") .'px) is ready for screen resolution: 1024 x 768px. (Personal computer)'), 'ok');
}

if ($primary_calc_rerrors < 0.50) {
  drupal_set_message( t('Set "Rounding error width" (for primary links) to a higher value in: ') .'<em>'. l('CSS layout / Advanced width settings / Rounding error width', 'themebuilder/edit/layout') .'</em>', 'error');
}
if ($secondary_calc_rerrors < 0.50) {
  drupal_set_message( t('Set "Rounding error width" (for secondary links) to a higher value in: ') .'<em>'. l('CSS layout / Advanced width settings / Rounding error width', 'themebuilder/edit/layout') .'</em>', 'error');
}


  $output = '<h3>'. t('Theme overview') .'</h3>'."\n";
//   $output .= '<h4>'. t('Theme setup') .'</h4>'."\n";
  $output .= '<ul>'."\n";
  $output .= '<li>'. $themebuilder_engine_name .' '. t('name') .': '. $themebuilder_theme_name .'.</li>'."\n";
  $output .= '<li>'. t('Save in the file folder: ') .'<em class="icon16 folder">"'. $themebuilder_theme_dir .'"</em> '. t('and in the directory: ') .'<span class="icon16 folder">'. $theme_path .'</span>'."\n";
  $output .= '</li>'."\n";
  $output .= '</ul>'."\n";
  $output .= '<h3 id="details"><a href="#details" title="'. t('Theme details') .'" class="details">'. t('Theme details') .'</a></h3>'."\n";
  $output .= '<div class="details">'."\n";
  $output .= '<h4>'. t('HTML structure') .'</h4>'."\n";
  $output .= '<ul>'."\n";
  $output .= '<li>'. t('Block order') .': '. $themebuilder_block_order_text .' ('. $themebuilder_block_order .').</li>'."\n";
  $output .= '<li>'. t('Theme layout') .': '. $themebuilder_theme_layout_text .' ('. $themebuilder_theme_layout .').</li>'."\n";
  $output .= '<li>'. t('Number of primary links: ') . $themebuilder_primary_links .' '. t('links') .'.</li>'."\n";
  $output .= '<li>'. t('Number of secondary links: ') . $themebuilder_secondary_links .' '. t('links') .'.</li>'."\n";
  $output .= '</ul>'."\n";
  $output .= '<h4>'. t('CSS layout') .'</h4>'."\n";
  $output .= '<ul>'."\n";
  $output .= '<li>'. t('Page width: ') . themebuilder_page_width("static") . $themebuilder_theme_layout .' ('. themebuilder_page_width("screen") .'px).'."\n";
  $output .= '  <ul>'."\n";
  $output .= '  <li>'. t('Page minimum width: ') . $themebuilder_page_min_width . $theme_min_suffix .'</li>'."\n";
  $output .= '  <li>'. t('Page maximum width: ') . $themebuilder_page_max_width . $theme_max_suffix .'</li>'."\n";
  $output .= '  </ul>'."\n";
  $output .= '</li>'."\n";
  $output .= '<li>'. t('Sidebars: ') . $body_sidebars_text .' ('. t('class: ') . $body_sidebars_class .')</li>'."\n";
  $output .= '<li>'. t('Primary padding: ') . $themebuilder_primary_padding . $themebuilder_theme_layout .' ('. ($themebuilder_primary_padding * $px) .'px)</li>'."\n";
  $output .= '<li>'. t('Secondary padding: ') . $themebuilder_secondary_padding . $themebuilder_theme_layout .' ('. ($themebuilder_secondary_padding * $px) .'px)</li>'."\n";
  $output .= '<li>'. t('Sidebar left width: ') . $themebuilder_left_width . $themebuilder_theme_layout .' ('. ($themebuilder_left_width * $px) .'px)'."\n";
  $output .= '  <ul>'."\n";
  $output .= '  <li>'. t('Sidebar left mininum width: ') . themebuilder_calculate($themebuilder_left_width, 'min') . $theme_min_suffix .'</li>'."\n";
  $output .= '  <li>'. t('Sidebar left maximum width: ') . themebuilder_calculate($themebuilder_left_width, 'max') . $theme_max_suffix .'</li>'."\n";
  $output .= '  </ul>'."\n";
  $output .= '</li>'."\n";
  $output .= '<li>'. t('Sidebar right width: ') . $themebuilder_right_width . $themebuilder_theme_layout .' ('. ($themebuilder_right_width * $px) .'px)'."\n";
  $output .= '  <ul>'."\n";
  $output .= '  <li>'. t('Sidebar right mininum width: ') . themebuilder_calculate($themebuilder_left_width, 'min') . $theme_min_suffix .'</li>'."\n";
  $output .= '  <li>'. t('Sidebar right maximum width: ') . themebuilder_calculate($themebuilder_left_width, 'max') . $theme_max_suffix .'</li>'."\n";
  $output .= '  </ul>'."\n";
  $output .= '</li>'."\n";
  $output .= '<li>'. t('Content width: ') . $themebuilder_content_width . $themebuilder_theme_layout .' ('. ($themebuilder_right_width * $px) .'px)'."\n";
  $output .= '  <ul>'."\n";
  $output .= '  <li>'. t('Content mininum width: ') . themebuilder_calculate($themebuilder_content_width, 'min') . $theme_min_suffix .'</li>'."\n";
  $output .= '  <li>'. t('Content maximum width: ') . themebuilder_calculate($themebuilder_content_width, 'max') . $theme_max_suffix .'</li>'."\n";
  $output .= '  </ul>'."\n";
  $output .= '</li>'."\n";
  $output .= '<li>'. t('Rounding error width: ') . $themebuilder_rounding_errors . $themebuilder_theme_layout .' ('. ($themebuilder_rounding_errors * $px) .'px)</li>'."\n";

  $output .= '<li>'. t('Browser margin: ') . $themebuilder_browser_margin . $themebuilder_theme_layout .' ('. ($themebuilder_browser_margin * $px) .'px)</li>'."\n";
  $output .= '</ul>'."\n";
  $output .= '<h4>'. t('CSS Hacks') .'</h4>'."\n";
  $output .= '<ul>'."\n";
  $output .= '<li>'. t('Width per primary link: ') . $themebuilder_primary_link_width . $themebuilder_theme_layout .' ('. ($themebuilder_primary_link_width * $px) .'px)</li>'."\n";
  $output .= '<li>'. t('Width per secondary link: ') . $themebuilder_secondary_link_width . $themebuilder_theme_layout .' ('. ($themebuilder_secondary_link_width * $px) .'px)</li>'."\n";
  $output .= '<li>'. t('Real rounding error width for primary links: ') . $primary_calc_rerrors . $themebuilder_theme_layout .' ('. ($primary_calc_rerrors * $px) .'px)</li>'."\n";
  $output .= '<li>'. t('Real rounding error width for secondary links: ') . $secondary_calc_rerrors . $themebuilder_theme_layout .' ('. ($secondary_calc_rerrors * $px) .'px)</li>'."\n";
  $output .= '</ul>'."\n";
  $output .= '</div>'."\n";
  $output .= '<h3>'. t('Full Preview') .'</h3>'."\n";
  $output .= '<div class="preview">'."\n";
  $output .= generate_previewtpl("page");
  $output .= '</div>'."\n";
return $output;
}