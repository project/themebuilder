<?php

/**
 * @file
 * Theme Builder Help Tutorial
 */
function generate_tutorial_tb() {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';
$path_themebuilder = drupal_get_path('module', 'themebuilder');
      $output .= '<div class="tutorial">'."\n";
      $output .= '<h3 id="tutorial">2 '. t('Tutorial') .'</h3>'."\n";
      $output .= '<h4 id="tutorial">2.1 '. t('Workflow: Generate Website') .'</h4>'."\n";
      $output .= '<p>'. t('Make:') .'</p>'."\n";
      $output .= '<ol>'."\n";
      $output .= '  <li>'. t('Wireframes and Concept') .'</li>'."\n";
      $output .= '  <li>'. t('Draft') .'</li>'."\n";
      $output .= '  <li>'. t('Presentation') .'</li>'."\n";
      $output .= '</ol>'."\n";

      $output .= '<h4 id="tutorial">2.2 '. t('Generate Themes') .'</h4>'."\n";
      $output .= '<h5>2.2.1 '. t('General') .'</h5>'."\n";
      $output .= '<ol>'."\n";
      $output .= '  <li>'. t('Define structure and layout. You can view, change and calculate the value in the spreadsheet document: <a class="icon16 oasis_spreadsheet" href="'. $module_base_path .'/source/layout.ods" title="The layout width" >layout.ods</a>') .'</li>'."\n";
      $output .= '  <li>'. t('Define typography.') .'</li>'."\n";
      $output .= '  <li>'. t('Define colors.') .'</li>'."\n";
      $output .= '  <li>'. t('Go to: ') .'"'. l('Theme builder', 'themebuilder') .'".</li>'."\n";
      $output .= '  <li>'. t('Save the configuration of your theme in each page. In the end, generate the theme on: ') . l('Generate', 'themebuilder/generate') .'</li>';
      $output .= '  <li>'. t('Create content and navigation (primary, secondary and main navigation).') .'</li>'."\n";
      $output .= '  <li>'. t('Edit images.') .'</li>'."\n";
      $output .= '  <li>'. t('Make a screenshot of the theme.') .'</li>'."\n";
      $output .= '  <li>'. t('Import color palettes in office or color software.') .'</li>'."\n";
      $output .= '  <li>'. t('Make the final documentation of the website.') .'</li>'."\n";
      $output .= '  <li>'. t('Done!') .'</li>'."\n";
      $output .= '</ol>'."\n";


      $output .= '<h5>2.2.2 '. t('Software') .'</h5>'."\n";
      $output .= '<h6>2.2.2.1 '. t('<abbr lang="en" xml:lang="en" title="Extensible Hypertext Markup Language" class="icon16 html">XHTML</abbr>/<abbr lang="en" xml:lang="en" class="icon16 css" title="Cascading Style Sheets">CSS</abbr> Editors') .'</h6>'."\n";
      $output .= '<ul>'."\n";
      $output .= '  <li><a href="http://www.gnome.org/projects/gedit/" title="gedit" class="icon16 gedit">gedit</a></li>'."\n";
      $output .= '  <li><a href="http://gtkedit1.sourceforge.net" title="gtkedit" class="icon16 gtkedit">gtkedit</a></li>'."\n";
      $output .= '  <li><a href="http://www.kate-editor.org" title="Kate" class="icon16 kate">Kate</a></li>'."\n";
      $output .= '  <li><a href="http://tarot.freeshell.org/leafpad/" title="Leafpad" class="icon16 leafpad">Leafpad</a></li>'."\n";
      $output .= '  <li><a href="https://sourceforge.net/projects/juffed/" title="JuffEd" class="icon16 juffed">JuffEd</a></li>'."\n";
      $output .= '  <li><a href="http://geany.uvena.de" title="Geany" class="icon16 geany">Geany</a></li>'."\n";
      $output .= '  <li><a href="http://www.jedit.org" title="jEdit" class="icon16 jedit">jEdit</a></li>'."\n";
      $output .= '  <li><a href="http://kile.sourceforge.net" title="Kile" class="icon16 kile">Kile</a> (TeX/LaTeX editor)</li>'."\n";
      $output .= '  <li><a href="http://www.nedit.org/" title="NEdit" class="icon16 nedit">NEdit</a></li>'."\n";
      $output .= '  <li><a href="http://notepad-plus.sourceforge.net" title="Notepad++" class="icon16 notepad++">Notepad++</a></li>'."\n";
      $output .= '  <li><a href="http://gnotepad.sourceforge.net/" title="gnotepad+" class="icon16 gnotepad+">gnotepad+</a></li>'."\n";
      $output .= '  <li><a href="http://www.gphpedit.org" title="gPHPEdit" class="icon16 gphpedit">gPHPEdit</a></li>'."\n";
      $output .= '  <li><a href="http://www.scintilla.org" title="gPHPEdit" class="icon16 scite"><abbr title="SCIntilla based Text Editor">sciTE</abbr></a></li>'."\n";
      $output .= '  <li><a href="http://www.w3.org/Amaya/" title="Amaya" class="icon16 amaya">Amaya</a></li>'."\n";
      $output .= '  <li><a href="http://www.bostream.nu/johanb/august/" title="August" class="icon16 august">August</a></li>'."\n";
      $output .= '  <li><a href="http://lisas.de/erwin/" title="Erwin" class="icon16 quantaplus">Erwin</a></li>'."\n";
      $output .= '  <li><a href="http://www.openwebsuite.org" title="OpenWebsuite.org" class="icon16 openwebsuite">OpenWebsuite.org</a></li>'."\n";
      $output .= '  <li><a href="http://www.mozilla.org/editor" title="Mozilla/SeaMonkey Composer" class="icon16 mozilla" >Mozilla/SeaMonkey Composer</a></li>'."\n";
      $output .= '  <li><a href="http://www.nvu.com" title="NVU" class="icon16 nvu"><abbr title="New View">NVU</abbr></a></li>'."\n";
      $output .= '  <li><a href="http://www.kompozer.net" title="KompoZer" class="icon16 kompozer">KompoZer</a></li>'."\n";
      $output .= '  <li><a href="http://quanta.kdewebdev.org" title="Quanta Plus" class="icon16 quantaplus">Quanta Plus</a></li>'."\n";
      $output .= '  <li><a href="http://bluefish.openoffice.nl/" title="Bluefish" class="icon16 bluefish">Bluefish</a></li>'."\n";
      $output .= '  <li><a href="http://cssed.sourceforge.net" title="CSSED" class="icon16 cssed"><abbr lang="en" xml:lang="en" title="Cascading Style Sheets">CSS</abbr><abbr lang="en" xml:lang="en" title="Editor">ED</abbr></a></li>'."\n";
      $output .= '</ul>'."\n";
      $output .= '<h6>2.2.2.2 '. t('Grafik Software') .'</h6>'."\n";
      $output .= '<ul>'."\n";
      $output .= '  <li>'. t('Image Manipulation Program') .': <a href="http://www.gimp.org" title="Gimp" class="icon16 gimp"><abbr lang="en" xml:lang="en" title="GNU Image Manipulation Program">GIMP</abbr></a> (<span class="icon16 linux">Linux</span>, <abbr title="Windows" class="icon16 win">Win</abbr>, <abbr title="Macintosh" class="icon16 mac">Mac</abbr>), <a href="http://www.koffice.org/krita/" title="Krita" class="icon16 krita" >Krita</a> (<span class="icon16 linux">Linux</span>)</li>'."\n";
      $output .= '  <li>'. t('Colorschemer') .': <a href="http://home.gna.org/colorscheme/" title="Agave" class="icon16 agave">Agave</a> (<span class="icon16 linux">Linux</span>)</li>'."\n";
      $output .= '  <li>'. t('Colors palette files editor') .': <a href="http://docs.kde.org/development/en/extragear-graphics/kcoloredit/index.html" title="KColorEdit" class="icon16 kcoloredit">KColorEdit</a> (<span class="icon16 linux">Linux</span>)</li>'."\n";
      $output .= '  <li>'. t('Color selector') .': <a href="http://gcolor2.sourceforge.net/" title="gcolor2" class="icon16 gcolor2">gcolor2</a> (<span class="icon16 linux">Linux</span>)</li>'."\n";
      $output .= '  <li>'. t('<abbr lang="en" xml:lang="en" class="icon16 svg" title="Scalable Vector Graphics">SVG</abbr> editors') .': <a href="http://inkscape.org" title="Incscape" class="icon16 inkscape">Inkscape</a></li>'."\n";
      $output .= '</ul>'."\n";
      $output .= '<h6>2.2.2.3 '. t('Office Software') .'</h6>'."\n";
      $output .= '<ul>'."\n";
      $output .= '  <li><a href="http://www.openoffice.org" title="OpenOffice.org" class="icon16 openofficeorg_2">OpenOffice.org 2</a></li>'."\n";
      $output .= '</ul>'."\n";






    $output .= '<h5>2.2.4 '. t('Layout width') .'</h5>'."\n";
      $output .= '<ol>'."\n";
      $output .= '  <li>'. t('Download ') .'<a href="http://www.openoffice.org" class="icon16 openofficeorg_2">OpenOffice.org 2</a> (<span class="icon16 win_xp">Win</span>, <span class="icon16 linux">Linux</span>, <abbr title="Macintosh" class="icon16 mac">Mac</abbr>, <abbr lang="en" title="Uniplexed Information and Computing Service" class="icon16 unix">UNIX</abbr>) '. t('or') .' <a href="http://www.neooffice.org" class="icon16 neooffice">NeoOffice</a> (<abbr title="Macintosh" class="icon16 mac">Mac</abbr>) or buy <a href="http://www.sun.com/software/star/staroffice/" class="icon16 staroffice_8">StarOffice</a> (<span class="icon16 win_xp">Win</span>, <span class="icon16 linux">Linux</span>) '. t('if you do not have this office software.') .' '. t('Open the document:') .' <a class="icon16 oasis_spreadsheet" href="'. $module_base_path .'/source/layout.ods" title="The layout width" >layout.ods</a>.</li>'."\n";
      $output .= '  <li>'. t('In "1. Settings" change values only in <span class="cell_editable">yellow cells</span>.') .'</li>'."\n";
      $output .= '  <li>'. t('Go to "2 Width for <abbr title="Cascading Style Sheets" class="icon16 css">CSS</abbr>" and copy the values in the <span class="cell_sidebar">gray cells</span> for the sidebar and the <span class="cell_content">dark gray cells</span> for the content in the theme builder or your <abbr title="Cascading Style Sheets" class="icon16 css">CSS</abbr> if you have a static <abbr lang="en" xml:lang="en" title="Extensible Hypertext Markup Language" class="icon16 html">XHTML</abbr> site. The <span class="cell_page">gray cells with blue text</span> for the page width and values with <span class="cell_page_screen">orange text</span> for the screen resolution width, and values with <span class="cell_page_max">red text</span> for the minimum and maximum width.') .'</li>'."\n";
      $output .= '</ol>'."\n";

      $output .= '<h5>2.2.5 '. t('Site name and Logo') .'</h5>'."\n";
      $output .= '<h6>2.2.5.1 '. t('Site name as title and logo without site name text') .'</h6>'."\n";
      $output .= '<p>'.  t('Set:') .'<em>"'. t('Enable site name.') .'"</em> '. t('in ') .' <em>". '. t('Site name display') .'"</em>. '. t('Set the logo or site name in a "home" link or generate a "Home" or "Start" link with your modul ') . l('Menues', 'admin/build/menu') .'.</p>'."\n";
      $output .= '<h6>2.2.5.2 '. t('Logo with site name or site name in a graphic') .'</h6>'."\n";
      $output .= '<p>'. t('Set:') .'<em>"'. t('Disable site name only for screen.') .'"</em> '. t('If your logo have your site name, then display the logo in a home link. Screen reader can any more read the site name.') .' <span class="icon16 warning">'. t('Warning: For web accessibility, do not disable the site name in your theme configuration: ') . l('Themes', 'admin/build/themes/settings/') .'!</span></p>';
      $output .= '<h6>2.2.5.3 '. t('No logo') .'</h6>'."\n";
      $output .= '<p>'. t('Disable the logo in ') . l('Themes', 'admin/build/themes/settings/') .'. '. t('Edit the: "Head background image"') .' (bg_head.png and bg_head.jpg)'.'</p>';
      $output .= '<p><em class="icon16 important">'. t('Improtant: The "Upload icon image" function in') .' <span class="icon16 drupal">Drupal</span> '. t('on') .' '. l('Themes', 'admin/build/themes/settings/') .' '. t('is not available!') .'</em>'. t('Why? Because:') .'</p>'."\n";
      $output .= '<ol>'."\n";
      $output .= '  <li>'. t('The themebuilder use 3 logos') .' (<span class="icon16 image">logo.gif</span>, <span class="icon16 image">logo_screen.gif</span> '. t('and') .' <span class="icon16 image">logo_screen.png</span>) '. t('for transparent logos with cross browser support! More information on ') .'<a class="icon16 pdf" href="'. $module_base_path .'/documentation.pdf" title="The documentation" >documentation.pdf</a> chapter 3.5.2.3</li>'."\n";
      $output .= '  <li>'. t('Drupal 6 can include only one <abbr title="Portable Network Graphics">PNG</abbr> Logo ("logo.png") automatically, but <strong>not a <abbr title="Graphics Interchange Format">GIF</abbr> logo!</strong> See: ') .'<a href="http://drupal.org/node/221290">Issues 221290</a>.</li>';
      $output .= '</ol>'."\n";

      $output .= '<h5>2.2.5 '. t('Create content') .'</h5>'."\n";
      $output .= '<h5>2.2.5 '. t('Primary links') .'</h5>'."\n";
      $output .= '<p>The "Servicenavigation". Use only links to pages, that give users the service to helps find concrete pages, communication ability or help of your website. '.' <span class="icon16 warning">'. ('Warning: Do not use the primary links for your main navigation! Because web accessibile website have to provide service links at first!') .'</span> '. t('Example links: ') .'</p>';
      $output .= '<h6>2.2.5.1 '. t('No sidebars (4 links)') .'</h6>'."\n";
      $output .= '<ol>'."\n";
      $output .= '  <li>'. t('Sitemap') .'</li>'."\n";
      $output .= '  <li>'. t('Glossary (Glossary after sitemap)') .'</li>'."\n";
      $output .= '  <li>'. t('Index (Index after glossary)') .'</li>'."\n";
      $output .= '  <li>'. t('Help (general help)') .'</li>'."\n";
      $output .= '</ol>'."\n";
      $output .= '<h6>2.2.5.2 '. t('One sidebar (5 links)') .'</h6>'."\n";
      $output .= '<ol>'."\n";
      $output .= '  <li>'. t('Sitemap') .'</li>'."\n";
      $output .= '  <li>'. t('Glossary (Glossary after sitemap)') .'</li>'."\n";
      $output .= '  <li>'. t('Index (Index after glossary)') .'</li>'."\n";
      $output .= '  <li>'. t('Help (general help)') .'</li>'."\n";
      $output .= '  <li>'. t('Imprint with contact form') .'</li>'."\n";
      $output .= '</ol>'."\n";
      $output .= '<h6>2.2.5.3 '. t('Two sidebars (6 links)') .'</h6>'."\n";
      $output .= '<ol>'."\n";
      $output .= '  <li>'. t('Sitemap') .'</li>'."\n";
      $output .= '  <li>'. t('Glossary (Glossary after sitemap)') .'</li>'."\n";
      $output .= '  <li>'. t('Index (Index after glossary)') .'</li>'."\n";
      $output .= '  <li>'. t('Help (general help)') .'</li>'."\n";
      $output .= '  <li>'. t('Imprint and/or contact form') .'</li>'."\n";
      $output .= '  <li>'. t('Copyright and/or imprint') .'</li>'."\n";
      $output .= '</ol>'."\n";
      $output .= '<h5>2.2.6 '. t('Search') .'</h5>'."\n";
      $output .= '<p>'. t('The search form. A service function like the sitemap to find content. Place it close to the servicenavigation.') .'</p>'."\n";
      $output .= '<h5>2.2.6 '. t('Secondary links') .'</h5>'."\n";
      $output .= '<p>'. t('The optional menue or your main navigation: "Navigation" in the head.') .' <span class="icon16 warning">'. ('Note: if you use the secondary links for your main navigation, the standard design is not made it for.') .'</span>'. t('Use only links to concrete pages with content and reference to your website! Example: ') .'</p>'."\n";
      $output .= '<h6>2.2.6.1 '. t('No sidebars (4 links)') .'</h6>'."\n";
      $output .= '<ol>'."\n";
      $output .= '  <li>'. t('<abbr lang="en" xml:lang="en" title="Frequently Asked Questions">FAQ</abbr>s') .'</li>'."\n";
      $output .= '  <li>'. t('Website Help (website specific help)') .'</li>'."\n";
      $output .= '  <li>'. t('Imprint') .'</li>'."\n";
      $output .= '  <li>'. t('Terms of use') .'</li>'."\n";
      $output .= '</ol>'."\n";
      $output .= '<h6>2.2.6.2 '. t('One sidebar (5 links)') .'</h6>'."\n";
      $output .= '<ol>'."\n";
      $output .= '  <li>'. t('Login') .'</li>'."\n";
      $output .= '  <li>'. t('<abbr lang="en" xml:lang="en" title="Frequently Asked Questions">FAQ</abbr>s') .'</li>'."\n";
      $output .= '  <li>'. t('Website Help (website specific help)') .'</li>'."\n";
      $output .= '  <li>'. t('Imprint') .'</li>'."\n";
      $output .= '  <li>'. t('Terms of use') .'</li>'."\n";
      $output .= '</ol>'."\n";
      $output .= '<h6>2.2.6.3 '. t('Two sidebars (6 links)') .'</h6>'."\n";
      $output .= '<ol>'."\n";
      $output .= '  <li>'. t('Login') .'</li>'."\n";
      $output .= '  <li>'. t('Forum') .'</li>'."\n";
      $output .= '  <li>'. t('<abbr lang="en" xml:lang="en" title="Frequently Asked Questions">FAQ</abbr>s') .'</li>'."\n";
      $output .= '  <li>'. t('Website Help (website specific help)') .'</li>'."\n";
      $output .= '  <li>'. t('Imprint') .'</li>'."\n";
      $output .= '  <li>'. t('Terms of use') .'</li>'."\n";
      $output .= '</ol>'."\n";




      $output .= '<h6>2.2.7 '. t('Use color palettes in other software') .'</h6>'."\n";
      $output .= '<ul>'."\n";
      $output .= '  <li class="ok">'. t('<abbr lang="en" xml:lang="en" title="GNU Image Manipulation Program" class="icon16 gimp">GIMP</abbr>. Place the <span class="icon16 text">theme.gpl</span> file in: <span class="icon16 folder">&#126;/.gimp-2.4/palettes/</span>.') .'</li>'."\n";
      $output .= '  <li class="ok">'. t('<span class="icon16 krita">Krita</span>. Place the <span class="icon16 text">theme.gpl</span> file in: <span class="icon16 folder">&#126;/.kde/share/config/colors/</span>.') .'</li>'."\n";
      $output .= '  <li class="ok">'. t('<span class="icon16 kcoloredit">KColorEdit</span>. Place the <span class="icon16 text">theme</span> file in: <span class="icon16 folder">&#126;/.kde/share/config/colors/</span>.') .'</li>'."\n";
      $output .= '  <li class="ok">'. t('<span class="icon16 quanta">Quanta Plus</span>. Place the <span class="icon16 text">theme</span> file in: <span class="icon16 folder">&#126;/.kde/share/config/colors/</span>.') .'</li>'."\n";
      $output .= '  <li class="error">'. t('<span class="icon16 bluefish">Bluefish</span>. Not available.') .'</li>'."\n";
      $output .= '  <li class="error">'. t('<span class="icon16 cssed">CSSED</span>. Not available.') .'</li>'."\n";
      $output .= '  <li class="ok">'. t('<span class="icon16 K">KColorChooser</span>. Place the <span class="icon16 text">theme</span> file in: <span class="icon16 folder">&#126;/.kde/share/config/colors/</span>.') .'</li>'."\n";
      $output .= '  <li class="ok">'. t('<span class="icon16 agave">Agave</span>. Place the <span class="icon16 text">theme.gpl</span> file in: <span class="icon16 folder">&#126;/.local/share/agave/palettes/</span>.') .'</li>'."\n";
      $output .= '  <li class="error">'. t('<span class="icon16 gcolor2">gcolor2</span>. No export.') .'</li>'."\n";
      $output .= '  <li class="ok">'. t('<span class="icon16 inkscape">Inkscape</span>. Place the <span class="icon16 text">theme.gpl</span> file in: <span class="icon16 folder">&#126;/.inkscape/palettes/</span>.') .'</li>'."\n";
      $output .= '  <li class="ok">'. t('<span class="icon16 koffice">KOffice</span>. Place the <span class="icon16 text">theme</span> file in: <span class="icon16 folder">&#126;/.kde/share/config/colors/</span>.') .'</li>'."\n";
      $output .= '  <li class="ok">'. t('<span class="icon16 openofficeorg_2">OpenOffice.org</span>. Place the <span class="icon16 text">theme.soc</span> file in: <span class="icon16 folder">&#126;/.openoffice.org2/user/config</span>. Create and select a Draw image, then under "<span class="icon16 menu">Format > Area</span>" click on the file folder icon "Load Color Palettes" and select the <span class="icon16 text">theme.soc</span> file.') .'</li>'."\n";
      $output .= '  <li class="new">'. t('<span class="icon16 scribus">Scribus</span> 1.3.4. Not released and tested.') .'</li>'."\n";
      $output .= '</ul>'."\n";


      $output .= '<h6>2.2.7 '. t('More information') .'</h5>'."\n";
      $output .= '<ul>'."\n";
      $output .= '  <li>'. l('Web colors', 'http://en.wikipedia.org/wiki/Web_colors') .'</li>'."\n";
      $output .= '  <li><a class="icon16 ods" href="'. $module_base_path .'/source/layout.ods" title="The documentation" >layout.ods</a></li>'."\n";
      $output .= '  <li><a class="icon16 odg" href="'. $module_base_path .'/source/documentation.odg" title="The documentation" >documentation.odg</a></li>'."\n";
      $output .= '</ul>'."\n";


      $output .= '<hr />'."\n";
      $output .= '</div>'."\n";
  return $output;
}