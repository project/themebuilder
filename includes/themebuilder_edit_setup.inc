<?php

/**
 * @file
 * Theme Builder Edit Setup
 */
function themebuilder_setup($form_values = NULL) {
  $form['themebuilder_setup'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site information'),
    '#description' => t('Some input fields are only for none engine') .' <span class="icon16 theme">'. t('themes') .'</span>: '. t('Only for static') .' <abbr lang="en" xml:lang="en" title="Hypertext Markup Language" class="icon16 html">HTML</abbr> '. t('and') .' <abbr lang="en" xml:lang="en" title="Extensible Hypertext Markup Language" class="icon16 html">XHTML</abbr> <span class="icon16 template">'. t('templates') .'</span>.',
    '#collapsible' => true,
    '#collapsed' => TRUE,
  );
  $form['themebuilder_setup']['themebuilder_site_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Site name'),
    '#default_value' => variable_get('themebuilder_site_name', 'Mysite'),
    '#size' => 50,
    '#maxlength' => 100,
    '#description' => t('The name of this website.') .' '. t('Example: "Mysite" or "Mysite.com". Standard: "Mysite"') .'<br /><span class="icon16 info">'. t('Only for static') .' <abbr lang="en" xml:lang="en" title="Extensible Hypertext Markup Language" class="icon16 html">XHTML</abbr> <span class="icon16 template">'. t('templates') .'</span>.</span>',
    '#required' => TRUE,
  );
  $form['themebuilder_setup']['themebuilder_site_slogan'] = array(
    '#type' => 'textfield',
    '#title' => t('Site slogan'),
    '#default_value' => variable_get('themebuilder_site_slogan', 'Slogan'),
    '#size' => 50,
    '#maxlength' => 100,
    '#description' => t("Your site's motto, tag line, or catchphrase (often displayed alongside the title of the site).") .' '. t('Standard: "Slogan"') .'<br /><span class="icon16 info">'. t('Only for static') .' <abbr lang="en" xml:lang="en" title="Extensible Hypertext Markup Language" class="icon16 html">XHTML</abbr> <span class="icon16 template">'. t('templates') .'</span>.</span>',
    '#required' => false,
  );
  $form['themebuilder_setup']['themebuilder_site_mission'] = array(
    '#type' => 'textarea',
    '#title' => t('Mission'),
    '#default_value' => variable_get('themebuilder_site_mission', 'Mission.'),
    '#size' => 50,
    '#maxlength' => 500,
    '#description' => t("Your site's mission or focus statement (often prominently displayed on the front page).") .' '. t('Standard: "Mission."') .'<br /><span class="icon16 info">'. t('Only for static') .' <abbr lang="en" xml:lang="en" title="Extensible Hypertext Markup Language" class="icon16 html">XHTML</abbr> <span class="icon16 template">'. t('templates') .'</span>.</span>',
    '#required' => false,
  );
  $form['themebuilder_setup']['themebuilder_site_footer'] = array(
    '#type' => 'textarea',
    '#title' => t('Footer message'),
    '#default_value' => variable_get('themebuilder_site_footer', 'Footer message'),
    '#size' => 50,
    '#maxlength' => 100,
    '#description' => t("This text will be displayed at the bottom of each page. Useful for adding a copyright notice to your pages.") .' '. t('Standard: "Slogan"') .'<br /><span class="icon16 info">'. t('Only for static') .' <abbr lang="en" xml:lang="en" title="Extensible Hypertext Markup Language" class="icon16 html">XHTML</abbr> <span class="icon16 template">'. t('templates') .'</span>.</span>',
    '#required' => false,
  );
  $form['themebuilder_setup']['themebuilder_theme_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Website url'),
    '#default_value' => variable_get('themebuilder_theme_url', 'http://localhost/drupal6/'),
    '#size' => 50,
    '#maxlength' => 100,
    '#description' => '<span class="icon16 warning">'. t('Enter your website url (example: "http://www.mysite.com") lowercase, without space and special character. Standard: "http://localhost/drupal6/"') .'</span>',
    '#required' => TRUE,
  );
  $form['themebuilder_setup']['themebuilder_theme_email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail address'),
    '#default_value' => variable_get('themebuilder_theme_email', 'info@mysite.com'),
    '#size' => 50,
    '#maxlength' => 100,
    '#description' => '<span class="icon16 warning">'. t('Enter the <span lang="en" xml:lang="en"><abbr title="Electronic">E</abbr>-Mail</span> adress of the webmaster or administer in lowercase and without space. Standard: "info@mysite.com"') .'</span>',
    '#required' => TRUE,
  );
  $form['themebuilder_setup']['themebuilder_theme_timezone'] = array(
    '#type' => 'select',
    '#title' => t('Default time zone'),
    '#options' => array(
      '-11:00' => '-11h',
      '-10:00' => '-10h',
      '-09:00' => '-9h',
      '-08:00' => '-8h',
      '-07:00' => '-7h',
      '-06:00' => '-6h',
      '-05:00' => '-5h',
      '-04:00' => '-4h',
      '-03:00' => '-3h',
      '-02:00' => '-2h',
      '-01:00' => '-1h',
      '00:00' => '0h',
      '+01:00' => '+1h',
      '+02:00' => '+2h',
      '+03:00' => '+3h',
      '+04:00' => '+4h',
      '+05:00' => '+5h',
      '+06:00' => '+6h',
      '+07:00' => '+7h',
      '+08:00' => '+8h',
      '+09:00' => '+9h',
      '+10:00' => '+10h',
      '+11:00' => '+11h',
      '+12:00' => '+12h',
      '+13:00' => '+13h',
      '+14:00' => '+14h',
    ),
    '#default_value' => variable_get('themebuilder_theme_timezone', '+05:00'),
    '#description' => t('Select the default site time zone.'),
  );
  $form['themebuilder_setup_theme'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme settings'),
    '#collapsible' => true,
    '#collapsed' => false,
  );
  $form['themebuilder_setup_theme']['themebuilder_theme_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Theme name'),
    '#default_value' => variable_get('themebuilder_theme_name', 'Standard Theme'),
    '#size' => 50,
    '#maxlength' => 100,
    '#description' => t('Enter a name of your theme for the info file.'),
    '#required' => TRUE,
  );
  $form['themebuilder_setup_theme']['themebuilder_theme_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Theme directory name'),
    '#default_value' => variable_get('themebuilder_theme_dir', 'standard'),
    '#size' => 50,
    '#maxlength' => 100,
    '#description' => '<span class="icon16 warning">'. t('Enter a name of your theme in lowercase, without space and special character.') .'</span>',
    '#required' => TRUE,
  );
  $form['themebuilder_setup_theme']['themebuilder_theme_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Theme description'),
    '#default_value' => variable_get('themebuilder_theme_description', 'Standard theme with blue base color.'),
    '#size' => 100,
    '#maxlength' => 500,
    '#description' => t('Enter a short description of your theme for the info file.') .'<br /><span class="icon16 info">'. t('Theme technical details: "Block order" and "Theme layout" will be generate automatically.') .'</span>',
    '#required' => TRUE,
  );
  $form['themebuilder_setup_theme']['themebuilder_theme_version'] = array(
    '#type' => 'select',
    '#title' => t('Theme for Drupal version'),
    '#options' => array(
      '0' => 'none',
      '4' => '4.x',
      '5' => '5.x',
      '6' => '6.x',
      '7' => '7.x',
    ),
    '#default_value' => variable_get('themebuilder_theme_version', '6'),
    '#description' => t('Enter the version of your theme for the info file.'),
    '#required' => TRUE,
  );
  $form['themebuilder_setup_theme']['themebuilder_engine'] = array(
    '#type' => 'select',
    '#title' => t('Theme engine'),
    '#options' => array(
      'none' => t('No engine - (X)HTML Template') .'[1]',
      'xtemplate' => t('XTemplate') .'[2]',
      'smarty' => t('Smarty Template') .'[3]',
      'drupal' => t('Drupal PHP Template') .'[4]',
    ),
    '#default_value' => variable_get('themebuilder_engine', 'drupal'), // default to Drupal PHP Template
    '#description' => t('Select the theme engine. Standard:') .'<span class="icon16 drupal">Drupal</span> <abbr title="PHP: Hypertext Preprocessor" class="icon16 php">PHP</abbr>'. t('Template') .'.<br /><span class="icon16 ok">[1] '. t('Static') .' <abbr lang="en" xml:lang="en" title="Extensible Hypertext Markup Language" class="icon16 html">XHTML</abbr> '. t('Template') .'. <abbr title="Content Management System">CMS</abbr> indipendend template. (<span class="icon16 warning">'. t('Do not works with') .' <span class="icon16 drupal">Drupal</span>!</span>)</span><br /><span class="icon16 error">[2] <span class="icon16 drupal">'. l('Drupal XTemplate engine', 'http://drupal.org/project/xtemplate') .'</span> ('. l('phpxtemplate.org', 'http://www.phpxtemplate.org') .'). '. t('For') .' <span class="icon16 drupal">Drupal</span> 4.x. '. t('Do not works with') .' <span class="icon16 drupal">Drupal</span> 5/6! <span class="icon16 warning">'. t('Not tested') .'</span>.</span><br /><span class="icon16 warning">[3] <span class="icon16 drupal">'. l('Drupal Smarty theme engine', 'http://drupal.org/project/smarty') .'</span> ('. l('smarty.net', 'http://www.smarty.net/') .'). '. t('For <span class="icon16 drupal">Drupal</span> 4.x, 5.x and 6.x. Download and install the engine.') .' <span class="icon16 warning">'. t('Not tested') .'</span></span><br /><span class="icon16 ok">[4] <a class="icon16 drupal" href="http://drupal.org/project/phptemplate" >Drupal "<abbr title="PHP: Hypertext Preprocessor" class="icon16 php">PHP</abbr>'. t('Template') .'" '. t('engine') .'</a>. '. t('For <span class="icon16 drupal">Drupal</span> 4.x, 5.x and 6.x. Official and standard core theme engine. <em class="icon16 important">Tested only on <span class="icon16 drupal">Drupal</span> 6.x!</em>') .'</span>',
  );
  $form['themebuilder_devel'] = array(
    '#type' => 'fieldset',
    '#title' => t('Code'),
    '#collapsible' => true,
    '#collapsed' => true,
  );
  $form['themebuilder_devel']['themebuilder_theme_devel_errors'] = array(
    '#type' => 'radios',
    '#title' => t('Show code errors'),
    '#options' => array(
      t('Show no code errors.'),
      t('Show basic code errors.'),
      t('Show strict code errors.'),
    ),
    '#default_value' => variable_get('themebuilder_theme_devel_errors', 1),
    '#description' => t('Show invalid, no web accessible or old code permanent with red color. <em>Note:</em> For the end user also helpful! Example, if he forget the "alt" text for a image.') .' <br /><span class="icon16 warning">'. t('Only for: Mozilla 1.4+, Netscape Navigator 7.1+, Firefox 1+, Konqueror 3+, Safari 2+') .'</span>'
  );
  return system_settings_form($form);
}