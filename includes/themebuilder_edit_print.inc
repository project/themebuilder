<?php

/**
 * @file
 * Theme Builder Edit Print
 */
function themebuilder_print() {
  $form['themebuilder_print'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global settings'),
    '#collapsible' => true,
    '#collapsed' => false,
  );
  $form['themebuilder_print']['themebuilder_print_mission'] = array(
    '#type' => 'checkbox',
    '#title' => t('Print mission'),
    '#default_value' => variable_get('themebuilder_print_mission', 1),
    '#description' => t('Print mission only on the index page.'),
  );
  return system_settings_form($form);
}