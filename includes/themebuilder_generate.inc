<?php

/**
 * @file
 * Theme Builder Generate
 */
function themebuilder_generate() {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';

if ($themebuilder_engine == 'none') {
  $themebuilder_engine_name_plain = "XHTML Template";
}
else if ($themebuilder_engine == 'xtemplate') {
  $themebuilder_engine_name_plain = "XTemplate";
}
else if ($themebuilder_engine == 'smarty') {
  $themebuilder_engine_name_plain = "Smarty Template";
}
else if ($themebuilder_engine == 'drupal') {
  $themebuilder_engine_name_plain = "PHPTemplate";
}
else {
  $themebuilder_engine_name_plain = "PHPTemplate";
}
  if (!file_exists($themes_path)) {
    drupal_set_message( t('The directory: ') .'<em class="icon16 folder">"'. base_path() . $themes_path .'"</em> '. t('do not exists. Create this file folder "themes" with your file browser <span class="icon16 konqueror">Konqueror</span> (<span class="icon16 linux">Linux</span>), <span class="icon16 ie">Explorer</span> (<abbr title="Windows" class="icon16 win">Win</abbr>) or <span class="icon16 mac">Finder</span> (<abbr title="Macintosh" class="icon16 mac">Mac</abbr>) and ') .' '. l('try again', 'themebuilder/generate' ) .'.', 'error' );
  //$output .= 'Change it and '. l('try again', 'themebuilder/generate' ).'</p>';
  $output .= '<p>'. l('Try again', 'themebuilder/generate' ) .'</p>';
  }
  else if (!is_writable($themes_path)) {
    drupal_set_message( t('The directory: ') .'<em class="icon16 folder_red">"'. base_path() . $themes_path .'"</em> '. t('is not writable. Last modified: ') . date("F d Y H:i:s.", filemtime($theme_path)) . t('Change the premission of this file folder with your file browser <span class="icon16 konqueror">Konqueror</span> (<span class="icon16 linux">Linux</span>), <span class="icon16 ie">Explorer</span> (<abbr title="Windows" class="icon16 win">Win</abbr>) or <span class="icon16 mac">Finder</span> (<abbr title="Macintosh" class="icon16 mac">Mac</abbr>) and ') . l('try again', 'themebuilder/generate') .'.', 'error' );
  //$output .= 'Change it and '. l('try again', 'themebuilder/generate' ).'</p>';
  $output .= '<p>'. l('Try again', 'themebuilder/generate' ) .'</p>';
  }

  else if (file_exists($theme_path)) {
//     drupal_set_message( t('The theme <em class="icon16 theme">"'. $theme_path .'"</em> exists in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>. Last modified: ', date("F d Y H:i:s.", filemtime($theme_path)) .' Change the theme directory name in ') . l('Theme builder', 'themebuilder' ) . t(', delete or rename the same named theme file with your file browser and build it again on:') .' '. l('Generate', 'themebuilder/generate' ) .'.', 'error' );
    drupal_set_message( t('The directory: ') .'<em class="icon16 folder">"'. base_path() . $theme_path .'"</em> '. t('exists. Last modified: ') . date("F d Y H:i:s.", filemtime($theme_path)) .' '. t('Delete or rename this file folder with your file browser <span class="icon16 konqueror">Konqueror</span> (<span class="icon16 linux">Linux</span>), <span class="icon16 ie">Explorer</span> (<abbr title="Windows" class="icon16 win">Win</abbr>) or <span class="icon16 mac">Finder</span> (<abbr title="Macintosh" class="icon16 mac">Mac</abbr>) or change the "Theme directory name" in:') .' '. l('Theme setup', 'themebuilder/setup' ) .' '.  t('and') .' '. l('try again.', 'themebuilder/generate' ) .', ', 'error' );
  //$output .= 'Change it and '. l('try again', 'themebuilder/generate' ).'</p>';
  $output .= '<p>'. l('Try again', 'themebuilder/generate' ) .'</p>';
  }
  else if (!file_exists($theme_path)) {
  $output .= '<p class="icon16 app generate">'. l( t('Generate') .' '.  $themebuilder_engine_name_plain, 'themebuilder/generate_files' ) .'</p>';
  }
  return $output;

}


/**
 * @file
 * Theme Builder Generate all files
 */
function themebuilder_generate_files() {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';
  if (!is_writable($themes_path)) {
    drupal_set_message( t('The directory: ') .'<em class="icon16 folder_red">"'. base_path() . drupal_get_path('module', 'themebuilder') .'/'. $themebuilder_theme_dir .'"</em> '. t('is not writable. Change it and ') . l('try again', 'themebuilder/generate' ) .'.', 'error' );
  //$output .= 'Change it and '. l('try again', 'themebuilder/generate' ).'</p>';
  $output .= '<p>'. l('Try again', 'themebuilder/generate' ) .'</p>';
  }
  else if (!file_exists($theme_path)) {

if ($themebuilder_engine == 'drupal') {
  $themebuilder_engine_name = "PHPTemplate";
  $file_info = $themebuilder_theme_dir .".info";
  $data_pagetpl = generate_pagetpl();
  $file_pagetpl = "page.tpl.php";
  $message = l('enable the theme', 'admin/build/themes');
}
else if ($themebuilder_engine == 'smarty') {
  $themebuilder_engine_name = "Smarty Template";
  $file_info = $themebuilder_theme_dir .".info";
  $data_pagetpl = generate_smarty();
  $file_pagetpl = "page.tpl";
  $message = l('enable the theme', 'admin/build/themes');
}
else if ($themebuilder_engine == 'xtemplate') {
  $themebuilder_engine_name = "XTemplate";
  $file_info = $themebuilder_theme_dir .".info";
  $data_pagetpl = generate_xtemplate();
  $file_pagetpl = "xtemplate.xtmpl";
  $message = l('enable the theme', 'admin/build/themes');
}
else if ($themebuilder_engine == 'none') {
  $themebuilder_engine_name = "XHTML Template";
  $file_info = $themebuilder_theme_dir .".txt";
  $data_pagetpl = generate_xhtml();
  $file_pagetpl = "index.html";
  $message = '<a href="'. $theme_path .'/index.html">'. t('view the') .' '. $valid_abbr .' <span class="icon16 theme">'. t('theme') .'</span></a>';
}
else {
  $themebuilder_engine_name = "PHPTemplate";
  $data_pagetpl = generate_pagetpl();
  $file_pagetpl = "page.tpl.php";
  $message = l('enable the theme', 'admin/build/themes');
}

/**
 * Generate a directory
 */
  if (!mkdir($theme_path, 0777)) {
    drupal_set_message( t('The directory: <em class="icon16 folder">"'. $themebuilder_theme_dir .'"</em> has been generated unsuccessfuly in the directory: <em class="icon16 folder">"'. $themes_path .'"</em>.'));
  }

/**
 * Copyright file
 */
  $copyright = $module_path_theme .'/LICENSE.txt';
  $copyright_copy = $theme_path .'/LICENSE.txt';
  if (!copy($copyright, $copyright_copy)) {
    drupal_set_message( t('The text file: <em class="icon16 text">"'. $copyright .'"</em> has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }

/**
 * Screenshot image
 */
  $screenshot_png = $module_path_theme .'/screenshot.png';
  $screenshot_copy = $theme_path .'/screenshot.png';
  if (!copy($screenshot_png, $screenshot_copy)) {
    drupal_set_message( t('The screenshot: "<em class="icon16 image">'. $screenshot_png .'"</em> has been copied unsuccessfuly in the directory: "<em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }

/**
 * Copy the icon in to the directory
 */
  if ($themebuilder_shortcuticon_display_default == 0) {
  $shortcut_icon = $themebuilder_link_shortcut_icon;
  }
  else {
  $shortcut_icon = $module_path_theme .'/favicon.ico';;
  }
  $shortcut_icon_copy = $theme_path .'/favicon.ico';
  if (!copy($shortcut_icon, $shortcut_icon_copy)) {
    drupal_set_message( t('The shortcut icon: "<em class="icon16 image">'. $shortcut_icon .'"</em> has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }

/**
 * Generate a "fonts" directory in to the theme directory
 */
  if (!mkdir($theme_path .'/fonts', 0777)) {
    drupal_set_message( t('The directory: "<em class="icon16 folder">'. $themebuilder_theme_dir .'/fonts</em>" has been generated unsuccessfuly in the directory: <em class="icon16 folder">"'. $themes_path .'"</em>.'));
  }

/**
 * Generate a "color" directory in to the theme directory
 */
  if (!mkdir($theme_path .'/color', 0777)) {
    drupal_set_message( t('The directory: "<em class="icon16 folder">'. $themebuilder_theme_dir .'/color</em>" has been generated unsuccessfuly in the directory: <em class="icon16 folder">"'. $themes_path .'"</em>.'));
  }

/**
 * Generate a "images" directory in to the theme directory
 */
  if (!mkdir($theme_path .'/images', 0777)) {
    drupal_set_message( t('The directory: "<em class="icon16 folder">'. $themebuilder_theme_dir .'/images</em>" has been generated unsuccessfuly in the directory: <em class="icon16 folder">"'. $themes_path .'"</em>.'));
  }
/**
 * Logos
 */
/*
   $image_logo = variable_get('image_logo', '/misc/drupal.png');
   $image_logo_copy = $theme_path .'/logo.png';
 */
  $image_logo_screen_png = $module_path_theme .'/images/logo_screen.png';
  $image_logo_screen_gif = $module_path_theme .'/images/logo_screen.gif';
  $image_logo_gif = $module_path_theme .'/images/logo.gif';
  if (copy($image_logo_screen_png, $theme_path .'/images/logo_screen.png')) {
    copy($image_logo_screen_gif, $theme_path .'/images/logo_screen.gif');
    copy($image_logo_gif, $theme_path .'/images/logo.gif');
  }
  else {
    drupal_set_message( t('The logos <em class="icon16 image">"'. $image_logo_screen_png .'"</em>  has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }

/**
 * Valid emblem icon
 */
  $copyright_icon = $module_path_theme .'/images/emblem-default8.png';
  $copyright_icon_copy = $theme_path .'/images/emblem-default8.png';
  if (!copy($copyright_icon, $copyright_icon_copy)) {
    drupal_set_message( t('The valid logos: <em class="icon16 image">"'. $copyright_icon .'"</em> has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }
/**
 * Copy the image files in to the theme directory "images"
 *
 * Background body
 */
  $body_bg = $module_path_theme .'/images/body.jpg';
  if (copy($body_bg, $theme_path .'/images/body.jpg')) {
  }
  else {
    drupal_set_message( t('The image: "<em class="icon16 image">'. $body_bg .'"</em> has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }

/**
 * Background head
 */
  $head_bg = $module_path_theme .'/images/div_header.jpg';
  if (copy($head_bg, $theme_path .'/images/div_header.jpg')) {
  }
  else {
    drupal_set_message( t('The image: "<em class="icon16 image">'. $head_bg .'"</em> has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }

/**
 * Background main
 */
  $main_bg = $module_path_theme .'/images/div_main.jpg';
  if (copy($main_bg, $theme_path .'/images/div_main.jpg')) {
  }
  else {
    drupal_set_message( t('The image: "<em class="icon16 image">'. $main_bg .'"</em> has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }

/**
 * Background sidebar
 */
  $sidebar_bg = $module_path_theme .'/images/div_sidebar.jpg';
  if (copy($sidebar_bg, $theme_path .'/images/div_sidebar.jpg')) {
  }
  else {
    drupal_set_message( t('The image: "<em class="icon16 image">'. $sidebar_bg .'"</em> has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }

/**
 * Background sidebar
 */
  $content_bg = $module_path_theme .'/images/div_content.jpg';
  if (copy($content_bg, $theme_path .'/images/div_content.jpg')) {
  }
  else {
    drupal_set_message( t('The image: "<em class="icon16 image">'. $content_bg .'"</em> has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }

/**
 * Background footer
 */
  $footer_bg = $module_path_theme .'/images/div_footer.jpg';
  if (copy($footer_bg, $theme_path .'/images/div_footer.jpg')) {
  }
  else {
    drupal_set_message( t('The image: "<em class="icon16 image">'. $footer_bg .'"</em> has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }
/**
 * Background tab
 */
  $primary_bg = $module_path_theme .'/images/div_primary_a.jpg';
  if (copy($primary_bg, $theme_path .'/images/div_primary_a.jpg')) {
  }
  else {
    drupal_set_message( t('The image: "<em class="icon16 image">'. $primary_bg .'"</em> has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }
/**
 * Search form icon
 */
  $search_bg = $module_path_theme .'/images/div_search_input_formtext.jpg';
  if (copy($search_bg, $theme_path .'/images/div_search_input_formtext.jpg')) {
    copy($module_path_theme .'/images/div_search_input_formtext_active.jpg', $theme_path .'/images/div_search_input_formtext_active.jpg');
  }
  else {
    drupal_set_message( t('The image: "<em class="icon16 image">'. $search_bg .'"</em> has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }
/**
 * Form background
 */
  if (copy($module_path_theme .'/images/input.jpg', $theme_path .'/images/input.jpg')) {
    copy($module_path_theme .'/images/input_active.jpg', $theme_path .'/images/input_active.jpg');
    copy($module_path_theme .'/images/input_editsubmit.jpg', $theme_path .'/images/input_editsubmit.jpg');
    copy($module_path_theme .'/images/input_editsubmit_hover.jpg', $theme_path .'/images/input_editsubmit_hover.jpg');
    copy($module_path_theme .'/images/input_editsubmit_active.jpg', $theme_path .'/images/input_editsubmit_active.jpg');
    copy($module_path_theme .'/images/input_editreset_hover.jpg', $theme_path .'/images/input_editreset_hover.jpg');
    copy($module_path_theme .'/images/input_editreset_active.jpg', $theme_path .'/images/input_editreset_active.jpg');
  }
  else {
    drupal_set_message( t('The images: "<em class="icon16 image">input_form_x.jpg</em>" has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }
/*
$search =  $module_path_theme .'/images/search.gif';
file_copy(&$search, $theme_path .'/images/search.gif', $replace = FILE_EXISTS_ERROR);
 */

/**
 * Table images
 */
  if (copy($module_path_theme .'/images/table_horizontal.jpg', $theme_path .'/images/table_horizontal.jpg')) {
    copy($module_path_theme .'/images/table_hover.jpg', $theme_path .'/images/table_hover.jpg');
    copy($module_path_theme .'/images/tfoot.jpg', $theme_path .'/images/tfoot.jpg');
    copy($module_path_theme .'/images/thead_hover.jpg', $theme_path .'/images/thead_hover.jpg');
    copy($module_path_theme .'/images/thead.jpg', $theme_path .'/images/thead.jpg');
    copy($module_path_theme .'/images/yes.gif', $theme_path .'/images/yes.gif');
    copy($module_path_theme .'/images/yes_hover.gif', $theme_path .'/images/yes_hover.gif');
    copy($module_path_theme .'/images/no.gif', $theme_path .'/images/no.gif');
    copy($module_path_theme .'/images/no_hover.gif', $theme_path .'/images/no_hover.gif');
    copy($module_path_theme .'/images/unknown.gif', $theme_path .'/images/unknown.gif');
  }
  else {
    drupal_set_message( t('The images: "<em class="icon16 image">table_x.jpg</em>" has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }
/**
 * Skip, internal and external link icons
 */
  if (copy($module_path_theme .'/images/internal.gif', $theme_path .'/images/internal.gif')) {
    copy($module_path_theme .'/images/internal8.gif', $theme_path .'/images/internal8.gif');
    copy($module_path_theme .'/images/external.gif', $theme_path .'/images/external.gif');
    copy($module_path_theme .'/images/external8.gif', $theme_path .'/images/external8.gif');
    copy($module_path_theme .'/images/skip_up.gif', $theme_path .'/images/skip_up.gif');
    copy($module_path_theme .'/images/skip_first.gif', $theme_path .'/images/skip_first.gif');
    copy($module_path_theme .'/images/skip_last.gif', $theme_path .'/images/skip_last.gif');

    copy($module_path_theme .'/images/internal.png', $theme_path .'/images/internal.png');
    copy($module_path_theme .'/images/internal8.png', $theme_path .'/images/internal8.png');
    copy($module_path_theme .'/images/external.png', $theme_path .'/images/external.png');
    copy($module_path_theme .'/images/external8.png', $theme_path .'/images/external8.png');
    copy($module_path_theme .'/images/skip_up.png', $theme_path .'/images/skip_up.png');
    copy($module_path_theme .'/images/skip_first.png', $theme_path .'/images/skip_first.png');
    copy($module_path_theme .'/images/skip_last.png', $theme_path .'/images/skip_last.png');
  }
  else {
    drupal_set_message( t('The images: "<em class="icon16 image">in/external_x.png</em>" has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }
/**
 * Rel and rev icons
 */
  if (copy($module_path_theme .'/images/rel_alternate.gif', $theme_path .'/images/rel_alternate.gif')) {
    copy($module_path_theme .'/images/rel_author.gif', $theme_path .'/images/rel_author.gif');
    copy($module_path_theme .'/images/rel_appendix.gif', $theme_path .'/images/rel_appendix.gif');
    copy($module_path_theme .'/images/rel_bookmark.gif', $theme_path .'/images/rel_bookmark.gif');
    copy($module_path_theme .'/images/rel_chapter.gif', $theme_path .'/images/rel_chapter.gif');
    copy($module_path_theme .'/images/rel_contents.gif', $theme_path .'/images/rel_contents.gif');
    copy($module_path_theme .'/images/rel_copyright.gif', $theme_path .'/images/rel_copyright.gif');
    copy($module_path_theme .'/images/rel_glossary.gif', $theme_path .'/images/rel_glossary.gif');
    copy($module_path_theme .'/images/rel_help.gif', $theme_path .'/images/rel_help.gif');
    copy($module_path_theme .'/images/rel_index.gif', $theme_path .'/images/rel_index.gif');
    copy($module_path_theme .'/images/rel_next.gif', $theme_path .'/images/rel_next.gif');
    copy($module_path_theme .'/images/rel_prev.gif', $theme_path .'/images/rel_prev.gif');
    copy($module_path_theme .'/images/rel_section.gif', $theme_path .'/images/rel_section.gif');
    copy($module_path_theme .'/images/rel_start.gif', $theme_path .'/images/rel_start.gif');
    copy($module_path_theme .'/images/rel_subsection.gif', $theme_path .'/images/rel_subsection.gif');
    
    copy($module_path_theme .'/images/rel_alternate.png', $theme_path .'/images/rel_alternate.png');
    copy($module_path_theme .'/images/rel_author.png', $theme_path .'/images/rel_author.png');
    copy($module_path_theme .'/images/rel_appendix.png', $theme_path .'/images/rel_appendix.png');
    copy($module_path_theme .'/images/rel_bookmark.png', $theme_path .'/images/rel_bookmark.png');
    copy($module_path_theme .'/images/rel_chapter.png', $theme_path .'/images/rel_chapter.png');
    copy($module_path_theme .'/images/rel_contents.png', $theme_path .'/images/rel_contents.png');
    copy($module_path_theme .'/images/rel_copyright.png', $theme_path .'/images/rel_copyright.png');
    copy($module_path_theme .'/images/rel_glossary.png', $theme_path .'/images/rel_glossary.png');
    copy($module_path_theme .'/images/rel_help.png', $theme_path .'/images/rel_help.png');
    copy($module_path_theme .'/images/rel_index.png', $theme_path .'/images/rel_index.png');
    copy($module_path_theme .'/images/rel_next.png', $theme_path .'/images/rel_next.png');
    copy($module_path_theme .'/images/rel_prev.png', $theme_path .'/images/rel_prev.png');
    copy($module_path_theme .'/images/rel_section.png', $theme_path .'/images/rel_section.png');
    copy($module_path_theme .'/images/rel_start.png', $theme_path .'/images/rel_start.png');
    copy($module_path_theme .'/images/rel_subsection.png', $theme_path .'/images/rel_subsection.png');
  }
  else {
    drupal_set_message( t('The images: "<em class="icon16 image">rel_x.png</em>" has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }
/**
 * Mimetype icons
 */
  if (copy($module_path_theme .'/images/x-text-generic.gif', $theme_path .'/images/x-text-generic.gif')) {
    copy($module_path_theme .'/images/x-text-html.gif', $theme_path .'/images/x-text-html.gif');
    copy($module_path_theme .'/images/x-image.gif', $theme_path .'/images/x-image.gif');
    copy($module_path_theme .'/images/x-office-presentation.gif', $theme_path .'/images/x-office-presentation.gif');
    copy($module_path_theme .'/images/x-office-drawing.gif', $theme_path .'/images/x-office-drawing.gif');
    copy($module_path_theme .'/images/x-office-document.gif', $theme_path .'/images/x-office-document.gif');
    copy($module_path_theme .'/images/x-office-address-book.gif', $theme_path .'/images/x-office-address-book.gif');
    copy($module_path_theme .'/images/x-office-calendar.gif', $theme_path .'/images/x-office-calendar.gif');
    copy($module_path_theme .'/images/x-office-spreadsheet.gif', $theme_path .'/images/x-office-spreadsheet.gif');
    copy($module_path_theme .'/images/x-text-generic.png', $theme_path .'/images/x-text-generic.png');
    copy($module_path_theme .'/images/x-text-html.png', $theme_path .'/images/x-text-html.png');
    copy($module_path_theme .'/images/x-image.png', $theme_path .'/images/x-image.png');
    copy($module_path_theme .'/images/x-office-presentation.png', $theme_path .'/images/x-office-presentation.png');
    copy($module_path_theme .'/images/x-office-drawing.png', $theme_path .'/images/x-office-drawing.png');
    copy($module_path_theme .'/images/x-office-document.png', $theme_path .'/images/x-office-document.png');
    copy($module_path_theme .'/images/x-office-address-book.png', $theme_path .'/images/x-office-address-book.png');
    copy($module_path_theme .'/images/x-office-calendar.png', $theme_path .'/images/x-office-calendar.png');
    copy($module_path_theme .'/images/x-office-spreadsheet.png', $theme_path .'/images/x-office-spreadsheet.png');

  }
  else {
    drupal_set_message( t('The images: "<em class="icon16 image">x-mymetytes.png</em>" has been copied unsuccessfuly in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>.'), 'error');
  }

/**
 * Generate info file (theme.info)
 */
$data_info = generate_info();

$file_pointer = fopen($theme_path ."/". $file_info, "w");
fputs($file_pointer, $data_info);
fclose($file_pointer);
// drupal_set_message( t('The file: "<em>'. $file_info .'"</em> has been generated successfuly in the directory: <em class="icon16 folder">"'. $themes_path . $themebuilder_theme_dir .'/</em>".'));
$success = TRUE;

/**
 * Generate HTML structure (page.tpl.php)
 */

$file_pointer = fopen($theme_path ."/". $file_pagetpl, "w");
fputs($file_pointer, $data_pagetpl);
fclose($file_pointer);
$success = TRUE;

  if ($success) {
  // drupal_set_message( t('The file: "<em>'. $file_pagetpl .'"</em> has been generated successfuly in the directory: <em class="icon16 folder">"'. $themes_path . $themebuilder_theme_dir .'/</em>".'));
  }
  else {
    drupal_set_message( t('The file: <em class="icon16 text">"'. $file_pagetpl .'"</em> can not generated successfuly in the directory: <em class="icon16 folder">"'. $themes_path . $themebuilder_theme_dir .'/</em>".'));
  }

/**
 * Generate base CSS
 */
$data_basecss = generate_basecss();
$file_basecss = "base.css";
$file_pointer = fopen($theme_path ."/". $file_basecss, "w");
fputs($file_pointer, $data_basecss);
fclose($file_pointer);
$success = TRUE;

  if ($success) {
  //  drupal_set_message( t('The file: "<em>'. $file_layoutcss .'"</em> has been generated successfuly in the directory: <em class="icon16 folder">"'. $themes_path . $themebuilder_theme_dir .'/</em>".'));
  }
  else {
    drupal_set_message( t('The file: <em class="icon16 css">"'. $file_basecss .'"</em> was not generated successfuly in the directory: <em class="icon16 folder">"'. $themes_path . $themebuilder_theme_dir .'/</em>".'));
  }
  
/**
 * Generate style CSS
 */

$data_layoutcss = generate_stylecss();
$file_layoutcss = "style.css";
$file_pointer = fopen($theme_path ."/". $file_layoutcss, "w");
fputs($file_pointer, $data_layoutcss);
fclose($file_pointer);
$success = TRUE;

  if ($success) {
  //  drupal_set_message( t('The file: "<em>'. $file_layoutcss .'"</em> has been generated successfuly in the directory: <em class="icon16 folder">"'. $themes_path . $themebuilder_theme_dir .'/</em>".'));
  }
  else {
    drupal_set_message( t('The file: <em class="icon16 css">"'. $file_layoutcss .'"</em> was not generated successfuly in the directory: <em class="icon16 folder">"'. $themes_path . $themebuilder_theme_dir .'/</em>".'));
  }
  
/**
 * Generate layout CSS
 */
$data_layoutcss = generate_layoutcss();
$file_layoutcss = "layout.css";
$file_pointer = fopen($theme_path ."/". $file_layoutcss, "w");
fputs($file_pointer, $data_layoutcss);
fclose($file_pointer);
$success = TRUE;

  if ($success) {
  //  drupal_set_message( t('The file: "<em>'. $file_layoutcss .'"</em> has been generated successfuly in the directory: <em class="icon16 folder">"'. $themes_path . $themebuilder_theme_dir .'/</em>".'));
  }
  else {
    drupal_set_message( t('The file: <em class="icon16 css">"'. $file_layoutcss .'"</em> was not generated successfuly in the directory: <em class="icon16 folder">"'. $themes_path . $themebuilder_theme_dir .'/</em>".'));
  }

/**
 * Generate hacks CSS
 */
$data_hackscss = generate_hackscss();
$file_hackscss = "hacks.css";
$file_pointer = fopen($theme_path ."/". $file_hackscss, "w");
fputs($file_pointer, $data_hackscss);
fclose($file_pointer);
$success = TRUE;

  if ($success) {
  //  drupal_set_message( t('The file: "<em>'. $file_hackscss .'"</em> has been generated successfuly in the directory: <em class="icon16 folder">"'. $themes_path . $themebuilder_theme_dir .'/</em>".'));
  }
  else {
    drupal_set_message( t('The file: <em class="icon16 css">"'. $file_hackscss .'"</em> was not generated successfuly in the directory: <em class="icon16 folder">"'. $themes_path . $themebuilder_theme_dir .'/</em>".'));
  }
  
/**
 * Generate print CSS
 */
$data_printcss = generate_printcss();
$file_printcss = "print.css";
$file_pointer = fopen($theme_path ."/". $file_printcss, "w");
fputs($file_pointer, $data_printcss);
fclose($file_pointer);
$success = TRUE;

/**
* Generate config file
*/
$data_config = serialize($themebuilder_vars);
$file_config = $themebuilder_theme_dir .'.cfg';
$file_pointer = fopen($theme_path ."/". $file_config, "w");
fputs($file_pointer, $data_config);
fclose($file_pointer);

/**
* Generate ini file
*/
$data_ini = '';
foreach ($themebuilder_vars as $key => $value) {
$data_ini .= $key .' = '. $value ."\n";
}
$file_ini = $themebuilder_theme_dir .'.ini';
$file_pointer = fopen($theme_path ."/". $file_ini, "w");
fputs($file_pointer, $data_ini);
fclose($file_pointer);

/**
 * Generate JavaScript
 */
$data_printcss = generate_javascriptjs();
$file_printcss = $themebuilder_theme_dir .'.js';
$file_pointer = fopen($theme_path ."/". $file_printcss, "w");
fputs($file_pointer, $data_printcss);
fclose($file_pointer);
$success = TRUE;

/**
 * Generate the GIMP Color Theme Palette (theme.gpl)
 */
$data_gimp_palette = generate_gimp_palette();
$file_palette = $themebuilder_theme_dir .'.gpl';
$file_pointer = fopen($theme_path ."/color/". $file_palette, "w");
fputs($file_pointer, $data_gimp_palette);
fclose($file_pointer);
$success = TRUE;

/**
 * Generate the KDE Color Theme Palette (theme)
 */
$data_kcoloredit_palette = generate_kde_palette();
$file_palette = $themebuilder_theme_dir .'';
$file_pointer = fopen($theme_path ."/color/". $file_palette, "w");
fputs($file_pointer, $data_kcoloredit_palette);
fclose($file_pointer);
$success = TRUE;

/**
 * Generate the OpenOffice.org Color Theme Palette (theme.soc)
 */
$data_openoffie_palette = generate_openoffice_palette();
$file_palette = $themebuilder_theme_dir .'.soc';
$file_pointer = fopen($theme_path ."/color/". $file_palette, "w");
fputs($file_pointer, $data_openoffie_palette);
fclose($file_pointer);
$success = TRUE;

  if ($success) {
  drupal_set_message( t('The theme:') .' <em class="icon16 theme">"'. $themebuilder_theme_name .'"</em> '. t('has been generated successfuly in the directory:') .' <em class="icon16 folder">"'. $theme_path .'"</em>. Last modified: '. date("F d Y H:i:s.", filemtime($theme_path)) .' Filesize: '. round(filesize($theme_path) / (1024), 2) .'kb', 'ok');
  }
  else {
  drupal_set_message( t('The theme:') .' <em class="icon16 theme">"'. $themebuilder_theme_name .'"</em> '. t('has been not generated successfuly in the directory:') .' <em class="icon16 folder">"'. $theme_path .'"</em>. Last modified: '. date("F d Y H:i:s.", filemtime($theme_path)) .' Filesize: '. round(filesize($theme_path) / (1024), 2) .'kb', 'error');
  }
}

else {
    drupal_set_message( t('The theme <em class="icon16 theme">"'. $theme_path .'"</em> exists in the directory: <em class="icon16 folder">"'. $theme_path .'"</em>. Last modified: '. date("F d Y H:i:s.", filemtime($theme_path)) .' Change the theme directory name in ') . l('Theme builder', 'themebuilder' ) . t(', delete or rename the same named theme file with your file browser and build it again on:') .' '. l('Generate', 'themebuilder/generate' ) .'.', 'error' );
    $message =  l('try again', 'themebuilder/generate' );
}
  $output .= '<p>'. l('Back', 'themebuilder' ) .' or '. $message .'.</p>';

  return $output;
}