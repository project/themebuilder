<?php

/**
 * @file
 * Theme Builder Edit Layout
 */
function themebuilder_layout() {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';
  $form['themebuilder_page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page'),
    '#description' => t('Page width') .': '. themebuilder_page_width("static") . $themebuilder_theme_layout .' ('. themebuilder_page_width("screen") .'px).',
    '#collapsible' => true,
    '#collapsed' => false,
  );
  $form['themebuilder_page']['themebuilder_page_alignment'] = array(
    '#type' => 'select',
    '#title' => t('Page alignment'),
    '#description' => t('Select the alignment of your page. Standard is center.'),
    '#options' => array(
     'left' => t('Left'),
     'center' => t('Center'),
     'right' => t('Right'),
    ),
    '#default_value' => variable_get('themebuilder_page_alignment', 'center')
  );
  $form['themebuilder_page']['themebuilder_page_min_max_unit'] = array(
    '#type' => 'select',
    '#title' => t('Minimum/Maximum unit'),
    '#options' => array(
      'px' => t('px'),
      '%' => t('%'),
      'em' => t('em'),
    ),
    '#default_value' => variable_get('themebuilder_page_min_max_unit', '%'),
    '#description' => t('Select the unit for minimum and maximum value. Standard is "em".') .' '. t('Current theme layout') .': '. $themebuilder_theme_layout_text .'.',
/* <br /><span class="warning">'. t('Warning: Do not use "px" in fixed Themes or "%" in liquid Themes or "em" in elastic Themes. It do not works!') .'</span>', */
  );
  $form['themebuilder_page']['themebuilder_page_min_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Page min-width'),
    '#default_value' => variable_get('themebuilder_page_min_width', $theme_min_default),
    '#field_suffix' => $theme_min_suffix,
    '#disabled' => $theme_min_max_disabled,
    '#size' => 8,
    '#maxlength' => 8,
    '#description' => t('Minimum width of page. Example: 50%, 600em or leave it blank. Standard: "20%".') .' <br /><span class="icon16 info">'. t('Only for liquid and elastic theme layout.') .'</span>',
  );
  $form['themebuilder_page']['themebuilder_page_max_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Page max-width'),
    '#default_value' => variable_get('themebuilder_page_max_width', $theme_max_default),
    '#field_suffix' => $theme_max_suffix,
    '#disabled' => $theme_min_max_disabled,
    '#size' => 8,
    '#maxlength' => 8,
    '#description' => t('Maximum width of page. Example: 100%, 600em or leave it blank. Standard: "100%".') .' <br /><span class="icon16 info">'. t('Only for liquid and elastic theme layout.') .'.</span>'
  );
  $form['themebuilder_page']['themebuilder_page_minmax_ie'] = array(
    '#type' => 'checkbox',
    '#title' => t('Minimum/Maximum hack for Internet Explorer'),
    '#default_value' => variable_get('themebuilder_page_minmax_ie', 0),
    '#description' => '<span class="error">'. t('Not <abbr title="Cascading Style Sheets">CSS</abbr> Valid in:') .' <a href="http://jigsaw.w3.org/css-validator/" title="W3C CSS Validation Service" ><abbr lang="en" xml:lang="en" title="World Wide Web Consortium">W3C</abbr> <abbr title="Cascading Style Sheets">CSS</abbr> Validation Service</a>!</span>',
  );
  $form['themebuilder_nav_padding'] = array(
    '#type' => 'fieldset',
    '#title' => t('Navigation'),
    '#description' => 'Set the primary and secondary padding left and right.',
    '#collapsible' => true,
    '#collapsed' => true,
  );
  $form['themebuilder_nav_padding']['themebuilder_primary_padding'] = array(
    '#type' => 'textfield',
    '#title' => t('Primary navigation padding'),
    '#default_value' => variable_get('themebuilder_primary_padding', '1'),
    '#field_suffix' => $themebuilder_theme_layout,
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t('Width padding left and right. Standard "1em"'),
  );
  $form['themebuilder_nav_padding']['themebuilder_secondary_padding'] = array(
    '#type' => 'textfield',
    '#title' => t('Secondary navigation padding'),
    '#default_value' => variable_get('themebuilder_secondary_padding', '0'),
    '#field_suffix' => $themebuilder_theme_layout,
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t('Width padding left and right. Standard "0em"'),
  );
  $form['themebuilder_sidebars'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sidebars'),
    '#description' => '<span class="icon16 help">'. t('Min. ca. 11em, Max. 30-37em.') .'<br />'. t('Max. 30em: By min. content width of 15em and 1em padding for 800 x 600 pixel') .'<br />'. t('Max. 37.em: By min. content width of 15em for 800 x 600 pixel.') .'</span><br /><span class="icon16 info">'. ('More information on:') .' <a class="icon16 oasis_spreadsheet" href="'. $module_path .'/source/layout.ods" title="The layout width" >layout.ods</a></span>',
    '#collapsible' => true,
    '#collapsed' => false,
  );
  $form['themebuilder_sidebars']['themebuilder_left_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Sidebar left width'),
    '#default_value' => variable_get('themebuilder_left_width', '19.5'),
    '#field_suffix' => $themebuilder_theme_layout,
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t('Width of sidebar-left. Standard: 19.5em. (20% of content by two sidebars)') .'<br /><span class="icon16 warning">'. t('Min. 11em, max. 37em.') .'</span>',
  );
  $form['themebuilder_sidebars']['themebuilder_right_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Sidebar right width'),
    '#default_value' => variable_get('themebuilder_right_width', '19.5'),
    '#field_suffix' => $themebuilder_theme_layout,
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t('Width of sidebar-left. Standard: 19.5em. (20% of content by two sidebars)') .'<br /><span class="icon16 warning">'. t('Min. 11em, max. 37em.') .'</span>',
  );
  $form['themebuilder_main_content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content'),
    '#collapsible' => true,
    '#collapsed' => false,
  );
  $form['themebuilder_main_content']['themebuilder_content_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Content width'),
    '#default_value' => variable_get('themebuilder_content_width', '40'),
    '#field_suffix' => $themebuilder_theme_layout,
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t('Width of content. Standard: 40em (for browser rounding errors minus 0.5em. No sidebars 33em).') .'<br /><span class="icon16 warning">'. t('Min. 15em, max. 37em. (for line length: 30-40, max. 70 indications)') .'</span>',
  );
  $form['themebuilder_rounding'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced width settings'),
    '#collapsible' => true,
    '#collapsed' => true,
  );
  $form['themebuilder_rounding']['themebuilder_rounding_errors'] = array(
    '#type' => 'textfield',
    '#title' => t('Rounding error width'),
    '#default_value' => variable_get('themebuilder_rounding_errors', '0.5'),
    '#field_suffix' => $themebuilder_theme_layout,
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Width for rounding errors. One sidebar: 0.5em, two sidebar 0.6em. Standard: 0.5em.') .'<br /><span class="icon16 warning">'. t('Min. 0.5em (8px), max. 1em (16px)') .'</span>',
  );
  $form['themebuilder_rounding']['themebuilder_browser_margin'] = array(
    '#type' => 'textfield',
    '#title' => t('Browser margin'),
    '#default_value' => variable_get('themebuilder_browser_margin', '2.5'),
    '#field_suffix' => $theme_layout_suffix,
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Browser margin width. Standard: 2.5em (left 1.25em and right 1.25em).') .'<br /><span class="icon16 warning">'. t('Min. 2.5em (40px), max. 3.75em (60px).') .'</span>',
  );
  $form['#validate'][] = 'themebuilder_validate_form';
    return system_settings_form($form);
}
/**
 *  Validate the administration form with drupal_validate_form
 */
function themebuilder_validate_form($form, &$form_state) {
  static $validated_forms = array();
  if (isset($validated_forms[$form_id])) {
    return;
  }
  if (isset($form['themebuilder_rounding'])) {
    if (!is_numeric($form_state['values']['themebuilder_browser_margin'])) {
    form_set_error('themebuilder_browser_margin', t('Please enter a number in "Browser margin".'));
    }
    if ($form_state['values']['themebuilder_browser_margin'] < 2.5 || $form_state['values']['themebuilder_browser_margin'] > 3.75) {
    form_set_error('themebuilder_browser_margin', t('Browser margin should between 2.5-3.75em;.'));
    }
    if ($form_state['values']['themebuilder_browser_margin'] < 2.5 || $form_state['values']['themebuilder_browser_margin'] > 3.75) {
    form_set_error('themebuilder_browser_margin', t('Browser margin should between 2.5-3.75em;.'));
    }
  }
  _form_validate($form, $form_state, $form_id);
  $validated_forms[$form_id] = TRUE;
}
