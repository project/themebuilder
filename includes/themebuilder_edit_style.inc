<?php

/**
 * @file
 * Theme Builder Edit Style
 */
function themebuilder_style() {
  $form['themebuilder_body_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Body'),
    '#description' => t('Body border style'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['themebuilder_body_style']['themebuilder_body_borderwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Body border width'),
    '#default_value' => variable_get('themebuilder_body_borderwidth', '0px'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('Set the border width. Standard: 0px'),
    '#required' => TRUE,
  );
  $form['themebuilder_body_style']['themebuilder_body_borderstyle'] = array(
    '#type' => 'select',
    '#title' => t('Body border style'),
    '#options' => array(
      'none' => 'none',
      'hidden' => 'hidden',
      'dotted' => 'dotted',
      'dashed' => 'dashed',
      'solid' => 'solid',
      'double' => 'double',
      'groove' => 'groove',
      'ridge' => 'ridge',
      'inset' => 'inset',
      'outset' => 'outset',
    ),
    '#default_value' => variable_get('themebuilder_body_borderstyle', 'none'),
    '#description' => t('Set the border style. Standard: "none".'),
  );
  $form['themebuilder_page_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page'),
    '#description' => t('The head border style.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['themebuilder_page_style']['themebuilder_div_page_borderwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Header border width'),
    '#default_value' => variable_get('themebuilder_div_page_borderwidth', '1px'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('Set the border width. Standard: 1px'),
    '#required' => TRUE,
  );
  $form['themebuilder_page_style']['themebuilder_div_page_borderstyle'] = array(
    '#type' => 'select',
    '#title' => t('Header border style'),
    '#options' => array(
      'none' => 'none',
      'hidden' => 'hidden',
      'dotted' => 'dotted',
      'dashed' => 'dashed',
      'solid' => 'solid',
      'double' => 'double',
      'groove' => 'groove',
      'ridge' => 'ridge',
      'inset' => 'inset',
      'outset' => 'outset',
    ),
    '#default_value' => variable_get('themebuilder_div_page_borderstyle', 'solid'),
    '#description' => t('Set the border style. Standard: "solid".'),
  );
  $form['themebuilder_header_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Header'),
    '#description' => t('The head border style.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['themebuilder_header_style']['themebuilder_div_header_borderwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Header border width'),
    '#default_value' => variable_get('themebuilder_div_header_borderwidth', '1px'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('Set the border width. Standard: 1px'),
    '#required' => TRUE,
  );
  $form['themebuilder_header_style']['themebuilder_div_header_borderstyle'] = array(
    '#type' => 'select',
    '#title' => t('Header border style'),
    '#options' => array(
      'none' => 'none',
      'hidden' => 'hidden',
      'dotted' => 'dotted',
      'dashed' => 'dashed',
      'solid' => 'solid',
      'double' => 'double',
      'groove' => 'groove',
      'ridge' => 'ridge',
      'inset' => 'inset',
      'outset' => 'outset',
    ),
    '#default_value' => variable_get('themebuilder_div_header_borderstyle', 'solid'),
    '#description' => t('Set the border style. Standard: "solid".'),
  );
  $form['themebuilder_sidebar_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sidebars'),
    '#description' => t('The sidebars border style.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['themebuilder_sidebar_style']['themebuilder_div_sidebar_borderwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Sidebars border width'),
    '#default_value' => variable_get('themebuilder_div_sidebar_borderwidth', '1px'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('Set the border width. Standard: 1px'),
    '#required' => TRUE,
  );
  $form['themebuilder_sidebar_style']['themebuilder_div_sidebar_borderstyle'] = array(
    '#type' => 'select',
    '#title' => t('Sidebars border style'),
    '#options' => array(
      'none' => 'none',
      'hidden' => 'hidden',
      'dotted' => 'dotted',
      'dashed' => 'dashed',
      'solid' => 'solid',
      'double' => 'double',
      'groove' => 'groove',
      'ridge' => 'ridge',
      'inset' => 'inset',
      'outset' => 'outset',
    ),
    '#default_value' => variable_get('themebuilder_div_sidebar_borderstyle', 'solid'),
    '#description' => t('Set the border style. Standard: "solid".'),
  );
  $form['themebuilder_content_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content'),
    '#description' => t('The content border style.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['themebuilder_content_style']['themebuilder_div_content_borderwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Main content border width'),
    '#default_value' => variable_get('themebuilder_div_content_borderwidth', '1px'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('Set the border width. Standard: 1px'),
    '#required' => TRUE,
  );
  $form['themebuilder_content_style']['themebuilder_div_content_borderstyle'] = array(
    '#type' => 'select',
    '#title' => t('Main content border style'),
    '#options' => array(
      'none' => 'none',
      'hidden' => 'hidden',
      'dotted' => 'dotted',
      'dashed' => 'dashed',
      'solid' => 'solid',
      'double' => 'double',
      'groove' => 'groove',
      'ridge' => 'ridge',
      'inset' => 'inset',
      'outset' => 'outset',
    ),
    '#default_value' => variable_get('themebuilder_div_content_borderstyle', 'solid'),
    '#description' => t('Set the border style. Standard: "none".'),
  );
  $form['themebuilder_footer_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Footer'),
    '#description' => t('The footer border style.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['themebuilder_footer_style']['themebuilder_div_footer_borderwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Footer border width'),
    '#default_value' => variable_get('themebuilder_div_footer_borderwidth', '1px'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('Set the border width. Standard: 1px'),
    '#required' => TRUE,
  );
  $form['themebuilder_footer_style']['themebuilder_div_footer_borderstyle'] = array(
    '#type' => 'select',
    '#title' => t('Footer border style'),
    '#options' => array(
      'none' => 'none',
      'hidden' => 'hidden',
      'dotted' => 'dotted',
      'dashed' => 'dashed',
      'solid' => 'solid',
      'double' => 'double',
      'groove' => 'groove',
      'ridge' => 'ridge',
      'inset' => 'inset',
      'outset' => 'outset',
    ),
    '#default_value' => variable_get('themebuilder_div_footer_borderstyle', 'solid'),
    '#description' => t('Set the border style. Standard: "solid".'),
  );
  $form['themebuilder_a_link_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['themebuilder_a_link_style']['themebuilder_a_link_borderwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Link border width'),
    '#default_value' => variable_get('themebuilder_a_link_borderwidth', '0px'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('Set the border width. Standard: 0px'),
    '#required' => TRUE,
  );
  $form['themebuilder_a_link_style']['themebuilder_a_link_borderstyle'] = array(
    '#type' => 'select',
    '#title' => t('Link border style'),
    '#options' => array(
      'none' => 'none',
      'hidden' => 'hidden',
      'dotted' => 'dotted',
      'dashed' => 'dashed',
      'solid' => 'solid',
      'double' => 'double',
      'groove' => 'groove',
      'ridge' => 'ridge',
      'inset' => 'inset',
      'outset' => 'outset',
    ),
    '#default_value' => variable_get('themebuilder_a_link_borderstyle', 'none'),
    '#description' => t('Set the border style. Standard: "none".'),
  );
  $form['themebuilder_a_visited_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link visited'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['themebuilder_a_visited_style']['themebuilder_a_visited_borderwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Link visited border width'),
    '#default_value' => variable_get('themebuilder_a_visited_borderwidth', '0px'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('Set the border width. Standard: 0px'),
    '#required' => TRUE,
  );
  $form['themebuilder_a_visited_style']['themebuilder_a_visited_borderstyle'] = array(
    '#type' => 'select',
    '#title' => t('Link visited border style'),
    '#options' => array(
      'none' => 'none',
      'hidden' => 'hidden',
      'dotted' => 'dotted',
      'dashed' => 'dashed',
      'solid' => 'solid',
      'double' => 'double',
      'groove' => 'groove',
      'ridge' => 'ridge',
      'inset' => 'inset',
      'outset' => 'outset',
    ),
    '#default_value' => variable_get('themebuilder_a_visited_borderstyle', 'none'),
    '#description' => t('Set the border style. Standard: "none".'),
  );
  $form['themebuilder_a_focus_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link focus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['themebuilder_a_focus_style']['themebuilder_a_focus_borderwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Link focus border width'),
    '#default_value' => variable_get('themebuilder_a_focus_borderwidth', '1px'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('Set the border width. Standard: 1px'),
    '#required' => TRUE,
  );
  $form['themebuilder_a_focus_style']['themebuilder_a_focus_borderstyle'] = array(
    '#type' => 'select',
    '#title' => t('Link focus border style'),
    '#options' => array(
      'none' => 'none',
      'hidden' => 'hidden',
      'dotted' => 'dotted',
      'dashed' => 'dashed',
      'solid' => 'solid',
      'double' => 'double',
      'groove' => 'groove',
      'ridge' => 'ridge',
      'inset' => 'inset',
      'outset' => 'outset',
    ),
    '#default_value' => variable_get('themebuilder_a_focus_borderstyle', 'solid'),
    '#description' => t('Set the border style. Standard: "solid".'),
  );
  $form['themebuilder_a_hover_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link hover'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['themebuilder_a_hover_style']['themebuilder_a_hover_borderwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Link hover border width'),
    '#default_value' => variable_get('themebuilder_a_hover_borderwidth', '1px'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('Set the border width. Standard: 1px'),
    '#required' => TRUE,
  );
  $form['themebuilder_a_hover_style']['themebuilder_a_hover_borderstyle'] = array(
    '#type' => 'select',
    '#title' => t('Link hover border style'),
    '#options' => array(
      'none' => 'none',
      'hidden' => 'hidden',
      'dotted' => 'dotted',
      'dashed' => 'dashed',
      'solid' => 'solid',
      'double' => 'double',
      'groove' => 'groove',
      'ridge' => 'ridge',
      'inset' => 'inset',
      'outset' => 'outset',
    ),
    '#default_value' => variable_get('themebuilder_a_hover_borderstyle', 'dashed'),
    '#description' => t('Set the border style. Standard: "dashed".'),
  );
  $form['themebuilder_a_active_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link active'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['themebuilder_a_active_style']['themebuilder_a_active_borderwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Link active border width'),
    '#default_value' => variable_get('themebuilder_a_active_borderwidth', '1px'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('Set the border width. Standard: 1px'),
    '#required' => TRUE,
  );
  $form['themebuilder_a_active_style']['themebuilder_a_active_borderstyle'] = array(
    '#type' => 'select',
    '#title' => t('Link active border style'),
    '#options' => array(
      'none' => 'none',
      'hidden' => 'hidden',
      'dotted' => 'dotted',
      'dashed' => 'dashed',
      'solid' => 'solid',
      'double' => 'double',
      'groove' => 'groove',
      'ridge' => 'ridge',
      'inset' => 'inset',
      'outset' => 'outset',
    ),
    '#default_value' => variable_get('themebuilder_a_active_borderstyle', 'solid'),
    '#description' => t('Set the border style. Standard: "solid".'),
  );
  return system_settings_form($form);
}