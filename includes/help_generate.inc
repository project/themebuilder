<?php

/**
 * @file
 * Generate the min/max with value
 *   $value: The normal width value for calculation.
 *   $def: "min" or "max" width
 *   Example: "themebuilder_calculate($themebuilder_left_width, 'min')"
 */
function themebuilder_calculate($value, $def) {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';
$page_width_calc = themebuilder_page_width("static");
if ($def == "min") {
  $value_calculate = round(($themebuilder_page_min_width * $value / $page_width_calc), 2);
}
else if ($def == "max") {
  $value_calculate = round(($themebuilder_page_max_width * $value / $page_width_calc), 2);
}
return $value_calculate;
}

/**
 * Calculate the page width
 */
function themebuilder_page_width($def) {
include drupal_get_path('module', 'themebuilder') .'/includes/data.inc';

if (!$themebuilder_sidebarleft_display == 0) { $left = $themebuilder_left_width; }
if (!$themebuilder_sidebarright_display == 0) { $right = $themebuilder_right_width; }

$page_width_calc = ($themebuilder_content_width + $left + $right + $themebuilder_rounding_errors );
$page_width_calc_left = ($themebuilder_content_width + $themebuilder_left_width  + $themebuilder_rounding_errors );
$page_width_calc_rigth = ($themebuilder_content_width  + $themebuilder_right_width + $themebuilder_rounding_errors );
$page_width_calc_two = ($themebuilder_content_width  + $themebuilder_left_width + $themebuilder_right_width + $themebuilder_rounding_errors );
$page_width_final_calc  = ($page_width_calc + $themebuilder_browser_margin);

if ($def == "static") {
  $value = $page_width_calc;
}
else if ($def == "left") {
  $value = $page_width_calc_left;
}
else if ($def == "right") {
  $value = $page_width_calc_rigth;
}
else if ($def == "two") {
  $value = $page_width_calc_two;
}
else if ($def == "screen") {
  if ($themebuilder_theme_layout == 'px') {
  $value = $page_width_calc;
  }
  else if ($themebuilder_theme_layout == '%') {
  $value = "x";
  }
  else if ($themebuilder_theme_layout == 'em') {
  $value = ($page_width_calc * 16);
  }
}
else if ($def == "final") {
  if ($themebuilder_theme_layout == 'px') {
  $value = $page_width_final_calc;
  }
  else if ($themebuilder_theme_layout == '%') {
  $value = "x";
  }
  else if ($themebuilder_theme_layout == 'em') {
  $value = ($page_width_final_calc  * 16);
  }
}
return $value;
}

/**
 * Calculate Hex colors to RGB
 */
function get_rgb($hex) {
  $hex_array = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    'A' => 10, 'B' => 11, 'C' => 12, 'D' => 13, 'E' => 14,
    'F' => 15);
  $hex = str_replace('#', '', strtoupper($hex));
  if (($length = strlen($hex)) == 3) {
    $hex = $hex{0} . $hex{0} . $hex{1} . $hex{1} . $hex{2} . $hex{2};
    $length = 6;
  }
  if ($length != 6 or strlen(str_replace(array_keys($hex_array), '', $hex)))
    return NULL;
    $rgb['r'] = $hex_array[$hex{0}] * 16 + $hex_array[$hex{1}];
    $rgb['g'] = $hex_array[$hex{2}] * 16 + $hex_array[$hex{3}];
    $rgb['b']= $hex_array[$hex{4}] * 16 + $hex_array[$hex{5}];
    return $rgb['r'] .' '. $rgb['g'] .' '. $rgb['b'];
}